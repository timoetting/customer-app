<?php 

class BackendController extends \BaseController {


	public function showBackend($projectSlug = null){

		$backendData = array();//Die Daten fürs Backend-View
		$backendData['currentProject'] = null;

		$projects = Project::all();
		if($projectSlug){
			// $project = $projects->where('id', $projectId)->first();
			// $project = $projects->find($projectId);
			$project = Project::where('slug', $projectSlug)->first();
			$backendData['currentProject'] = $project;
			$users = $project->users;
			//Ordnerstruktur
			$foldersArray = array();
			$foldersArray = CustomerFilesController::dirToDirArray(Config::get('paths.baseDir') . '/projects/' . $project->slug);//::get('paths.baseDir/'.$project->slug));
		}else{
			
			//$allFiles = CustomerFile::all();
			$foldersArray = array();
			$foldersArray = CustomerFilesController::dirToDirArray(Config::get('paths.baseDir'));

			$users = User::all();
			// foreach ($allFiles as $key => $file) {
			// 	if(!in_array($file->path, $foldersArray)){
			// 		$foldersArray[] = $file->path;
			// 	}
			// }



			//$usergroups = Usergroup::where('user_id', '=', NULL)->get();
		}
		//Ordnerstruktur




		// $backendData['foldersTree'] = $foldersTree;
		$backendData['foldersArray'] = $foldersArray;
		$backendData['users'] = $users;
		//$backendData['usergroups'] = $usergroups;
		$backendData['projects'] = $projects;
		$backendData['filesDiff'] = CustomerFilesController::getFilesDiff();

		return View::make('backend/layout')->with($backendData);
	}	

	public function showDashboard(){
		$backendData['projects'] = Project::with('users')->orderBy('title', 'asc')->get();
		$backendData['users'] = User::with('projects')->orderBy('username', 'asc')->get();
		
		$backendData['view_size'] = Input::get('view_size', 's');
		$backendData['sorting'] = Input::get('sorting', 'file_name');
		$backendData['getParams'] = '?sorting='.$backendData['sorting'].'&view_size='.$backendData['view_size'];

		return View::make('backend/pages/dashboard')->with($backendData);
	}

	public function showProject($projectSlug){
		$project = Project::where('slug', $projectSlug)->first();
		$backendData['project'] = $project;
		$users = $project->users;
		$backendData['users'] = $users;
		$backendData['projectFolder'] = $project->projectFolder();
		$backendData['view_size'] = Input::get('view_size', 's');
		$backendData['sorting'] = Input::get('sorting', 'file_name');
		$backendData['collapsedFolders'] = Auth::user()->collapsedFolders->lists('id');
		$backendData['getParams'] = '?sorting='.$backendData['sorting'].'&view_size='.$backendData['view_size'];
		if(isset($_GET['view-size']) && $_GET['view-size'] == 'm')
			$backendData['viewSize'] = 'm';
		if(!isset($_GET['view-size']) || $_GET['view-size'] == 's')
			$backendData['viewSize'] = 's';

		Auth::user()->load('collapsedFolders');

		return View::make('backend/pages/project')->with($backendData);
	}

	public function showUserInProject($projectSlug, $username, $userfolderId = null){
		$backendData['project'] 	= Project::where('slug', $projectSlug)->first();
		$backendData['user'] 		= User::where('username', $username)->first();
		$backendData['users'] 		= $backendData['project']->users;
		$backendData['projectFolder'] = $backendData['project']->projectFolder();
		$backendData['mainFolder'] 	= false;
		$backendData['view_size'] 	= Input::get('view_size', 's');
		$backendData['sorting'] 	= Input::get('sorting', 'file_name');
		$backendData['custom_sort_mode'] = Input::get('custom_sort_mode', false);
		$backendData['collapsedFolders'] = Auth::user()->collapsedFolders->lists('id');
		$backendData['getParams'] = '?sorting='.$backendData['sorting'].'&view_size='.$backendData['view_size'].'&custom_sort_mode='.$backendData['custom_sort_mode'];
		if($userfolderId != null && $userfolderId != 0){
			$backendData['userfolder'] = Userfolder::find($userfolderId);
		}
		else{
			$backendData['userfolder'] = null;
			if($userfolderId == 0)
				$backendData['mainFolder'] = true;
		}
		
		if(isset($_GET['view-size']) && $_GET['view-size'] == 'm')
			$backendData['viewSize'] = 'm';
		if(!isset($_GET['view-size']) || $_GET['view-size'] == 's')
			$backendData['viewSize'] = 's';

		Auth::user()->load('collapsedFolders');

		return View::make('backend/pages/user')->with($backendData);
	}

	public function createDirForm(){
		return View::make('backend.forms.create_folder');
	}

	public function createDir(){//obsolet
		$input = Input::get();
		// return $input['current-dir'].$input['dir'];
		File::makeDirectory($input['current-dir'] . $input['dir']);
		return 'bla';
	}

	public function getFolderTreeView(){
		$foldersArray = CustomerFilesController::dirToDirArray(Config::get('paths.baseDir'));
		return View::make('backend.folder_tree')->with('foldersArray', $foldersArray);
	}
}

?>
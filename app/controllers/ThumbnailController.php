<?php

class ThumbnailController extends \BaseController {
	
	public function saveProjectThumb($projectId){
		$project = Project::find($projectId);
		$thumbName = $project->slug . $project->id . microtime() . 'jpg';

		Image::make(Input::file('thumb'))->resize(200, null, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
		})->save(Config::get('paths.thumbnailsProjects').$thumbName , 60);

		$project->thumb = $thumbName;
		$project->save();
	}

	public static function saveFileThumb($file = null, $imgPath = null){
		if($file == null){
			$input = Input::get();
			$file = CustomerFile::find($input['fileId']);
			$img 	= Image::make(Input::file('thumb'));
			$imgMd 	= Image::make(Input::file('thumb'));
		}else{
			if(is_int($file))
				$file = CustomerFile::find($file);
			$img 	= Image::make($imgPath);
			$imgMd 	= Image::make($imgPath);
		}

		

		// $imgMd = $img;

		//small thumb
		$img->resize(120, 90, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
		});

		//medium thumb
		$imgMd->resize(500, 313, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
		});
		if($imgPath == null){
			$thumbName = 'thumb_' . $file->id . '_' . time()  . '.jpg';
		}else{
			$thumbName = basename($imgPath);
		}
		$img->save(Config::get('paths.thumbnailsFilesSm').$thumbName , 60);
		$imgMd->save(Config::get('paths.thumbnailsFilesMd').$thumbName , 60);

		$file->thumb = $thumbName;
		$file->save();

		return 'thumb saved';
	}

	public static function thumbFromVideo($file, $seconds = null){

		if(is_numeric($file))
			$file = CustomerFile::find($file);

		//dd($file);
		$video = $file->fullPath();
		$thumbnail = Config::get('paths.thumbnailsFilesLg') . 'thumb_' . $file->id . '_' . time()  . '.jpg';
		//dd(shell_exec(Config::get('paths.ffmpeg') . " -i ".$video." 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//"));
		$time = exec(Config::get('paths.ffmpeg') . " -i '".$video."' 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");

		if($seconds == null){
			$totalSeconds = strtotime("1970-01-01 $time UTC");
			$seconds = round($totalSeconds / 2);
		}

		// dd('time: '. $time . ' totalSeconds: ' . $totalSeconds . ' $seconds: ' . $seconds);

		$t = $seconds;
		$time = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);

		// dd(Config::get('paths.ffmpeg') . ' -i "'.$video.'" -deinterlace -an -ss '.$seconds.' -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg "'.$thumbnail.'" 2>&1');
		shell_exec(Config::get('paths.ffmpeg') . ' -ss '.$seconds.' -i "'.$video.'" "'.$thumbnail.'" -deinterlace -an -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg 2>&1');
		// dd($thumbnail);
		// ffmpeg -ss 123 -i "VIDEO_FILE" "THUMBNAIL_FILE" -r 1 -vframes 1 -an -vcodec mjpeg

		self::saveFileThumb($file, $thumbnail);

	}


}
<?php

class FoldersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * moves Folder to Folder. Updates entries and move file on Server
	 * @param  int/object $file File ID or object
	 * @param  string $path destination
	 * @return $file       the moved file as object
	 */
	public static function moveFolderToFolder($folderId, $newParentId){
		// if(is_int($folder))
			$folder = Folder::findOrFail($folderId);
		// if(is_int($newParent))
			$newParent = Folder::findOrFail($newParentId);

		$oldPath = $folder->fullPath();
		$newPath = $newParent->fullPath() . $folder->title . DIRECTORY_SEPARATOR;

		


		File::copyDirectory($oldPath, $newPath);
		// dd($oldPath, $newPath);
		File::deleteDirectory($oldPath);

		$files = CustomerFile::where('path', 'like', $oldPath.'%')->get();
		foreach ($files as $file) {
			$file->path = substr_replace($file->path, $newPath, 0, strlen($oldPath));//nochmal drüber nachdenken
			$file->save();
		}
		$subFolders = Folder::where('path', 'like', $oldPath.'%')->get();
		foreach ($subFolders as $subFolder) {
			$subFolder->path = substr_replace($subFolder->path, $newPath, 0, strlen($oldPath));//nochmal drüber nachdenken
			$subFolder->save();
		}

		//File::move($file->path . $file->file_name, $path . $file->file_name);
		$folder->path = $newParent->fullPath();
		// $folder->path = $newParent->path;
		$folder->parent()->associate($newParent);

		$folder->save();

		$parentUsers = $newParent->users;

		foreach ($parentUsers as $user) {
			$folder->attachUser($user);
		}

		return $folder;

		// echo substr_replace('clientfiles/Apps/WorkFlowy/Data/', 'neuer/Ordner/', 0, strlen('clientfiles/Apps/')) . "<br />\n"; 
		
	}

	public function attachFolderToUser($folderId, $userId){
		$user = User::find($userId);
		$folder = Folder::find($folder);
		$folder->attachUser($user);
		echo 'ok';
	}

	public function toggleFolderUserRelation($folderId, $userId){
		$folder = Folder::find($folderId);
		$user = User::find($userId);
		if($folder->hasUser($user->id))
			$folder->detachUser($user);
		else
			$folder->attachUser($user);
		echo 'ok';
	}

	public function detachFolderFromUser($folder, $user){
		if(is_int($folder))
			$folder = Folder::find($folderId);
		if(is_int($user)){
			$user = User::find($user);
		}

		$folder->detachUser($user);
		//echo 'ok';
	}

	public function attachFolderToUserfolder(){
		$postData = Input::all();
		$folder = Folder::find($postData['folderId']);
		$userfolder = Userfolder::find($postData['folderId']);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.forms.create_folder');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::get();
		self::storeByTitle($input['title'], Folder::find($input['parent-id']));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param string $title Titel des Folders
	 * @param int or Folder $parent 	Wenn int, dann parent ID, sonst direkt parent-folder-Objekt
	 * @param int or Project-Objct $project zugeordnetes Projet
	 * @param boolean $isProjectFolder ist dies der Hauptordner eines Projekts?
	 * @return Response
	 */
	public static function storeByTitle($title, $parent = null, $project = null, $isProjectFolder = false)
	{
		$folder = new Folder;
		$folder->title = $title;
		$folder->is_project_folder = $isProjectFolder;

		if($project != null){
			if(is_int($project)){
				$project = Project::find($project);
			}
		}

		if($parent != null){
			if(is_int($parent)){
				$parent = Folder::find($parent);
			}
			$folder->path = $parent->fullPath();

			if($project == null){
				$project = $parent->project;
			}
		}else{
			$folder->path = null;
		}

		if($project != null)
			$folder->project()->associate($project);

		if($parent != null)
			$folder->parent()->associate($parent);

		if(Folder::getFolderByPath($folder->path . $folder->title . DIRECTORY_SEPARATOR) != null)
			return false;

		$folder->save();

		if(!File::isDirectory($folder->fullPath())){
			File::makeDirectory($folder->fullPath(), 0777, true);
		}

		$parentUsers = $parent->users;

		foreach ($parentUsers as $user) {
			$folder->attachUser($user);
		}

		return $folder;

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param string $path Pfad, z.b. clientfiles/projects/neuesprojekt/
	 * @param int or Project-Objct $project zugeordnetes Projet
	 * @param boolean $isProjectFolder ist dies der Hauptordner eines Projekts?
	 * @return Response
	 */
	public static function storeByPath($path, $project = null, $isProjectFolder = false)
	{
		if(Folder::getFolderByPath($path) != null)
			return false;
		$folder = new Folder;
		$folder->title = basename($path);
		$folder->path = dirname($path) . DIRECTORY_SEPARATOR;
		if($isProjectFolder) 
			$folder->is_project_folder = true;

		$folder->is_project_folder = $isProjectFolder;

		

		if($project != null && is_int($project)){
			$project = Project::find($project);
		}

		// $folder->parent()->save(Folder::getFolderByPath($folder->path));
		$parent = Folder::getFolderByPath($folder->path);
		if($parent != null){
			$folder->parent()->associate($parent);
			$parentProject = $parent->project;
			if($parentProject != null)
				$folder->project()->associate($parentProject);
			elseif($project != null){
				$folder->project()->associate($project);
			}
		}


		$folder->save();

		if(!File::isDirectory($folder->fullPath())){
			File::makeDirectory($folder->fullPath(), 0777, true);
		}
		if($parent != null){

			$parentUsers = $parent->users;

			foreach ($parentUsers as $user) {
				$folder->attachUser($user);
			}
		}

		return $folder;
	}

	public function collapse($folderId, $userId){
		
		$user = User::find($userId);
		$user->collapsedFolders()->attach($folderId);
		$user->save();

	}

	public function decollapse($folderId, $userId){
		$user = User::find($userId);
		$user->collapsedFolders()->detach($folderId);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$folder = Folder::find($id);
		return View::make('backend.forms.update_folder', compact('folder'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$folder = Folder::find($id);
		$newTitle = Input::get('title');
		$oldTitle = $folder->title;
		$newPath = $folder->path . $newTitle . DIRECTORY_SEPARATOR;
		$oldPath = $folder->fullPath();

		$folder->title = $newTitle;

		$files = CustomerFile::where('path', 'like', $oldPath.'%')->get();
		foreach ($files as $file) {
			$file->path = substr_replace($file->path, $newPath, 0, strlen($oldPath));//nochmal drüber nachdenken
			$file->save();
		}
		$subFolders = Folder::where('path', 'like', $oldPath.'%')->get();
		foreach ($subFolders as $subFolder) {
			$subFolder->path = substr_replace($subFolder->path, $newPath, 0, strlen($oldPath));//nochmal drüber nachdenken
			$subFolder->save();
		}


		rename($oldPath, $newPath);

		$folder->save();


	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// $folder = Folder::find($id);
		// CustomerFile::where('path', 'like', $folder->fullPath().'%')->delete();
		// Folder::where('path', 'like', $folder->fullPath().'%')->delete();
		
		// File::deleteDirectory($folder->fullPath);
		// $folder->delete();
	}

	public static function delete($id)
	{
		$folder = Folder::find($id);
		CustomerFile::where('path', 'like', $folder->fullPath().'%')->delete();
		Folder::where('path', 'like', $folder->fullPath().'%')->delete();
		
		File::deleteDirectory($folder->fullPath());
		$folder->delete();
	}
}

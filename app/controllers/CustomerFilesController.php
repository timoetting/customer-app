<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Filesystem;

class CustomerFilesController extends \BaseController {

	//public static function drawFileTree($project, )

	/**
	 * sorts Collection by natural order, i.e. 1.8 < 1.54
	 * @param  [type] $files [description]
	 * @return [type]        [description]
	 */
	public static function sortByNaturalFileName($files){
		$orderArrays = array();
		return $files->sort(function($a, $b) use($orderArrays){
			$oldOrder = array($a->file_name, $b->file_name);
			$newOrder = $oldOrder;
			natsort($newOrder);	
			return ($oldOrder === $newOrder) ? -1 : 1;
		});
	}

	/**
	 * holt alle Dateien aus dem gegebenen Verzeichnis und überträgt sie ins DB-Filesystem
	 */
	public function initFileList(){
		echo getcwd();
		$filesInFolders = $this->dirToFilePathArray(Config::get('paths.baseDir'));
		foreach ($filesInFolders as $key => $filePath) {
			$this->store($filePath);
		}
	}

	static function getFolderFiles($path){

	}

	public static function getDownloadLink($id){
		$file = CustomerFile::find($id);
		header("Location: " . URL::to('/'));
		//header("Location: " . URL::to('/').$file->path.$file->file_name);
		//Response::download($file->path . $file->file_name);
	}

	/**
	 * vergleicht das tatsächliche Verzeichnis mit den DB-Einträgen. Neue Einträge werden nachgetragen.
	 */

	public function matchFileList(){
		$allFiles = CustomerFile::all();
		$allFilesPaths = array();
		$allFolderFiles = $this->dirToFilePathArray(Config::get('paths.baseDir'));

		echo '<pre>';
		foreach ($allFiles as $key => $file) {
			$allFilesPaths[] = $file->path . $file->file_name;
			echo $file->file_name;;
		}
		echo '<br>';
		foreach ($allFolderFiles as $key => $file) {
			echo $file . '<br>';
		}
		echo '</pre>';
	}

	/**
	 * Synct alle File Entries mit den Files im Ordner und löscht fehlende (beiderseits)
	 */
	public static function syncFilesystem(){
		self::syncFolderEntries();
		self::syncFileEntries();
	}

	/**
	 * Synct alle folder Entries mit den Folders und löscht fehlende (beiderseits)
	 */
	public static function syncFolderEntries(){

		$folderDiff = CustomerFilesController::getFolderDiff();
		foreach ($folderDiff[0] as $folderPath) {
			FoldersController::storeByPath($folderPath);
		}
		foreach ($folderDiff[1]  as $folderId => $folderPath) {
			FoldersController::delete($folderId);
		}
	}

	/**
	 * Synct alle file Entries mit den Folders und löscht fehlende (beiderseits)
	 */
	public static function syncFileEntries(){

		$fileDiff = CustomerFilesController::getFilesDiff();
		foreach ($fileDiff[0] as $filePath) {
			//check if file is entirely uploaded
			if(time() - filemtime($filePath) > 30)
				self::store($filePath);
		}
		foreach ($fileDiff[1]  as $fileId => $filePath) {
			self::delete($fileId);
		}
	}

	/**
	 * Checkt ob Datei komplett hochgeladen ist.
	 */
	// private function fileComplete($filePath){
	// 	$size1 = filesize($filePath);
	// 	sleep()
	// 	$size2 = 0;

	// }

	/**
	 * Synct alle Folder Entries mit den Files im Ordner
	 */
	// public function syncFolderEntries(){

	// 	$filesToAdd = CustomerFilesController::getFilesDiff();
	// 	foreach ($filesToAdd[0] as $filePath) {
	// 		$this->store($filePath);
	// 	}
	// }


	/**
	 * 	inDB = true: holt alle Files, die in DB, aber nicht im Ordner vorhanden sind
	 *	inDB = false: holt alle Files, die in Ordner, aber nicht in DB vorhanden sind
	 * @return Diff Array
	 */
	public static function getFilesDiff(){
		$return = array();

		$allFileEntries = CustomerFile::all();
		$allFilesDB = array();
		$allFilesFolder = CustomerFilesController::dirToFilePathArray(Config::get('paths.baseDir'));

		foreach ($allFileEntries as $file) {
			$allFilesDB[$file->id] = $file->path . $file->file_name;
		}

		return array ( array_diff($allFilesFolder, $allFilesDB) , array_diff($allFilesDB, $allFilesFolder) );

		// if($inFolder){
		// 	return array_diff($allFilesFolder, $allFilesDB);
		// } else {
		// 	return array_diff($allFilesDB, $allFilesFolder);
		// }
	}

	/**
	 * 	inDB = true: holt alle Files, die in DB, aber nicht im Ordner vorhanden sind
	 *	inDB = false: holt alle Files, die in Ordner, aber nicht in DB vorhanden sind
	 * @return Diff Array
	 */
	public static function getFolderDiff(){
		$return = array();
		$allFolderEntries = Folder::where('path', 'like', Config::get('paths.baseDir') .'%')->get();
		// $allFolderEntries = Folder::whereRaw('path like ' . Config::get('paths.baseDir') . '%');
		$allDbFolders = array();
		foreach ($allFolderEntries as $folder) {
			$allDbFolders[$folder->id] = $folder->path . $folder->title;
		}

		$allDirFolders = CustomerFilesController::dirToFolderPathArray(Config::get('paths.baseDir'));

		return array ( array_diff($allDirFolders, $allDbFolders) , array_diff($allDbFolders, $allDirFolders) );



		// if($inFolder){
		// 	return array_diff($allFilesFolder, $allFilesDB);
		// } else {
		// 	return array_diff($allFilesDB, $allFilesFolder);
		// }
	}

	/**
	 * 	inDB = true: holt alle Files, die in DB, aber nicht im Ordner vorhanden sind
	 *	inDB = false: holt alle Files, die in Ordner, aber nicht in DB vorhanden sind
	 * @return Diff Array
	 */
	// static function getFilesDiff() {
	// 	return Response::json(getFilesDiff());
	// }

	/**
	 *  gets an Array of all filespaths inside the given directory
	 * 
	 */
	static function dirToFilePathArray($dir) { 

		$result = array(); 

		$cdir = scandir($dir); 
		foreach ($cdir as $key => $value) 
		{ 
			if (!in_array($value,array(".","..")) && substr($value, 0, 1) != '.') 
			{ 
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
				{ 
					$result = array_merge($result, CustomerFilesController::dirToFilePathArray($dir . DIRECTORY_SEPARATOR . $value)); 
				} 
				else 
				{ 
					$result[] = $dir . DIRECTORY_SEPARATOR . $value; 
				} 
			} 
		} 
		return $result; 
	} 

	/**
	 *  gets an Array of all folderspaths inside the given directory
	 * 
	 */
	static function dirToFolderPathArray($dir) { 

		$result = array(); 

		$cdir = scandir($dir); 
		foreach ($cdir as $key => $value) 
		{ 
			if (!in_array($value,array(".","..")) && substr($value, 0, 1) != '.') 
			{ 
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
				{ 
					$result[] = $dir . DIRECTORY_SEPARATOR . $value;
					$result = array_merge($result, CustomerFilesController::dirToFolderPathArray($dir . DIRECTORY_SEPARATOR . $value)); 
				} 
				// else 
				// { 
				// 	$result[] = $dir . DIRECTORY_SEPARATOR . $value; 
				// } 
			} 
		} 
		return $result; 
	} 

	/**
	 *  gets all FileEntries below folder
	 *
	 *@param int or Folder-Object $folder Folder ID if int, Folder-Object if Object
	 * @return Collection of CustomerFiles
	 */
	public static function getCustomerFilesBelowFolder($folder) { 
		if(is_int($folder)){//wenn ID
			$folder = Folder::findOrFail($folder);
		}elseif(is_string($folder)){//wenn Pfad
			$folder = Folder::getFolderByPath($folder);
		}
		//ansonsten sollte $folder ein Folder-Objekt sein
		$files = CustomerFile::where('path', 'like', $folder->fullPath().'%')->get();
		// $files = CustomerFile::where('path', $folder->fullPath());
		// $files = CustomerFile::where('path', 'clientfiles/projects/testor-x/')->get();
		return $files;
	}

	/**
	 * creates nested array of path list
	 */

	static function build_tree($path_list) {
	    $path_tree = array();
	    foreach ($path_list as $path) {
	        $list = explode('/', trim($path, '/'));
	        $last_dir = &$path_tree;
	        foreach ($list as $dir) {
	            $last_dir =& $last_dir[$dir];
	        }
	        // $last_dir['__title'] = 'Titel';
	    }
	    return $path_tree;
	}

	/**
	 *  gets an Array of all directories inside the given directory
	 * 
	 */
	static function dirToDirArray($dir) { 
		// $dir = DIRECTORY_SEPARATOR . $dir;

		$result = array(); 
		$result[] = $dir;

		$cdir = scandir($dir); 
		foreach ($cdir as $key => $value) 
		{ 
			if (!in_array($value,array(".","..")) && substr($value, 0, 1) != '.') 
			{ 
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
				{ 
					$result = array_merge($result, CustomerFilesController::dirToDirArray($dir . DIRECTORY_SEPARATOR . $value)); 
				}
			}
		}
		return $result; 
	}

	/**
	 * saves all child folders as Folder Model
	 */

	static function writeFolders($dir, $parentId = null) { 

		// $dir = DIRECTORY_SEPARATOR . $dir;
		$result = array(); 
		$result[] = $dir;
		$folder = new Folder();
		$folder->title = basename($dir);
		$folder->path = dirname($dir) . DIRECTORY_SEPARATOR;
		$folder->parent_id = $parentId;
		$folder->save();

		$cdir = scandir($dir); 
		foreach ($cdir as $key => $value) 
		{ 
			if (!in_array($value,array(".","..")) && substr($value, 0, 1) != '.') 
			{ 
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
				{ 
					$result = array_merge($result, CustomerFilesController::writeFolders($dir . DIRECTORY_SEPARATOR . $value, $folder->id)); 
				}
			}
		}
		return $result; 
	}





	/**
	 * Displays all Files that are inside specific Path
	 */

	public function inPath($path = null){

		if($path){

			$files = CustomerFile::where('path', $path . '/')->get();

			return View::make('backend.file_list')->with('files', $files);
		}
	}

	/**
	 * Displays all Files that are inside specific Project
	 */

	// public function getFilesInProject($projectId){

	// 	$project = Project::findOrFail($projectId);
	// 	return CustomerFile::where('path', Config::get('paths.projectsDir') . '/' . $project->slug . '/')->get();
	// }

	/**
	 * Displays all Files of a given User
	 */

	public static function ofUser($userId, $userfolder = '/'){

		$user = User::findOrFail($userId);

		if($userfolder == '/'){

			$files = $user->customerFiles;

			return View::make('backend.file_list')->with('files', $files);

			// foreach ($files as $file) {
			// 	echo View::make('backend.file')->with('file', $file);
			// }
		} 
	}

	public function attachFileToUser($fileId, $userId){
		$file = CustomerFile::find($fileId);
		$user = User::find($userId);
		$file->attachUser($user);
		echo 'ok';
	}

	public function attachFilesToUser($userId){

		//dd(Input::get('fileIds'));
		$fileIds = Input::get('fileIds');
		$files = CustomerFile::find($fileIds);
		$user = User::find($userId);
		//dd(Input::get('userId'));
		foreach ($files as $file) {
			$file->attachUser($user, false);
		}
		echo 'ok, attached';
		// return Response::json(array(
		//                 'msg' => 'allright' 
		//             ), 200);
	}

	public function detachFileOfUser($fileId, $userId){
		$file = CustomerFile::find($fileId);
		$user = User::find($userId);
		$file->detachUser($user);
		echo 'ok';
	}

	public function detachFilesFromUser($userId){
		$fileIds = Input::get('fileIds');
		$files = CustomerFile::find($fileIds);
		$user = User::find($userId);
		foreach ($files as $file) {
			$file->detachUser($user, false);
		}
		echo 'ok, detached';
		// return Response::json(array(
		//                 'msg' => 'allright' 
		//             ), 200);
	}

	public function toggleFileUserRelation($fileId, $userId){
		$file = CustomerFile::find($fileId);
		$user = User::find($userId);
		if($file->hasUser($user->id))
			$file->detachUser($user);
		else
			$file->attachUser($user);
		echo 'ok';
	}

	// public function connectFileToUsergroup($fileId, $usergroupId, $return = true){
	// 	$file = CustomerFile::find($fileId);

	// 	$usergroup = Usergroup::find($usergroupId);

	// 	if(!$usergroup->customerfiles->contains($fileId)){
	// 		$usergroup->customerFiles()->save($file);
	// 	}

	// 	if($return){
	// 		echo View::make('backend.usergroup')->with('usergroup', $usergroup);
	// 	}
	// }

	public function uploadHandler($folderId){
		$folder = Folder::find($folderId);
		$file = Input::file('file');

		$path = $folder->fullPath();
		$destinationPath = $path;
		$filename = $file->getClientOriginalName();
		$extension =$file->getClientOriginalExtension(); 

		$fileExisted = false;

		if( file_exists($path . $filename) ){
			$fileExisted = true;
		}

		$upload_success = $file->move($destinationPath, $filename);

		if( $upload_success ) {
			if (!$fileExisted) {
				$filePath = $path . $filename;
				self::store($filePath);
			} else {
				$file = CustomerFile::whereRaw('path = "' . $path . '" and file_name = "' . $filename . '"')->first();
				$file->touch();
			}
		   return Response::json('success', 200);
		} else {
		   return Response::json('error', 400);
		}
	}

	/**
	 * moves multiple files to a folder
	 */
	public static function moveFilesToFolder(){
		$input = Input::get();
		$fileIds = $input['fileIds'];
		$folderId = $input['folderId'];

		foreach ($fileIds as $fileId) {
			self::moveFileToFolder($fileId, $folderId);
		}

		return Response::json('success', 200);
	}

	/**
	 * moves File to Folder. Updates entries and moves file on Server
	 * @param  int/object $file File ID or object
	 * @param  string $path destination
	 * @return $file       the moved file as object
	 */
	public static function moveFileToFolder($fileId, $folderId){
		$folder = Folder::findOrFail($folderId);
		$file = CustomerFile::findOrFail($fileId);

		File::move($file->path . $file->file_name, 
			$folder->fullPath() . $file->file_name);
		$file->path = $folder->fullPath();
		$file->folder()->associate($folder);
		$file->save();

		$folderUsers = $folder->users;

		// echo 'folderUserCount: '.count($folderUsers);
		// foreach ($folderUsers as $fuser) {
		// 	echo ' '.$fuser->username;
		// }
		$fileUsers = $folder->users->merge($file->users);

		// foreach($folderUsers as $user){
		// 	$file->attachUser($user);
		// }	
		foreach ($fileUsers as $fileUser) {
			$file->attachUser($fileUser);
		}

		// if($file->hasVideoBrother()){	

		// 	$brotherFile = $file->videoBrother;
		// 	File::move($brotherFile->path . $brotherFile->file_name, 
		// 		$folder->fullPath() . $brotherFile->file_name);
		// 	$brotherFile->path = $folder->fullPath();
		// 	$brotherFile->folder()->associate($folder);
		// 	$brotherFile->save();
		// }else{
		// 	if($file->file_type == 'mp4' || $file->file_type == 'webm'){
		// 		self::checkForVideoBrother($file);
		// 	}
		// }

		return $file;
	}

	public static function moveFileToUserfolder($fileId, $userfolderId){
		$userfolder = Userfolder::findOrFail($userfolderId);
		$file = CustomerFile::findOrFail($fileId);

		$file->userfolders()->save($userfolder);

		if($file->hasVideoBrother()){	
			$brotherFile = $file->videoBrother;
			$brotherFile->userfolder()->associate($userfolder);
		}
		
		return $file;
	}


	/**
	 * 
	 *
	 *	Recource Controller Stuff	
 	 *
	 *
	 */

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$files = CustomerFile::all();
		foreach($files as $file){
			echo View::make('backend.file')->with('file', $file);
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public static function store($filePath, $title = '')
	{
		$pathInfo = pathinfo($filePath);
		$file = new CustomerFile;
		if($title != '')
			$file->title = $title;
		else
			$file->title = basename($filePath);
		$file->file_name = basename($filePath);
		$file->path = dirname($filePath) . '/';

		$file->file_type = ( !empty($pathInfo['extension']) ) ? $pathInfo['extension'] : 'none';

		$folder = Folder::getFolderByPath($file->path);

		if($folder != null){
			$file->folder()->associate($folder);// = Folder::getFolderByPath($file->path);
		}
		//$file->folder_id = Folder::getFolderByPath($file->path);
		$file->save();

		if($folder != null){
			$folderUsers = $folder->users;
			foreach ($folderUsers as $folderUser) {
				$file->attachUser($folderUser, false);
				// $file->users()->save($folderUser);
			}
		}

		if($file->file_type == 'mp4' || $file->file_type == 'webm'){
			self::checkForVideoBrother($file);
		}

		if($file->file_type == 'mp4'){
			ThumbnailController::thumbFromVideo($file);
		}

		return true;

	}

	/**
	 * Checks for other file next to this file to combine to video, i.e. mp4 and webM will be combined
	 */
	private static function checkForVideoBrother($file){
		$otherFiles = CustomerFile::where('path', $file->path)
									->where('id', '!=', $file->id)->get();
								
		foreach ($otherFiles as $otherFile) {
			if(basename($file->file_name, "." . $file->file_type) == basename($otherFile->file_name, "." . $otherFile->file_type) &&
			($otherFile->file_type == 'mp4' || $otherFile->file_type == 'webm')){
				$file->attachVideoBrother($otherFile);
				return true;
			}
		}
		return false;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		App::error(function(ModelNotFoundException $e)
		{
		    return Response::make('Not Found', 404);
		});

		$file = CustomerFile::findOrFail($id);
		return View::make('backend.file')->with('file', $file);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$file = CustomerFile::findOrFail($id);

		if($file->isVideo())
			return View::make('backend.forms.update_video', compact('file'));
		else
			return View::make('backend.forms.update_file', compact('file'));

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$file = CustomerFile::findOrFail($id);

		// $file->fill(Input::all());
		$input = Input::get();
		$file = CustomerFile::find($id);
		$file->title 		= $input['title'];
		$file->description 	= $input['description'];
		// $file->thumb_path = 'thumbnails/'.$input['thumb_file_name'];
		if($input['filename'] != $file->file_name){
			$file->setFileName($input['filename']);
		}
		$file->save();

		return View::make('backend.file', compact('file'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public static function destroy($id)
	{
		// $file = CustomerFile::findOrFail($id);
		// $file->delete();
	}

	public static function multiDelete()
	{
		$input = Input::get();
		$fileIds = $input['selectedFilesIds'];
		foreach ($fileIds as $fileId) {
			$file = CustomerFile::findOrFail($fileId);
			File::delete($file->fullPath());
			$file->delete();
		}
	}

	public static function delete($id)
	{
		$file = CustomerFile::findOrFail($id);
		File::delete($file->fullPath());
		// if($file->hasVideoBrother()){
		// 	$otherFile = $file->videoBrother;
		// 	File::delete($otherFile->fullPath());
		// 	$otherFile->delete();
		// }
		$file->delete();
	}


	public static function sendFile($path, $name = null, array $headers = array()){
		    if (is_null($name)) $name = basename($path);

		    $file = new Symfony\Component\HttpFoundation\File\File($path);
		    $mime = $file->getMimeType();

		    // Prepare the headers
		    $headers = array_merge(array(
		        'Content-Description'       => 'File Transfer',
		        'Content-Type'              => $mime,
		        'Content-Transfer-Encoding' => 'binary',
		        'Expires'                   => 0,
		        'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
		        'Pragma'                    => 'public',
		        'Content-Length'            => File::size($path),
		        'Content-Disposition'	    => 'attachment; filename='.$name
		    ), $headers);

		    $response = new Symfony\Component\HttpFoundation\Response('', 200, $headers);

		    // If there's a session we should save it now
		    if (Config::get('session.driver') !== ''){
		        Session::save();
		    }

		    session_write_close();
		    if (ob_get_length()) ob_end_clean();
		    
		    $response->sendHeaders();
		    
		    // Read the file
		    if ($file = fopen($path, 'rb')) {
		        while(!feof($file) and (connection_status()==0)) {
		            print(fread($file, 1024*8));
		            flush();
		        }
		        fclose($file);
		    }

		    // Finish off, like Laravel would
		    Event::fire('laravel.done', array($response));
		    $response->send();

		    exit;
		}





}

<?php

class FrontendController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($username)
	{
		$user = User::where('username', $username)->first();

		if ( Auth::user()->username != $user->username && Auth::user()->role != 'admin'){
			return Response::make('Unauthorized', 401);
		}
		if( $user->projects()->count() == 1){
			return self::project($user->username, $user->projects->first()->slug);
		}
		return View::make('frontend.download_area')->with(array('user' => $user));
	}

	public function project($username, $projectSlug, $userfolderId = null){
		$user = User::where('username', $username)->first();

		if ( Auth::user()->username != $user->username && Auth::user()->role != 'admin'){
			return Response::make('Unauthorized', 401);
		}
		
		$project = Project::where('slug', $projectSlug)->first();

		$baseUserfolder = Userfolder::getProjectUserfolder($project->id, $user->id);
		if($userfolderId != null){
			$userfolder = Userfolder::find($userfolderId);
		} else {
			$defaultUserfolder = Userfolder::getProjectDefaultUserfolder($project->id, $user->id);
			$userfolder = $defaultUserfolder;
		} 
		 	
		if(isset($_GET['view-size'])){
			$backendData['viewSize'] = $_GET['view-size'];
		}
		elseif($userfolder->sorting){
			$backendData['viewSize'] = $userfolder->view_style;
		}else{
			$backendData['viewSize'] = 'm';
		}

		if(isset($_GET['sort-by'])){
			$backendData['sortBy'] = $_GET['sort-by'];
		}
		elseif($userfolder->sorting){
			$backendData['sortBy'] = $userfolder->sorting;
		}else{
			$backendData['sortBy'] = 'default';
		}

		if($backendData['sortBy'] == 'default')
			$files = $userfolder->customerFiles;
		if($backendData['sortBy'] == 'date')
			$files = $userfolder->customerFilesByDate;
		if($backendData['sortBy'] == 'title')
			$files = CustomerFilesController::sortByNaturalFileName($userfolder->customerFiles);
			// $files = $userfolder->customerFilesByFileName;

		$backendData['getHelper'] = '?view-size='.$backendData['viewSize'].'&sort-by='.$backendData['sortBy'];
		
		$data = array(
			'user'				=> $user,
			'project' 			=> $project,
			'baseUserfolder' 	=> $baseUserfolder,
			'currentUserfolder'		=> $userfolder,
			'files'				=> $files);

		$data = array_merge($backendData, $data);

		return View::make('frontend.download_area_project')->with($data);
	}

	/**
	 * Display a listing of the resource (alt).
	 *
	 * @return Response
	 */
	public function userarea($path = null)
	{
		$userFiles = Auth::user()->customerFiles();

		$data 					= array();
		$data['currentPath'] 	= $path;
		if($path != null){
			$path .= '/';
			$data['files'] = array();
			foreach ($userFiles as $userFile) {
				if ($userFile->path == Config::get('paths.baseDir') . '/' . $path) {
					$data['files'][] = $userFile;
				}
			}
		} 
		else {
			// $data['files']		= $userFiles;

			$data['files'] = array();
			foreach ($userFiles as $userFile) {
				if ($userFile->path == Config::get('paths.baseDir'). '/') {
					$data['files'][] = $userFile;
				}
			}
			$data['currentPath'] 	= 'Home';
		}

		$data['folderTree']		= array();

		foreach ($userFiles as $userFile) {
			if( $userFile->path != Config::get('paths.baseDir') ){
				$data['folderTree'][] = $userFile->path;
			}
		}
		$data['folderTree'] = CustomerFilesController::build_tree( $data['folderTree'] );
		if(!empty($data['folderTree'])){
			$data['folderTree'] = $data['folderTree'][ Config::get('paths.baseDir') ];
		}
		// echo '<pre>';
		// echo var_dump($userFiles);
		// echo '</pre>';
		
		return View::make('userarea')->with($data);
	}


}

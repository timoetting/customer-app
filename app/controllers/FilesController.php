<?php

use Illuminate\Filesystem\Filesystem;

class FilesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	public function updateFileList(){

	}

	public function initFileList(){
		$filesInFolders = driToListArray('costomerfiles');
		foreach ($filesInFolders as $key => $filePath) {
			$pathInfo = pathinfo($filePath);
			$file = new File;
			$file->title = basename($filePath);
			$file->filename = basename($filePath);
			$file->path = $filepath;
			$file->filetype = $pathInfo['extension'];
		}
	}

	private function dbFilesToListArray(){

	}

	private function dirToListArray($dir) { 

		$result = array(); 

		$cdir = scandir($dir); 
		foreach ($cdir as $key => $value) 
		{ 
			if (!in_array($value,array(".","..")) && substr($value, 0, 1) != '.') 
			{ 
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
				{ 
					$result = array_merge($result, dirToListArray($dir . DIRECTORY_SEPARATOR . $value)); 
				} 
				else 
				{ 
					$result[] = $dir . DIRECTORY_SEPARATOR . $value; 
				} 
			} 
		} 
		return $result; 
	} 


}

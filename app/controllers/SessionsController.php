<?php

class SessionsController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sessions.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$attempt = Auth::attempt([
			'username' => $input['username'],
			'password' => $input['password']
		]);

		if($attempt){
			if(Auth::user()->role == 'admin'){
				return Redirect::to('backend');
			}
			//return Redirect::to('userarea');
			$projectSlug = Auth::user()->projects()->first()->slug;
			return Redirect::route('downloadarea', array(Auth::user()->username, $projectSlug));
			// return Redirect::back();
		}
		return Redirect::to('login')->with('message', 'Fehler beim Login');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Redirect::to('login');
	}


}

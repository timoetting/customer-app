<?php

class UserfolderController extends \BaseController {


	public static function attachFileToUserfolder($file, $userfolder){
		//dd('fileId'.$file.'userfolderId'.$userfolder);
		if(is_string($file) || is_int($file))
			$file = CustomerFile::find($file);
		if(is_string($userfolder) || is_int($userfolder))
			$userfolder = Userfolder::find($userfolder);

		if(!$file->userfolders->contains($userfolder->id)){
			$user = $userfolder->user;
	
			//schauen, in welchem Userfolder des Nutzers sich die Datei befindet um sie daraus zu lösen und neu zu verknüpfen
			$fileUserfolders = $file->userfolders;
			$userUserfolders = $user->userfolders;
	
			$relatedUserfolder = $fileUserfolders->intersect($userUserfolders)->first();
			
			if(count($relatedUserfolder)!= 0){
				$file->userfolders()->detach($relatedUserfolder->id);
				if($file->hasVideoBrother()){	
					$file->videoBrother->userfolders()->detach($relatedUserfolder->id);
				}
			}
			$file->userfolders()->save($userfolder);
			if($file->hasVideoBrother()){	
				$file->videoBrother->userfolders()->save($userfolder);
			}
		}
	}

	public static function attachFilesToUserfolder(){
		// dd(Input::all());
		$files = CustomerFile::find(Input::get('fileIds'));
		$userfolder = Userfolder::find(Input::get('userfolderId'));

		foreach ($files as $file) {
			self::attachFileToUserfolder($file, $userfolder);
		}
	}

	public static function attachFolderToUserfolder(){
		// $postData = Input::all();
		$folderId = Input::get('folderId');
		$userfolderId = Input::get('userfolderId');
		$folder = Folder::find($folderId);
		$userfolder = Userfolder::find($userfolderId);
		$user = $userfolder->user;

		//schauen, in welchem Userfolder des Nutzers sich die Datei befindet um sie daraus zu lösen und neu zu verknüpfen
		$folderUserfolders = $folder->userfolders;
		$userUserfolders = $user->userfolders;

		$relatedUserfolder = $folderUserfolders->intersect($userUserfolders)->first();
		
		if(count($relatedUserfolder)!= 0){
			$folder->userfolders()->detach($relatedUserfolder->id);
		}
		$folder->userfolders()->save($userfolder);
	}

	public function sortUserfolder($userfolderId){
		$sortArray = unserialize(Input::get('sort'));

		$userfolder = Userfolder::find($userfolderId);

		$fileOrder = Input::get()['file'];

		$files = $userfolder->customerFiles;

		for ($i=0; $i < count($files); $i++) { 
			$newPos = array_search((string)$files[$i]->id, $fileOrder);
			$files[$i]->pivot->sort = $newPos;
			$files[$i]->pivot->save();
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	// /**
	//  * Show the form for creating a new resource.
	//  *
	//  * @return Response
	//  */
	// public function createNew($userId, $parentId = null)
	// {
	// 	return View::make('backend.forms.create_userfolder')->with(array('userId' => $userId, 'parentId' => $parentId));
	// }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.forms.create_userfolder');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::get();
		$userfolder = new Userfolder;
		$userfolder->title = $input['title'];
		//$userfolder->path = $input['path'];
		$userfolder->user_id = $input['current-user-id'];
		$userfolder->project_id = $input['current-project-id'];
		$userfolder->view_style = $input['view_style'];
		$userfolder->sorting = $input['sorting'];
		$userfolder->save();

		return Response::json($userfolder);

		//return View::make('backend.user', compact('user'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$userfolder = Userfolder::findOrFail($id);
		return View::make('backend.forms.update_userfolder', compact('userfolder'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$userfolder = Userfolder::findOrFail($id);

		$userfolder->fill(Input::all());
		$is_default = Input::get('is_default', false);
		// $is_default = 'hello';
		if($is_default == 1){
			$user = $userfolder->user;
			$project = $userfolder->project;
			$siblings = $user->userfoldersByProject($project);
			foreach ($siblings as $sibling) {
				if($sibling->id != $userfolder->id){
					$sibling->is_default = false;
					$sibling->save();
				}
			}
		}
		$userfolder->is_default = $is_default;
		$userfolder->save();

		return 'ok';

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		$userfolder = Userfolder::findOrFail($id);
		if($userfolder->is_main)
			return 'not allowed to delete main user folder.';
		$project = $userfolder->project;
		$user = $userfolder->user;
		$mainUserfolder = Userfolder::getProjectUserfolder($project->id, $user->id);
		$userfolderFiles = $userfolder->customerFiles;
		foreach ($userfolderFiles as $file) {
			UserfolderController::attachileToUserfolder($file, $mainUserfolder);
		}
		$userfolder->delete();
	}
}

<?php

class UserController extends \BaseController {

	// public function addToUsergroup($userId, $usergroupId){
	// 	$user = User::find($userId);
	// 	$usergroup = Usergroup::find($usergroupId);


	// 	if(!$usergroup->users->contains($user->id)){
	// 		$usergroup->users()->save($user);
	// 	}


	// 	$usergroup = Usergroup::find($usergroupId);

	// 	return View::make('backend.usergroup')->with('usergroup', $usergroup);
	// }

	public function showUserDetail($userId, $projectId = null, $userfolderId = null){
		$user = User::findOrFail($userId)/*->with('userfolders')*/;
		$files = null;
		$userfolders = null;
		$userfolder = null;

		if($projectId != null && $projectId != 'undefined'){
			$userfolders = $user->userfolders->filter(function($folder) use($projectId){
				return ($folder->project_id == $projectId);
			});
			$userfolders->load('children');
			
			$userfolder = null;
			if($userfolderId == null || $userfolderId == 'undefined'){
				// $userfolder = $userfolders->filter(function($folder){
				// 	return ($folder->parent_id == null);
				// })->first();
				$userfolder = Userfolder::getProjectUserfolder($projectId, $userId);
			} else {
				$userfolder = $userfolders->filter(function($folder) use($userfolderId){
					return ($folder->id == $userfolderId);
				})->first();
				// $userfolder = Userfolder::find($userfolderId);
			}
			if($userfolder == null){
				return 'nutzerordner ist null';
			}
			if($userfolder->user_id != $user->id) {
				return 'Nutzerordner und Nutzer passen nicht zusammen.';
			}
			$files = $userfolder->customerFiles;
			//if($userfolderId == null){
			$data = array(
				'files'			=> $files,
				'user'			=> $user,
				'userfolders' 	=> $userfolders,
				'activeUserfolder'	=> $userfolder);
			// return View::make('backend.detail_user', $data);
			$return = array(
				'html' => View::make('backend.detail_user', $data)->render(),
				'user' => $user,
				'userfolder' => $userfolder
			);

		} else{
			$data = array('user' => $user);
			$return = array(
				'html' => View::make('backend.datail_user_no_project', $data)->render(),
				// 'html' => 'nixda',
				'user' => $user,
				'userfolder' => null
			);
		}

		return Response::json($return);
		//}
	}



	public function connectUserToProject($userId, $projectId){

		$user = User::find($userId);
		$project = Project::find($projectId);
		if(!$user->hasProject($projectId)){
			$user->projects()->save($project);
		}
	}

	public function attachProject($userId, $projectId){
		connectUserToProject($userId, $projectId);
	}

	public function detachProject($userId = null, $projectId = null){
		$input = Input::get();
		if($userId == null)
			$userId = $input['userId'];
		if($projectId == null)
			$projectId = $input['projectId'];
		$project = Project::find($projectId);
		echo $project->title;
		$project->users()->detach($userId);
		$project->save();
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.forms.create_user');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::get();
		$user = new User;
		$user->username = $input['username'];
		$user->first_name = $input['first_name'];
		$user->last_name = $input['last_name'];
		$user->company = $input['company'];
		$user->password = Hash::make($input['password']);	
		$user->role = $input['role'];		
		$user->save();

		return View::make('backend.user', compact('user'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);
		return View::make('backend.detail_user', compact('user'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::findOrFail($id);

		return View::make('backend.forms.update_user', compact('user'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::findOrFail($id);

		// $user->fill(Input::all());
		// $user->save();

		$input = Input::get();
		$user->username = $input['username'];
		$user->first_name = $input['first_name'];
		$user->last_name = $input['last_name'];
		$user->company = $input['company'];
		if($input['password'] != '')
			$user->password = Hash::make($input['password']);
		$user->role = $input['role'];		
		$user->save();

		return View::make('backend.user', compact('user'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		$user = User::find($id);
		$user->userfolders->each(function($userfolder)
		{
		    $userfolder->delete();
		});

		$user->delete();
	}


}

<?php

class ProjectController extends \BaseController {



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.forms.create_project');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::get();
		$project = new Project;
		$project->title = $input['title'];
		$project->slug = Str::slug($input['title']);
		$project->folder = Config::get('paths.baseDir') . '/projects/' . $project->slug . DIRECTORY_SEPARATOR;
		$project->thumb = $input['thumb'];
		$project->color = trim($input['color'], '#');

		$project->meta_informations = array_filter(array_combine(Input::get('meta_info_titles'), Input::get('meta_info_contents')));

		$project->save();

		FoldersController::storeByPath($project->folder, $project, true);


		//return View::make('backend.user', compact('user'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project = Project::find($id);
		return View::make('backend.forms.update_project', compact('project'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$project = Project::find($id);
		$input = Input::get();
		$project->title = $input['title'];
		$project->slug = Str::slug($input['title']);
		$project->thumb = $input['thumb'];
		$project->color = trim($input['color'], '#');

		$project->meta_informations = array_filter(array_combine(Input::get('meta_info_titles'), Input::get('meta_info_contents')));

		$project->save();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public static function destroy($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public static function delete($id)
	{
		$project = Project::find($id);

		FoldersController::delete($project->projectFolder()->id);

		$project->delete();
	}


}

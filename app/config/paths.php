<?php 

return array( 

	'baseDir' => 'clientfiles',
	'projectsDir' => 'clientfiles/projects',
	'ffmpeg' => '/usr/local/Cellar/ffmpeg/2.2.4/bin/ffmpeg',
	'thumbnails' => 'thumbnails',
	'thumbnailsProjects' => 'thumbnails/projects/',
	'thumbnailsFilesMd' => 'thumbnails/files/md/',
	'thumbnailsFilesSm' => 'thumbnails/files/sm/',
	'thumbnailsFilesLg' => 'thumbnails/files/lg/',

);
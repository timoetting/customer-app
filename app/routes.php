<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('backend', function()
// {
// 	return View::make('backend/layout');
// });

Route::get('zipstream', function(){
	$file1 = CustomerFile::find(123);
	$file2 = CustomerFile::find(124);
	$filePath1 = public_path() . '/' . $file1->path . $file1->file_name;
	$filePath2 = public_path() . '/' . $file2->path . $file2->file_name;
	$zip = new ZipStream\ZipStream('example.zip');
	$zip->addFileFromPath($file1->file_name, $file1->path . $file1->file_name);
	$zip->addFileFromPath($file2->file_name, $file2->path . $file2->file_name);
	$zip->finish();
	echo "done";
});

Route::get('zipstream/{projectSlug}/{fileIds}', function($projectSlug, $fileIds){
	$fileIdArray = explode(",", $fileIds);
	$zip = new ZipStream\ZipStream($projectSlug.'.zip');
	array_filter($fileIdArray);
	foreach ($fileIdArray as $fileId) {
		$file = CustomerFile::find($fileId);
		$zip->addFileFromPath($file->file_name, $file->path . $file->file_name);
	}
	$zip->finish();
	var_dump($fileIdArray);
});

Route::get('symlink/{fileId}', function($fileId){
	$file = CustomerFile::find($fileId);
	$target = public_path() . '/' . $file->path . $file->file_name;
	$sessionId = Session::getId();
	$downloadDir = 'download/' . $sessionId . '/' . $file->id . '/';
	$downloadPath = public_path() . '/' . $downloadDir;
	if(!is_dir($downloadPath))
		mkdir($downloadPath, 0777, true);
	$link_name = $file->file_name;
	$link = $downloadPath . $link_name;
	if(is_file($link))
		unlink($link);
	echo Session::getName();
	symlink($target, $link);
	return link_to($downloadDir . $link_name) ;
});

Route::get('cleanupdl', function(){

});

Route::get('rewrite', function(){
	$files = CustomerFile::all();
	foreach ($files as $file) {
		$file->path =  Config::get('paths.baseDir') . substr($file->path, strlen('customerfiles'));
		$file->save();
	}

	return "done";
});

Route::get('/', function()
{
	return Redirect::route('backend');
});

// Route::get('backend', array('as' => 'backend', 'uses' => 'BackendController@showBackend'))->before('auth|backend');
// Route::get('backend/project/{id}', 'BackendController@showBackend')->before('auth|backend');
// 
Route::get('backend', array('as' => 'backend', 'uses' => 'BackendController@showDashboard', 'before' => 'auth|backend'));
Route::get('backend', array('as' => 'dashboard', 'uses' => 'BackendController@showDashboard', 'before' => 'auth|backend|updateFileSyste'));
Route::get('backend/project/{slug}', array('as' => 'project', 'uses' => 'BackendController@showProject', 'before' => 'auth|backend|updateFileSyste'));
Route::get('backend/project/{slug}/{username}/{userfolder?}', array('as' => 'user-in-project', 'uses' => 'BackendController@showUserInProject', 'before' => 'auth|backend|updateFileSyste'));

Route::get('backend/showFolderTree', 'BackendController@getFolderTreeView');

Route::get('downloadarea/{username}', array('as' => 'downloadarea', 'uses' => 'FrontendController@index'))->before('auth');
Route::get('downloadarea/{username}/{projectSlug}/{userfolderId?}', array('as' => 'downloadarea_project', 'uses' => 'FrontendController@project'))->before('auth');

// Route::get('writefolders', function()
// {
	
// 	echo '<pre>';
// 	var_dump(CustomerFilesController::dirToDirArray(Config::get('paths.baseDir')));
// 	echo '</pre>';
// });

Route::get('usertogroups', function()
{
	foreach (User::all() as $user) {
		$usergroup = new Usergroup;
    	$usergroup->title = $user->username . '_singlegroup';
    	// $usergroup->is_single_group = true;
    	$usergroup->save();

    	$user->usergroup()->save($usergroup);
        $user->usergroups()->save($usergroup);
	}
});
Route::get('userfolders/connect-file/{fileId}/to-userfolder/{userfolderId}', 'UserfolderController@connectFileToUserfolder')->before('auth|backend');
Route::get('userfolders/delete/{userfolderId}', 'UserfolderController@delete')->before('auth|backend');

Route::get('init-files', 'CustomerFilesController@syncFileEntries');

Route::get('folder/create', 'BackendController@createDirForm');
Route::post('folder/save', array('as' => 'savefolder', 'uses' => 'BackendController@createDir'));

Route::get('files/add-new-files', 'CustomerFilesController@syncFileEntries');
//Route::get('files/connect-to-user/{fileId}/{userId}', array('as' => 'connectFileToUser', 'uses' => 'CustomerFilesController@attachFileToUser', 'before' => 'auth|backend'));
Route::post('files/attach-to-user/{userId}', array('as' => 'attachFilesToUser', 'uses' => 'CustomerFilesController@attachFilesToUser', 'before' => 'auth|backend'));
Route::post('files/detach-from-user/{userId}', array('as' => 'detachFilesFromUser', 'uses' => 'CustomerFilesController@detachFilesFromUser', 'before' => 'auth|backend'));
Route::get('files/toggle-user-relation/{fileId}/{userId}', array('as' => 'toggleFileUserRelation', 'uses' => 'CustomerFilesController@toggleFileUserRelation', 'before' => 'auth|backend'));
// Route::get('files/connect-to-usergroup/{fileId}/{usergroupId}', 'CustomerFilesController@connectFileToUsergroup');
//Route::get('files/move-to-folder/{fileId}/{path?}', 'CustomerFilesController@moveFileToFolder')->where('path', '(.+)');
Route::post('files/move-to-folder', 'CustomerFilesController@moveFilesToFolder');
Route::get('files/move-to-folder/{fileId}/{folderId}', 'CustomerFilesController@moveFileToFolder');
Route::post('files/move-to-Userfolder', array('as' => 'moveFilesToUserfolder', 'uses' => 'UserfolderController@attachFilesToUserfolder', 'before' => 'auth|backend'));
// Route::post('files/move-to-folder', 'CustomerFilesController@moveFilesToFolder');
// Route::get('files/move-to-userfolder/{fileId}/{userfolderId}', 'UserfolderController@attachFileToUserfolder');
// Route::post('file/move-to-userfolder', 'UserfolderController@attachFilesToUserfolder');
// Route::post('file/move-to-userfolder', function(){
// 	return 'halo';
// });
Route::get('files/path/{path?}', 'CustomerFilesController@inPath')->where('path', '(.+)');
Route::post('files/delete/{id}', array('as' => 'deleteFile', 'uses' => 'CustomerFilesController@delete', 'before' => 'auth|backend'));
Route::post('files/multiDelete', array('as' => 'multiDeleteFiles', 'uses' => 'CustomerFilesController@multiDelete', 'before' => 'auth|backend'));

Route::get('file/{id}', array('as' => 'file', 'use' => 'CustomerFilesController@getDownloadLink'))->before('downloadable');

Route::post('folders/collapse/{folderId}/{userId}', 'FoldersController@collapse');
Route::post('folders/decollapse/{folderId}/{userId}', 'FoldersController@decollapse');
Route::get('folders/move-to-folder/{folder}/{newParent}', 'FoldersController@moveFolderToFolder');
Route::get('folders/connect-to-user/{folderId}/{userId}', array('as' => 'connectFolderToUser', 'uses' => 'FoldersController@attachFolderToUser', 'before' => 'auth|backend'));
Route::get('folders/toggle-user-relation/{fileId}/{userId}', array('as' => 'toggleFolderUserRelation', 'uses' => 'FoldersController@toggleFolderUserRelation', 'before' => 'auth|backend'));
Route::get('folders/delete/{folderId}', array('as' => 'deleteFolder', 'uses' => 'FoldersController@delete', 'before' => 'auth|backend'));
Route::post('folders/attach-to-userfolder', array('as' => 'attachFolderToUserfolder', 'uses' => 'UserfolderController@attachFolderToUserfolder', 'before' => 'auth|backend'));

Route::get('projects/delete/{projectId}', array('as' => 'deleteProject', 'uses' => 'ProjectController@delete', 'before' => 'auth|backend'));
Route::get('users/delete/{userId}', array('as' => 'deleteUser', 'uses' => 'UserController@delete', 'before' => 'auth|backend'));


Route::post('thumb/upload', array('as' => 'uploadThumb', 'uses' => 'ThumbnailController@saveFileThumb', 'before' => 'auth|backend'));
Route::get('thumb-from-video/{fileId}/{seconds}', array('as' => 'captureThumb', 'uses' => 'ThumbnailController@thumbFromVideo', 'before' => 'auth|backend'));


Route::get('file-tree/{projectId}/{view_size?}/{sorting?}', array('as' => 'file-tree', 'before' => 'auth|backend', function($projectId, $view_size = null, $sorting = null){
// Route::get('file-tree/{projectId}', array('as' => 'file-tree', function($projectId){
	$project = Project::find($projectId);
	$treeData['project'] = $project;
	$treeData['users'] = $project->users;
	$treeData['folder'] = $project->projectFolder();
	$treeData['parent'] = $project->projectFolder();
	$treeData['view_size'] = ($view_size) ? $view_size : 's';
	$treeData['sorting'] = ($sorting) ? $sorting : 'file_name';
	$treeData['authUser'] = Auth::user()->load('collapsedFolders');
	//$treeData['depth'] = 0;
	return View::make('backend.file_tree', $treeData);
	// return 'hollo';
}));
Route::get('file-tree-user/{projectId}/{userId}/{view_size?}/{sorting?}', array('as' => 'file-tree-user', 'before' => 'auth|backend', function($projectId, $userId, $view_size = null, $sorting = null){
// Route::get('file-tree/{projectId}', array('as' => 'file-tree', function($projectId){
	$project = Project::find($projectId);
	$treeData['user'] = User::find($userId);
	$treeData['folder'] = $project->projectFolder();
	$treeData['parent'] = $project->projectFolder();
	$treeData['view_size'] = ($view_size) ? $view_size : 's';
	$treeData['sorting'] = ($sorting) ? $sorting : 'file_name';
	$treeData['authUser'] = Auth::user()->load('collapsedFolders');
	//$treeData['depth'] = 0;
	return View::make('backend.file_tree_user', $treeData);
	// return 'hollo';
}));
Route::get('file-tree-userfolder/{projectId}/{userId}/{userfolderId}/{viewSize?}/{sorting?}', array('as' => 'file-tree-userfolder', 'before' => 'auth|backend', function($userfolderId, $viewSize = null){
// Route::get('file-tree/{projectId}', array('as' => 'file-tree', function($projectId){
	if($userfolderId != 0){ //mainFolder
		$treeData['userfolder'] = Userfolder::find($userfolderId);
	}else{
		$treeData['user'] = User::find($userId);
		$treeData['userfolder'] = null;
	}
	$treeData['viewSize'] = ($viewSize != null) ? $viewSize : 's'; 
	return View::make('backend.file_tree_userfolder', $treeData);
	// return 'hollo';
}));

Route::post('userfolder-sort/{userfolderId}', 'UserfolderController@sortUserfolder');

Route::get('dashboard/project/{id}', function($id){
	return View::make('backend.dashboard.project')->with('project', Project::find($id));
});
Route::get('dashboard/user/{id}', function($id){
	return View::make('backend.dashboard.user')->with('user', User::find($id));
});



//Route::get('users/add-to-usergroup/{userId}/{usergroupId}', 'UserController@addToUsergroup');
Route::get('users/connect/{userId}/toproject/{projectId}', 'UserController@connectUserToProject');
Route::get('user/{userId}/{projectId}/{userfolderId}', 'UserController@showUserDetail');
Route::post('users/detachproject', 'UserController@detachProject');
// Route::get('user/{userId}', 'UserController@showUserDetail');
Route::get('json-user/{id}', function($id){
	$user = User::find($id);
	return Response::json( $user );
});

Route::get('json-userfolder/{id}', function($id){
	$userfolder = Userfolder::find($id);
	return Response::json( $userfolder );
});

Route::post('upload-handler/{folderId}', 'CustomerFilesController@uploadHandler');

Route::get('userarea/{path?}', array('as' => 'userarea', 'uses' => 'FrontendController@userarea'))->where('path', '(.+)')->before('auth');



Route::get('login', 'SessionsController@create');
Route::get('logout', array('as' => 'logout', 'uses' => 'SessionsController@destroy'));

// Route::get('foldering', function(){
// 	$users = User::all();
// 	foreach ($users as $user) {
// 	    $userfolder = new Userfolder;
// 		$userfolder->title = 'Home';
// 		$userfolder->save();

// 		$user->userfolders()->save($userfolder);
// 	}
// 	echo 'fertig';
// });

//gibt einem den gesamten app-block
// Route::get('block/usersofproject/{projectId}', function($projectId){
// 	$project = Project::find($projectId);
// 	$users = $project->users;
// 	return View::make('backend.app_col_block_users', compact('users'));
// });

Route::get('block_detail/user/{userId}', function($userId){
	$project = Project::find($projectId);
	$users = $project->users;
	return View::make('backend.app_col_block_users', compact('users'));
});

Route::resource('userfolders', 'UserfolderController');
Route::resource('usergroups', 'UsergroupController');
Route::resource('users', 'UserController');
Route::resource('files', 'CustomerFilesController');
Route::resource('folders', 'FoldersController');
Route::resource('projects', 'ProjectController');

Route::resource('sessions', 'SessionsController', array('create', 'store', 'destroy'));




//NUR MAL EBEN///////////////////////////
//
Route::get('test/{fileId}/{userId}', function($fileId, $userId)
{
	$file = CustomerFile::find($fileId);
	// $user = User::find($userId);
	return Response::json($file->hasUser($userId));

});

Route::get('test', function()
{
	echo '<h2>Testlink Routed</h2><a href="'.URL::to('testfile').'" class="btn download-button">Download</a><h2>PHP Info</h2>';
	phpinfo();

});

Route::get('testfile', function(){
	// $file = CustomerFile::find(588);
	$file = CustomerFile::find(159);
	CustomerFilesController::sendFile($file->path . $file->file_name);
	// echo '<pre>';
	// var_dump(Response::download($file->path . $file->file_name));
	// echo '</pre>';
});

Route::get('clientfiles/{path?}', function($path){
	return 'clientfile';
})->where('path', '(.+)');;

Route::get('connectfilestouser', function(){
	$files = CustomerFile::all();
	$folders = Folder::all();

	foreach ($files as $file) {
		//$file->folder = Folder::where('path', $file->path)->first();
		$the_folder = $folders->filter(function($folder) use ($file){
			return ($folder->path . $folder->title . DIRECTORY_SEPARATOR == $file->path);
		})->first();
		if($the_folder != NULL){
			$file->folder_id = $the_folder->id;
			$file->save();
		}
	}

	echo 'durch';
});


Route::get('setprojectstofolders', function(){
	$folders = Folder::whereNotNull('project_id')->get();

	foreach ($folders as $folder) {
		setChildFoldersProjects($folder, $folder->parent_id);
	}

});

function setChildFoldersProjects($folder, $projectId){
	$children = $folder->children;
	foreach($children as $child){
		echo $child;
		$child->project_id = $projectId;
		$child->save();

		setChildFoldersProjects($child, $projectId);
	}
}

Route::get('video/{fileId}/{qt?}', function($fileId, $qt = null)
{
	$file = CustomerFile::find($fileId);
	$data = array();
	$data['file'] = $file;
	$data['qt'] = $qt;
	return View::make('htmlVideo')->with($data);
});








<?php
class Video extends CustomerFile {

	protected $guarded = array('id', 'created_at', 'updated_at');

	public function files(){
		return $this->hasMany('CustomerFile');
	}

	public function thumbMd(){
		return Config('paths.thumbnailsVideoMd').$this->thumb;
	}

	public function thumbSm(){
		return Config('paths.thumbnailsVideoSm').$this->thumb;
	}
}
<?php
class Project extends Eloquent {

	protected $guarded = array('id', 'created_at', 'updated_at');

	public function users(){
		return $this->belongsToMany('User')->orderBy('username');
	}

	public function userfolders(){
		return $this->hasMany('Userfolder');
	}

	public function getMetaInformationsAttribute($value){
		return unserialize($value);
	}

	public function setMetaInformationsAttribute($value){
		$this->attributes['meta_informations'] = serialize($value);
	}

	public function files(){
		return CustomerFile::where('path', Config::get('paths.projectsDir') . '/' . $this->slug . '/')->get();
	}

	public function folders(){
		return $this->hasMany('Folder');
	}

	public function projectFolder(){
		$folders = $this->folders;
		return $folders->filter(function($folder){
			return ($folder->is_project_folder);
		})->first();
	}

	public static function boot()
    {
        parent::boot();

        static::created(function($project)
        {
        	// if(!File::isDirectory($project->folder)){
        	// 	File::makeDirectory($project->folder, 0777, true);
        	// }
        	
        	$users = User::all();
        	foreach ($users as $user) {
	            $userfolder = new Userfolder;
	        	$userfolder->title = 'Main';
	        	$userfolder->is_main = true;
        		$userfolder->user()->associate($user);
        		$userfolder->project()->associate($project);
	        	$userfolder->save();
        		// $user->userfolders()->save($userfolder);  
        	}
        });
    }
}
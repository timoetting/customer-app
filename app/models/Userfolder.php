<?php
class Userfolder extends Eloquent {

	protected $guarded = array('id', 'created_at', 'updated_at');
	
	public function customerFiles(){
		return $this->belongsToMany('CustomerFile')->withTimestamps()
		->withPivot('sort')
		->orderBy('sort', 'asc');
	}

	public function customerFilesByTitle(){
		return $this->belongsToMany('CustomerFile')
		->orderBy('title', 'asc');
	}

	public function customerFilesByFileName(){
		return $this->belongsToMany('CustomerFile')
		->orderBy('file_name', 'asc');
	}

	public function customerFilesByDate(){
		return $this->belongsToMany('CustomerFile')->withTimestamps()
		->withPivot('created_at')
		->orderBy('pivot_created_at', 'desc');
	}

	public function user(){
		return $this->belongsTo('User');
	}

	public function parent(){
		return $this->belongsTo('Userfolder', 'parent_id');
	}

	public function project(){
		return $this->belongsTo('Project');
	}

	public function children(){
		return $this->hasMany('Userfolder', 'parent_id');
	}

	public function sorting(){
		if(isset($this->sorting))
			return $this->sorting;
		else
			return 'default';
	}

	public function view_style(){
		if(isset($this->view_style))
			return $this->view_style;
		else
			return 'm';
	}

	public function is_default(){
		if(isset($this->is_default))
			return $this->is_default;
		else
			return false;
	}

	public static function getProjectUserfolder($projectId, $userId){
		// $project = Project::find($projectId);
		$user = User::find($userId);

		$userfolders = $user->userfolders;
		// echo '<pre>';
		// foreach ($userfolders as $userfolder) {
		// 	if($userfolder->project != null)
		// 	var_dump(($userfolder->project->id == $projectId && 
	 //    		$userfolder->is_main));
		// }
		// dd();

		$projectUserfolder = $userfolders->filter(function($userfolder) use ($projectId)
	    {
	    	if($userfolder->project != null)
	    		return ($userfolder->project->id == $projectId && 
	    			$userfolder->is_main);
	    })->first();

    if($projectUserfolder == null){
    	$projectUserfolder = $userfolders->filter(function($userfolder) use ($projectId)
    	{
    	if($userfolder->project != null)
    		return ($userfolder->project->id == $projectId);
    	})->first();
    }

    return $projectUserfolder;
	}

		public static function getProjectDefaultUserfolder($projectId, $userId){
		$user = User::find($userId);

		$userfolders = $user->userfolders;

		$projectUserfolder = $userfolders->filter(function($userfolder) use ($projectId)
	    {
	    	if($userfolder->project != null)
	    		return ($userfolder->project->id == $projectId && 
	    			$userfolder->is_default);
	    })->first();

    if($projectUserfolder == null){
    	$projectUserfolder = $userfolders->filter(function($userfolder) use ($projectId)
    	{
    	if($userfolder->project != null)
    		return ($userfolder->project->id == $projectId);
    	})->first();
    }

    return $projectUserfolder;
	}

	/**
	 * if folder is attached to userfolder of selected user, return this
	 * @param  [type] $folder [description]
	 * @param  [type] $user   [description]
	 * @return [type]         [description]
	 */
	public static function getUserfolderOfFolder($folder, $user){
		if(is_string($folder) || is_int($folder))
			$folder = Folder::find($folder);
		if(is_string($user) || is_int($user))
			$user = User::find($user);

		$folderUserfolders = $folder->userfolders;
		$userUserfolders = $user->userfolders;

		return $folderUserfolders->intersect($userUserfolders)->first();

	}

	// public static function boot()
 //    {
 //        parent::boot();

 //        static::creating(function($userfolder)
 //        {
 //            if ( Userfolder::where('user_', 100)->count()) 
 //            	return false;
 //        });

 //        // $picture = Picture::whereHas('gallery', function($q) use ($galleryPath)
 //        // {
 //        //     $q->where('path', $galleryPath);

 //        // })->first();
 //    }
}
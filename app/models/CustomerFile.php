<?php

class CustomerFile extends Eloquent {

	protected $guarded = array('id', 'created_at', 'updated_at');
	
	public function usergroups(){
		return $this->belongsToMany('Usergroup');
	}

	public function userfolders(){
		return $this->belongsToMany('Userfolder')->withTimestamps();
	}

	public function users(){
		return $this->belongsToMany('User');
	}

	public function folder(){
		return $this->belongsTo('Folder');
	}

	public function project(){
		return $this->folder->project;
	}

	public function videoBrother(){
		return $this->belongsTo('CustomerFile', 'video_brother');
	}

	public function hasVideoBrother(){
		return false;
		//return count($this->video_brother);
	}

	public function isVideo(){
		return ($this->file_type == 'mp4');
	}

	public function attachVideoBrother($brother){
		if(is_int($brother))
			$brother = CustomerFile::find($brother);
		$this->video_brother = $brother->id;
		$brother->video_brother = $this->id;
		$this->save();
		$brother->save();

		foreach($brother->users as $brotherUser){
			$this->users()->save($brotherUser);
		}
		foreach($this->users as $user){
			$brother->users()->save($user);
		}
	}

	public function attachUser($user, $iterateVideo = true){
		if(is_int($user) || is_string($user))
			$user = User::findOrFail($user);
		// var_dump($user->id);
		// if(count($this->project()) != 0){
		$userfolder = Userfolder::getUserfolderOfFolder($this->folder, $user);
		if(count($userfolder) == 0)
			$userfolder = Userfolder::getProjectUserfolder($this->project()->id, $user->id);
		// echo $userfolder->title;
		
		if(!$this->userfolders->contains($userfolder->id))
			UserfolderController::attachFileToUserfolder($this, $userfolder);
		// }
		if(!$this->hasUser($user->id)){
			$this->users()->save($user);
		}

		// if($iterateVideo && $this->hasVideoBrother())
		// 	$this->videoBrother->attachUser($user, false);	
	}

	public function detachUser($user, $iterateVideo = true){
		if(is_int($user))
			$user = User::find($user);

		$this->users()->detach($user->id);

		$this->folder->users()->detach($user->id);
		$parentFolders = $this->folder->parents();
		foreach ($parentFolders as $parentFolder) {
			$parentFolder->users()->detach($user->id);
		}

		$userfolders = $user->userfolders;
		foreach ($userfolders as $userfolder) {
			$userfolder->customerFiles()->detach($this->id);
		}

		if($this->hasVideoBrother() && $iterateVideo)
			$this->detachUser($this->videoBrother, false);

		return $this;

	}

	static public function filterFilesByUser($files, $user){
		return $files->filter(function($file) use($user){
			return ($user->hasFile($file->id));
		});
	}

	public function hasUser($user){
		$id = $user;
	    // return ! $this->users->filter(function($fileUser) use ($id)
	    // {
	    //     return $fileUser->id == $id;
	    // })->isEmpty();
	    return ($this->users->contains($id));
	}

	public function fullPath(){
		return $this->path.$this->file_name;
	}

	public function thumbSm(){
		if($this->thumb)
			return URL::to('/') . '/' . Config::get('paths.thumbnailsFilesSm') . $this->thumb;
		else
			return false;
	}

	public function thumbMd(){
		if($this->thumb)
			return URL::to('/') . '/' . Config::get('paths.thumbnailsFilesMd') . $this->thumb;
		else
			return false;
	}

	public function thumbLg(){
		if($this->thumb)
			return URL::to('/') . '/' . Config::get('paths.thumbnailsFilesLg') . $this->thumb;
		else
			return false;
	}

	public function htmlVideo(){
		if($this->isVideo()){
			return View::make('htmlVideo', $this);
		}
		return false;
	}

	public function getTemporaryDownloadUrl(){
		$target = public_path() . '/' . $this->path . $this->file_name;
		$sessionId = Session::getId();
		$downloadDir = 'download/' . $sessionId . '/' . $this->id . '/';
		$downloadPath = public_path() . '/' . $downloadDir;
		if(!is_dir($downloadPath))
			mkdir($downloadPath, 0777, true);
		touch(public_path() . '/download/' . $sessionId);
		$link_name = $this->file_name;
		$link = $downloadPath . $link_name;
		if(is_link($link))
			unlink($link);
		symlink($target, $link);
		return $downloadDir . $link_name ;
	}

	public function getFileName(){
		return $this->attributes['file_name'];
	}

	public function setFileName($value){
		rename ($this->attributes['path'] . $this->attributes['file_name'],
		 $this->attributes['path'] . $value);
		$this->attributes['file_name'] = $value;
	}



}
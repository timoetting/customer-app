<?php
class Folder extends Eloquent {

	protected $guarded = array('id', 'created_at', 'updated_at');
	
	public function customerFiles(){
		return $this->hasMany('CustomerFile');
	}

	public function customerFilesByTitle(){
		return $this->hasMany('CustomerFile')
		->orderBy('title', 'asc');
	}

	public function customerFilesByFileName(){
		return $this->hasMany('CustomerFile')
		->orderBy('file_name', 'asc');
	}

	public function customerFilesByDate(){
		return $this->hasMany('CustomerFile')
		->orderBy('created_at', 'asc');
	}

	public function customerFilesDeep(){
		// return $this->hasMany('CustomerFile');
	}

	public function users(){
		return $this->belongsToMany('User');
	}

	public function collapsedUsers(){
		return $this->belongsToMany('User', 'collapsed_folders');
	}

	public function isCollapsed($userId){
		return $this->collapsedUsers->contains($userId);
	}

	public function userfolders(){
		return $this->belongsToMany('Userfolder');
	}

	/**
	 * user is watchfolder, if user is connected. therefore, all folders and files under this folder are also connected to this user
	 * @param  int/object $user 
	 * @return [type]       [description]
	 */
	public function attachUser($user){
		if(is_int($user))
			$user = User::find($user);

		$this->users()->save($user);

		//$subFolders = $this->childrenDeep();
		$childFolders = Folder::where('path', 'like', $this->fullPath().'%')->get();
		foreach ($childFolders as $childFolder) {
			// $subFolder->users()->save($user);
			$childFolder->attachUser($user);
		}

		$files = CustomerFile::where('path', 'like', $this->fullPath().'%')->get();
		//dd('lol');
		foreach ($files as $file) {
			$file->attachUser($user);
			// $file->users()->save($user);
		}
		return $this;
	}


	public function detachUser($user){
		if(is_int($user))
			$user = User::find($user);

		$this->users()->detach($user->id);

		$parentFolders = $this->parents();
		foreach ($parentFolders as $parentFolder) {
			$parentFolder->users()->detach($user->id);
		}
		return $this;

	}

	/**
	 * detaches Users from Folder and all Files under this Folder
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function detachAll($user){
		if(is_int($user))
			$user = User::find($user);

		$this->users()->detach($user->id);

		$parentFolders = $this->parents();
		foreach ($parentFolders as $parentFolder) {
			$parentFolder->users()->detach($user->id);
		}

		$childFolders = Folder::where('path', 'like', $this->fullPath().'%')->get();
		foreach ($childFolders as $childFolder) {
			$childFolder->detachUser($user);
		}

		$files = CustomerFile::where('path', 'like', $this->fullPath().'%')->get();
		foreach ($files as $file) {
			$file->detachUser($user);
		}

		return $this;

	}



	public function parent(){
		return $this->belongsTo('Folder', 'parent_id');
	}

	public function parents(){
		$result = array(); 

		$parent = $this->parent;
		// foreach ($children as $child) 
		// { 
		if($parent != null){
			$result = array_merge($result, $parent->parents()); 
			$result[] = $parent; 
		}
		// } 
		return $result; 
	}

	public function hasParent(){
		return ($this->parent != null);
	}

	public function children(){
		return $this->hasMany('Folder', 'parent_id')->orderBy('title', 'asc');;
	}

	public function childrenDeep(){
		$result = array(); 

		$children = $this->children;
		foreach ($children as $child) 
		{ 
			$result = array_merge($result, $child->childrenDeep()); 
			$result[] = $child; 
		} 
		return $result; 
	}

	public static function getFolderByPath($path){
		$parentPath = dirname($path) . DIRECTORY_SEPARATOR;
		$title = basename($path);
		// dd("path = '" . $parentPath . "' and title = '" . $title ."'");
		$folder = Folder::whereRaw("path = '" . $parentPath . "' and title = '" . $title ."'")->get()->first();
		// 
		return $folder;
	}

	public function project(){
		return $this->belongsTo('Project');
	}

	public function fullPath(){
		return $this->path . $this->title . DIRECTORY_SEPARATOR;
	}

	public function isInWatchFolder($user){
		if(is_int($user))
			$user = User::find($user);

		$parents = $this->parents();
		foreach($parents as $parent){
			if($parent->hasUser($user->id)){
				return true;
			}
		}

		return false;
	}

	public function hasUser($id)
	{
	    return ! $this->users->filter(function($user) use ($id)
	    {
	        return $user->id == $id;
	    })->isEmpty();
	}
}
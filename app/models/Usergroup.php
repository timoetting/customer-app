<?php
class Usergroup extends Eloquent {

	protected $guarded = array('id', 'created_at', 'updated_at');
	
	public function customerFiles(){
		return $this->belongsToMany('CustomerFile');
	}

	public function users(){
		return $this->belongsToMany('User');
	}
}
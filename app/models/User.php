<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $guarded = array('id', 'created_at', 'updated_at');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function name(){
		if($this->first_name != NULL && $this->last_name != NULL){
			return $this->first_name . ' ' . $this->last_name;
		} else {
			return $this->username;
		}
	}

	public function usergroups(){
		return $this->belongsToMany('Usergroup')->orderBy('title');
	}

	public function userfolders(){
		return $this->hasMany('Userfolder');
	}

	public function userfoldersByProject($project){
		if(is_int($project))
			$project = Project::find($project);

		$id = $this->id;
		$userfolders = $project->userfolders()->orderBy('title')->get()->filter(function($userfolder) use ($id){
			return ($userfolder->user_id == $id);
		});

		return $userfolders;
		//return $project->userfolders->intersect($this->userfolders);
		
	}

	public function projects(){
		return $this->belongsToMany('Project');
	}

	public function folders(){
		return $this->belongsToMany('Folder');
	}

	public function collapsedFolders(){
		return $this->belongsToMany('Folder', 'collapsed_folders');
	}

	public function customerFiles(){

		return $this->belongsToMany('CustomerFile');

		// $customerFiles;
		// $usergroups = $this->usergroups;
		// $usergroups->load('customerFiles');
		// foreach ($usergroups as $usergroup){
		// 	$customerFiles = (empty($customerFiles)) ? $usergroup->customerFiles : $customerFiles->merge($usergroup->customerFiles);
		// 	// foreach ($usergroup->customerFiles as $file) {
		// 	// 	$customerFiles = (empty($customerFiles)) 
		// 	// }
		// }
		// return $customerFiles;
	}

	// public function customerFiles(){
	// 	$files = $this->customerFiles;
	// }

	/**
	 * Die Gruppe, wo nur der eine User drin ist (wird für rechtemanagement verwendet)
	 */

	public function usergroup(){
		return $this->hasOne('Usergroup');
	}

	public function hasProject($id)
	{
	    return ! $this->projects->filter(function($project) use ($id)
	    {
	        return $project->id == $id;
	    })->isEmpty();
	}

	public function hasFile($id)
	{
	    return ! $this->customerFiles->filter(function($file) use ($id)
	    {
	        return $file->id == $id;
	    })->isEmpty();
	}

	public static function boot()
    {
        parent::boot();

        static::created(function($user)
        {
        	$projects = Project::all();
        	foreach ($projects as $project) {
	            $userfolder = new Userfolder;
	        	$userfolder->title = 'Main';
	        	$userfolder->is_main = true;
        		$userfolder->user()->associate($user);
        		$userfolder->project()->associate($project);
	        	$userfolder->save();
        		// $user->userfolders()->save($userfolder);  
        	}
        });
    }


}

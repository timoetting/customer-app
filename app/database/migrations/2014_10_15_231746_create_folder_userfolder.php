<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFolderUserfolder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('folder_userfolder', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
	        $table->integer('folder_id');
	        $table->integer('userfolder_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('folder_userfolder');
	}

}

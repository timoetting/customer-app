<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerFileUserfolder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_file_userfolder', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
	        $table->integer('customer_file_id');
	        $table->integer('userfolder_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_file_userfolder');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIsDefaultToUserfolders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('userfolders', function(Blueprint $table)
		{
			$table->boolean('is_default')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('userfolders', function(Blueprint $table)
		{
			Schema::drop('is_default');
		});
	}

}

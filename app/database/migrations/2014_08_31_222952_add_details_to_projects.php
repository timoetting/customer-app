<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDetailsToProjects extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->string('thumb_path')->nullable();
			$table->string('product')->nullable();
			$table->string('production')->nullable();
			$table->string('agency')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->dropColumn('thumb_path');
			$table->dropColumn('product');
			$table->dropColumn('production');
			$table->dropColumn('agency');
		});
	}

}

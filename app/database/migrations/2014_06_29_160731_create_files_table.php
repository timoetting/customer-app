<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_files', function($table)
	    {
	        $table->increments('id');
	        $table->string('title');
	        $table->string('file_name');
	        $table->string('path');
	        $table->string('thumb_path')->nullable();
	        $table->string('file_type')->nullable();
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_files');
	}

}

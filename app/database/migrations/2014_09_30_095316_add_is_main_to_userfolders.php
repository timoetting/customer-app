<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIsMainToUserfolders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('userfolders', function(Blueprint $table)
		{
			$table->boolean('is_main')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('userfolders', function(Blueprint $table)
		{
			$table->dropColumn('sort');
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddThumbToCustomerFiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customer_files', function(Blueprint $table)
		{
			$table->string('thumb')->nullable();
			$table->dropColumn('thumb_path');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customer_files', function(Blueprint $table)
		{
			$table->dropColumn('thumb');
			$table->string('thumb_path')->nullable();
		});
	}

}

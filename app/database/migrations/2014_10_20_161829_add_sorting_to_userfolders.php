<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSortingToUserfolders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('userfolders', function(Blueprint $table)
		{
			$table->string('sorting')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('userfolders', function(Blueprint $table)
		{
			$table->dropColumn('sorting');
		});
	}

}

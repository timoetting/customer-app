<html>
<head>
	<meta charset="utf-8"> 
	<title>Downloadbereich</title>
	{{ HTML::style('stylesheets/bootstrap.css') }}
	{{ HTML::style('stylesheets/icon-style.css') }}
	{{ HTML::style('stylesheets/bootstrap.css') }}
	{{ HTML::style('stylesheets/style.css') }}
	<!-- <link rel="stylesheet" href="{{ asset('stylesheets/bootstrap.css') }}">
	<link rel="stylesheet" href="stylesheets/icon-style.css">
	<link rel="stylesheet" href="stylesheets/bootstrap.css">
	<link rel="stylesheet" href="stylesheets/style.css"> -->
	<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>

<div class="container userarea">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1>Downloadbereich</h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-3">
							<ul class="userarea-nav">
								<li><a class="homepage-link" href="{{URL::route('userarea')}}" title="zur zur Startseite"><i class="icon-home2"></i> Startseite</a></li>
								{{drawFolderTree($folderTree, $currentPath, '')}}
							</ul>
						</div>
						<div class="col-md-9">
							<h2>{{ basename($currentPath) }}</h2>
							@if( count($files) > 0 )
							<ul class="list-group file-list">
								@foreach ($files as $file) 

									<li class="list-group-item">{{ link_to_route('file', $file->title, array('id' => $file->id ), array('title' => $file->file_name) ) }}</li>

								@endforeach
							</ul>
							@else
							<div class="well">
								Dieser Bereich stellt Ihnen verschiedene Dateien zum Download zur Verfügung. Diese Dateien sind in Unterbereichen organisiert, welche links ausgewählt werden können.
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

</body>

<?php 

function drawFolderTree($dirArray, $currentPath, $path){
	foreach ($dirArray as $dirName => $value) { 
		$activeClass = ($currentPath == $path.$dirName) ? 'active' : ''; ?>
		@if ($value == null)
		    <li><a href="{{URL::route('userarea')}}/{{ $path.$dirName }}" data-path="{{$path.$dirName}}" class="{{$activeClass}}">{{$dirName}}</a></li>
		@else
			<li class="has-child opened"><a href="{{URL::route('userarea')}}/{{ $path.$dirName }}" data-path="{{$path.$dirName}}/" class="{{$activeClass}}"></i>{{$dirName}}</a>
			<ul class="userarea-nav">
				<?php drawFolderTree($value, $currentPath, $path.$dirName.'/') ?>
			</ul>
			</li>
		@endif
	<?php }
}

?>
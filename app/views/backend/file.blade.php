<div class="file entry" data-id="{{$file->id}}">
	<div class="thumb">
		@if($file->thumb_path == null || empty($file->thumb_path))
		<div class="icon">
			<i class="icon-file4"></i>
		</div>
		<div class="file-type">
			{{$file->file_type}}
		</div>
		@else

		<img src="{{URL::to('/thumbnails/'.$file->thumb_path)}}" alt="">
		@endif
	</div>
	<div class="file-info">
		<div class="title">{{$file->title}}</div> 
		<div class="file-name">{{$file->file_name}}</div> 
	</div>
	<div class="control">
		<a href="{{route('file', array('id' => $file->id))}}" title="herunterladen" class="btn-clear download"><i class="icon-inbox"></i></a>
		<button class="btn-clear edit" title="Dateieintrag bearbeiten" data-id="{{$file->id}}"><i class="icon-menu"></i></button>
	</div>
	<div class="clear"></div>
</div>
<div class="app-col-header">
	<h2 class="app-col-title">{{$user->username}} <div class="secondary">{{$user->first_name}} {{$user->last_name}}</div></h2>
	<div class="tool-bar">
		<button class="btn btn-xs btn-primary" title="aktuelles Projekt bearbeiten">bearbeiten</button>
		<button class="btn btn-xs btn-primary" title="aktuelles Projekt löschen">löschen</button>
	</div>
</div>
<div class="userfolders app-col-body">
	<h4>Benutzerordner</h4>
	<ul class="folder-tree">
	<?php 
		$topFolder = $userfolders->filter(function($folder)
	    {
	    	//echo $folder->parent()->count();
	        return ($folder->parent()->count() == 0);
	    })->first();
	 ?>
		@include('backend.userfolder_recursive', array('userfolder' => $topFolder))
	</ul>
</div>
<div class="files-container app-col-body">
	@foreach($files as $file)
		@include('backend.file', array('file', $file))
	@endforeach
</div>
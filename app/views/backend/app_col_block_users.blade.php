@extends('backend.app_col_block')


@section('classname')
	user-block user-manager
@overwrite	

@section('opened')
	
@overwrite	



@section('list-header')
	<h2 class="app-col-title">
	Benutzer
	</h2>
	<div class="tool-bar">	
		<button class="tip-bottom tool-bar-btn create create-user" title="Benutzer hinzufügen"><i class="icon-plus"></i></button>
	</div>
@overwrite	

@section('list-body')
	<ul class="entry-list user-list">
		@foreach($users as $user)
		<li>
			<div class="user entry" data-id="{{$user->id}}">
				<div class="icon"><i class="icon-user"></i></div> {{$user->username}} <span class="secondary name">{{$user->first_name}} {{$user->last_name}}</span>
				<div class="control">
					<button class="btn-clear open-detail" title="Benutzerinfos einblenden" data-id="{{$user->id}}"><i class="icon-arrow-right"></i></button>
					<button class="btn-clear close-detail" title="Benutzerinfos ausblenden" data-id="{{$user->id}}"><i class="icon-arrow-left"></i></button>
				</div>
			</div>
		</li>
		@endforeach
	</ul>
@overwrite	

@section('detail-header')
	<h2 class="app-col-title">ausgewählter Benutzer</h2>
@overwrite	 

@section('detail-body')
	<div class="userfolders app-col-body">

	</div>
	<div class="files-container userfiles app-col-body">
		Text
	</div>
@overwrite	 
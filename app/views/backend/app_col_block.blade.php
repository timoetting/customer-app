<div class="app-col-block @yield('classname') @yield('opened')">
	<div class="app-col app-col-list">
		<div class="app-col-header">
			@yield('list-header')
		</div>
		<div class="app-col-body">
			@yield('list-body')
		</div>
	</div>
	<div class="app-col app-col-detail">
		<div class="app-col-header">
			@yield('detail-header')
		</div>
		<div class="app-col-body">
			@yield('detail-body')
		</div>	
	</div>
</div>
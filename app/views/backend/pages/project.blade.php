@extends('backend.scaffolding')

@section('title')
	{{$project->title}} | FinalEdit
@stop	

@section('breadcrumbs')
	<a href="{{ URL::route('backend') }}">
	    Dashboard
	</a>
	<i class="icon-arrow-right"></i>
	<a href="{{ URL::route('project', array('slug' => $project->slug)) }}">
		<i class="icon-box"></i> {{$project->title}}
	</a>
@stop

@section('content')
<div class="app-col app-col-minus-1">
	<div class="app-col-header">
		<h2 class="app-col-title">
			{{$project->title}}
		</h2>
		<div class="control">
			<div class="btn-group">
			  <a href="?sorting={{$sorting}}&view_size=s" type="button" class="btn btn-primary @if($view_size == 's') active @endif"><i class="icon-list"></i></a>
			  <a href="?sorting={{$sorting}}&view_size=m" type="button" class="btn btn-primary @if($view_size != 's') active @endif"><i class="icon-th-list"></i></a>
			</div>
		</div>
	</div>
	<div class="app-col-body">
		<div class="file-tree-wrapper">
			<div class="col-indicator"></div>
			<div class="file-tree-container">
				<div class="file-tree">
					
					@include('backend.file_tree', array('folder' => $projectFolder, 'depth' => 0, 'parent' => $projectFolder))
				</div>
			</div>
		</div>
		<div class="upload-info-box hidden">
			
		</div>
	</div>
</div>
<div class="app-col app-col-list">
	<div class="app-col-header">
		<h2 class="app-col-title">
			Users
		</h2>
	</div>
	<div class="app-col-body">
		<ul class="entry-list user-list">
			@foreach($users as $user)
			<li>
				<a href="{{ URL::route('user-in-project', array('slug' => $project->slug, 'username' => $user->username)) }}{{$getParams}}">
					<div class="user entry" data-id="{{$user->id}}">
						<div class="icon"><i class="icon-user"></i></div> {{$user->username}} <span class="secondary name">{{$user->first_name}} {{$user->last_name}}</span>
					</div>
				</a>
			</li>
			@endforeach
		</ul>
	</div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				Really?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary submit" data-dismiss="modal"b>confirm</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">close</button>
			</div>
		</div>
	</div>
</div>

@stop


@section('dropzone-template')
<div id="dropzone-template">
	<span class="name" data-dz-name></span>
	<strong class="error text-danger" data-dz-errormessage></strong>
	<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
		<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
	</div>
</div>
@stop




@section('javascript')
<script>
	folderId = 0;
	myDropzone = '';
	checkedFiles = new Array();
	selectedFiles = new Array();
	collapsedFoldersIds = {{json_encode($collapsedFolders)}};
	$collapsedFolders = new Array();

	$.fn.selectControl = function() {
		console.log('in selectable');
	    var lastSelected,
	        $files = this;

	    $files.click(function(evt) {
	    	evt.preventDefault();
	    	console.log('click on file');
	        if(!lastSelected) {
	            lastSelected = this;
	            $(this).parent('.file.entry').addClass('checked');
	            return;
	        }
	 
	        if(evt.shiftKey) {
	        	console.log('shift');
	            var start = $files.index(this),
	                end = $files.index(lastSelected);
	            if($(lastSelected).parent('.file.entry').hasClass('checked')){
		            $files.slice(Math.min(start, end), Math.max(start, end) + 1)
		            	.parent('.file.entry')
		                .addClass('checked');
	            }else{
	            	$files.slice(Math.min(start, end), Math.max(start, end) + 1)
	            		.parent('.file.entry')
	            	    .removeClass('checked');
	            }
	        }else if(evt.altKey) {
	        	console.log('alt');
                $(this).parent('.file.entry').toggleClass('checked');
            }else{
	        	console.log('no key');
            	$files.parent('.file.entry').removeClass('checked');
            	$(this).parent('.file.entry').addClass('checked');
            }
	        lastSelected = this;
	        selectedFiles = new Array();
	        $('.file.entry.checked').each(function(element){
				selectedFiles[selectedFiles.length] = $(this).attr('data-id');
			});
	    });

		$('.folder.entry .name').click(function(evt){
	    	evt.preventDefault();
	    	evt.stopPropagation();
			console.log('folder click');
			var id = $(this).parent('.folder.entry').data('id');
			console.log(id);
			$subFiles = $('.file.entry[data-parent-id='+id+']');
			if($('.file.entry.checked[data-parent-id='+id+']').length == $subFiles.length){
				$subFiles.removeClass('checked');
			}else{
				$subFiles.addClass('checked');
			}
	        selectedFiles = new Array();
	        $('.file.entry.checked').each(function(element){
				selectedFiles[selectedFiles.length] = $(this).attr('data-id');
			});
		})
	};

	$(window).load(function(){
		initDropzone();
		initMouseInteraction();
		initFileSelectable();
		initLayoutControl();
		initDragDrop();
		collapsedFoldersIds.forEach(function(id){
			collapseFolder(id);
		});
		redrawFileTreeHeader();
		//$collapsedFolders = $('.folder.entry.collapsed');
	})

	function toggleFolderUserRelation(folderId, userId){
		$.ajax({
			url: '{{ URL::to("/") }}/folders/toggle-user-relation/' + folderId+ '/' + userId
		}).success(function(data) {
			console.log('fertig');
			redrawFileTree();
		});
	}
	
	function connectFolderToUser(folderId, userId){

	}
	//Diese muss zunaechst angepasst werden
	function toggleFileUserRelation(clickTarget, fileId, userId){
		if(selectedFiles.length > 0)
		{
			var fileIds = new Array();
			// $('.file.entry.checked').each(function(entry){
			// 	fileIds[fileIds.length] = entry.id;
			// })
			selectedFiles.forEach(function(id){
				fileIds[fileIds.length] = id;
			});
		}else{
			var fileIds = [fileId];
		}

		if($(clickTarget).hasClass('active')){
			detachFilesFromUser(fileIds, userId);
		}else{
			attachFilesToUser(fileIds, userId);
		}
			
	}

	function getSelectedFileIds(){
		ids = Array();
        $('.file.entry.checked').each(function(element){
			ids[ids.length] = $(this).attr('data-id');
		});
		if(ids.length > 0)
			return ids;
		else
			return null;
	}

	function attachFileToUser(fileId, userId){

		$.ajax({
			type: 'POST',
			url: '{{ URL::to("/") }}/files/attach-to-user/' + userId
		}).success(function(data) {
			console.log('fertig');
			redrawFileTree();
		});
	}

	function attachFilesToUser(fileIds, userId){
		
		var data = {fileIds:fileIds, userId:userId}
		$.ajax({
			type: 'POST',
			url: '{{ URL::to("/") }}/files/attach-to-user/' + userId ,
			data: data
		}).success(function() {
			console.log('attached');
			redrawFileTree();
		});
	}

	function detachFilesFromUser(fileIds, userId){
		var data = {fileIds:fileIds, userId:userId}
		$.ajax({
			type: 'POST',
			url: '{{ URL::to("/") }}/files/detach-from-user/' + userId ,
			data: data
		}).success(function() {
			console.log('detached');
			redrawFileTree();
		});
	}

	function redrawFileTree(){

		$.ajax({
			url: '{{ URL::to("file-tree", array($project->id, $view_size, $sorting)) }}'
		}).success(function(data) {
			$('.file-tree').html(data);
			initDragDrop();
			initMouseInteraction();
			initFileSelectable();
			initLayoutControl();
			$collapsedFolders.each(function(){
				// $(this).addClass('collapsed');
				$('.folder.entry[data-id='+$(this).attr('data-id')+']').addClass('collapsed');
				collapseFolders($(this).attr('data-id'));
			})
			redrawFileTreeHeader();

			
		});
	}

	function createFolder(parentId){
		event.stopPropagation();
		$.ajax({
			url: '{{ URL::to('/') }}/folders/create'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal .modal-content').find('.parent-id').val( parentId );
			$('#modal .modal-dialog').removeClass('modal-lg');
			$('#modal .modal-dialog').addClass('modal-sm');
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					$('#modal').modal('toggle');
					redrawFileTree();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function editFolder(folderId){
		event.stopPropagation();
		$.ajax({
			url: '{{ URL::to('/') }}/folders/'+folderId+'/edit'
		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal .modal-dialog').removeClass('modal-lg');
			$('#modal .modal-dialog').addClass('modal-sm');
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				// type: 'PUT',
				success: function (response) {
					$('#modal').modal('toggle');
					redrawFileTree();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function deleteFolder(folderId){
		event.stopPropagation();
		$('#modal-confirm .modal-body').text('Do you really want to delete this folder?');
		$('#modal-confirm').modal('show');
		$('#modal-confirm .submit').unbind('click');
		$('#modal-confirm .submit').click(function(){
			console.log('modal submit');
			$.ajax({
				url: '{{ URL::to('/') }}/folders/delete/'+folderId
			}).success(function(data) {
				redrawFileTree();
			});
		})
	}

	function uploadFile(id){
		event.stopPropagation();
		folderId = id;
		
		myDropzone.options.previewsContainer = ".upload-info-box";//".file-tree .folder.list[data-id='"+id+"'] >li .file.list";
		myDropzone.options.url = "{{ URL::to('/') }}/upload-handler/" + id;
		$('.dz-hidden-input').trigger('click');
	}

	function editFile(id){
		event.stopPropagation();
		
		$.ajax({
			url: '{{ URL::to('/') }}/files/' + id + '/edit'

		}).done(function(data) {
			$('#modal .modal-dialog').removeClass('modal-lg');
			$('#modal .modal-dialog').addClass('modal-sm');
			$('#modal .modal-content').html(data);
			$('#modal').modal('show');

			$('#modal #update-file').ajaxForm({
				dataType: "html",
				success: function (response) {
					$('#modal').modal('hide');
					redrawFileTree();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend(errorThrown);
				}
			});
		});
	}

	function editVideo(id){
		event.stopPropagation();
		
		$.ajax({
			url: '{{ URL::to('/') }}/files/' + id + '/edit'

		}).done(function(data) {
			$('#modal .modal-dialog').removeClass('modal-sm');
			$('#modal .modal-dialog').addClass('modal-lg');
			$('#modal .modal-content').html(data);
			$('#modal').modal('show');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					$('#modal').modal('hide');
					redrawFileTree();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend(errorThrown);
				}
			});
		});
	}

	function selectThumb(id, isVideo){
		$('.btn-thumb-upload').trigger('click');
		$('#modal #upload-thumb .btn-thumb-upload').on('change', function(){

			console.log('onchange');

			$('#modal #upload-thumb').submit(function(){

				console.log('submit');

			    var formData = new FormData($(this)[0]);

			    $.ajax({
			        url: $('#modal #upload-thumb').attr("action"),
			        type: 'POST',
			        data: formData,
			        async: false,
			        success: function (data) {
			        	if(isVideo = true){
			            	editVideo(id);
			        	}else{
			        		editFile(id);
			        	}
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });

			    return false;
			});
			$('#modal #upload-thumb').trigger('submit');

		});

	}

	function captureThumb(id){
		var vid = document.getElementById("previewVideo");
		console.log(vid.currentTime);
		var currentTime = vid.currentTime;

		$.ajax({
			url: '{{ URL::to('/') }}/thumb-from-video/'+id+'/'+currentTime

		}).done(function(data) {
			console.log('gecaptured');
			editVideo(id);
			redrawFileTree();
		});
	}

	function deleteFile(id){
		event.stopPropagation();
		$('#modal-confirm').modal('show');
		$('#modal-confirm .submit').unbind('click');
		if(selectedFiles.length > 1){
			$('#modal-confirm .modal-body').text('Do you really want to delete ' + selectedFiles.length + ' file?');
			var data = {selectedFilesIds: selectedFiles};
			console.log('mehr als einer selected');
			$('#modal-confirm .submit').click(function(){
				$.ajax({
					type: 'POST',
					data: data,
					url: '{{ URL::to('/') }}/files/multiDelete'
				}).done(function(data) {
					redrawFileTree();
					console.log('done');
				});
			});
		}else{
			$('#modal-confirm .modal-body').text('Do you really want to delete this file?');
			$('#modal-confirm .submit').click(function(){
				$.ajax({
					type: 'POST',
					url: '{{ URL::to('/') }}/files/delete/'+id
				}).success(function(data) {
					redrawFileTree();
				});
			});
		}
	}

	function moveFilesToFolder(fileIds, folderId){
		var data = {fileIds:fileIds, folderId:folderId};
		$.ajax({
			type: 'POST',
			data: data,
			url: '{{ URL::to('/') }}/files/move-to-folder'
		}).done(function(data) {
			redrawFileTree();
		});
	}

	function initDragDrop(){
		$( ".file.entry .name, .folder.entry .name" ).draggable({ 
			// helper: "clone",
			helper: function(event) {
				console.log(getSelectedFileIds());
				var selectedFileIds = getSelectedFileIds();
				if(selectedFileIds != null && $.inArray($(this).data('id'), selectedFileIds) && selectedFileIds.length > 1)
					return $('<div class="file drag-container" data-ids>Drag Container</div>').text(getSelectedFileIds().length + ' files');
				else
					return $('<div class="file drag-container" data-ids data-id='+$(this).data('id')+'>Drag Container</div>').text($(this).find('.file-name, .folder-name').text());
			},
			appendTo: ".app-container",
			//containment: ".file-tree",
			cursorAt:{
				top: 20,
				left: 20
			},
			scroll: true
		})

		$( ".folder.entry" ).droppable({ 
			accept: ".file.entry .name, .folder.entry .name",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
				// if($(ui.helper).data('id') != $(event.target).data('id')){
					if($(ui.helper).hasClass('file')){
						console.log('drop');
						if(getSelectedFileIds() != null && $.inArray($(ui.helper).data('id'), getSelectedFileIds() ) ){
							console.log('hier');
							moveFilesToFolder(getSelectedFileIds(), $(event.target).data('id'));
						}else{
							console.log($(ui.helper));
							var fileId = [$(ui.helper).data('id')]
							moveFilesToFolder(fileId, $(event.target).data('id'));
						}
						$('.file.entry.checked').removeClass('checked');
						selectedFiles = new Array();
						// $.ajax({
						// 	url: '{{ URL::to('/') }}/files/move-to-folder/' + $(ui.helper).data('id') + '/' + $(event.target).data('id')
						// }).done(function(data) {
						// 	redrawFileTree();
						// });
					}else if($(ui.helper).hasClass('folder') && $(ui.helper).data('id') != $(event.target).data('id')){
						console.log('dropinfolder');
						$.ajax({
							url: '{{ URL::to('/') }}/folders/move-to-folder/' + $(ui.helper).data('id') + '/' + $(event.target).data('id')
						}).done(function(data) {
							checkedFiles = new Array();
							redrawFileTree();
						});
					}
			}
		});
	}

	function initDropzone(){
		myDropzone = new Dropzone('body', { // Make the whole body a dropzone
		  url: "{{ URL::to('/') }}/upload-handler/", // Set the url
		  maxFilesize: 10000,
		  parallelUploads: 20,
		  //previewTemplate: '<div class="file entry"><span class="progress-bar-title" data-dz-name></span><strong class="error text-danger" data-dz-errormessage></strong><div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div></div></div>',
		  previewTemplate: '<div><span class="progress-bar-title" data-dz-name></span><strong class="error text-danger" data-dz-errormessage></strong><div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div></div></div>',
		  autoQueue: true,
		  previewsContainer: ".upload-info-box"//".file-tree" ,
		});

		myDropzone.on("sending", function(file) {
			$('.upload-info-box').removeClass('hidden');
		});

		myDropzone.on("queuecomplete", function(progress) {
			$('.upload-info-box').html('');
			$('.upload-info-box').addClass('hidden');
			redrawFileTree();
		});

		myDropzone.on("error", function(file,response) {
		   console.log('dropzone error: ');
		   console.log(response);
		});
	}

	function initMouseInteraction(){
		$( ".file.entry" ).hover(function(e){
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').addClass('look-up');
		}, function(e){
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').removeClass('look-up');
		})

		$( ".folder.entry" ).hover(function(e){
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').addClass('look-up');
		}, function(e){
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').removeClass('look-up');
		})

		$( ".user-toggle" ).hover(function(e){
			$('.col-indicator').removeClass('hidden');
			$('.col-indicator').css('left', $(this).position().left)
		}, function(e){
			$('.col-indicator').addClass('hidden');
		})
	}

	function initFileSelectable(){

		$('.file.entry .name').selectControl();

		selectedFiles.forEach(function(id){
			$('.file.entry[data-id='+id+']').addClass('checked');
		})
	}

	function initLayoutControl(){
		console.log('initLayoutControl');

		$('.file-tree-container').scroll(function(){
			$('.file-tree .col.name').css('left', $('.file-tree-container').scrollLeft());
			if($('.file-tree-container').scrollLeft() > 0)
				$('.file-tree .col.name').addClass('covering');
			else
				$('.file-tree .col.name').removeClass('covering');

			$('.file-tree .file-tree-header').css('left', - $('.file-tree-container').scrollLeft());
		})
	}

	function toggleFolder(id){
		event.stopPropagation();
		// if($(this).hasClass('collapsed'))
		var $folder = $('.folder.entry[data-id='+id+']');
		// $folder.toggleClass('collapsed');
		if(!$folder.hasClass('collapsed')){
			$.ajax({
				url: '{{ URL::to('/') }}/folders/collapse/' + id + '/{{Auth::user()->id}}',
				type: 'POST'
			}).success(function(data) {
				collapseFolder(id);
				// $('.folder.entry[data-id='+id+']').addClass('collapsed')
			});
			
		}else{
			$.ajax({
				url: '{{ URL::to('/') }}/folders/decollapse/' + id + '/{{Auth::user()->id}}',
				type: 'POST'
			}).success(function(data) {
				decollapseFolder(id);
				// $('.folder.entry[data-id='+id+']').removeClass('collapsed')
			});
		}
	}

	function collapseFolder(id){
		$('.folder.entry[data-id='+id+']').addClass('collapsed');
		collapseFolders(id);
		$collapsedFolders = $('.folder.entry.collapsed');
		redrawFileTreeHeader();
	}

	function decollapseFolder(id){
		$('.folder.entry[data-id='+id+']').removeClass('collapsed');
		decollapseFolders(id);
		$collapsedFolders = $('.folder.entry.collapsed');
		redrawFileTreeHeader();
	}

	function collapseFolders(id){
		$('.file.entry[data-parent-id='+id+']').addClass('hidden');
		var $subfolders = $('.folder.entry[data-parent-id='+id+']');
		$subfolders.each(function(){
			$(this).addClass('hidden');
			collapseFolders($(this).attr('data-id'));
		})
	}

	function decollapseFolders(id){
		$('.file.entry[data-parent-id='+id+']').removeClass('hidden');
		var $subfolders = $('.folder.entry[data-parent-id='+id+']');
		$subfolders.each(function(){
			$(this).removeClass('hidden');
			if(!$(this).hasClass('collapsed')){
				decollapseFolders($(this).attr('data-id'));
			}
		})
	}

	function redrawFileTreeHeader(){
		console.log('fixwidth');
		$('.file-tree-header .col.name').css('min-width', $('.file.entry:not(.hidden) .col.name').outerWidth());
	}
	
	$('#modal').on('shown.bs.modal', function (e) {
	  $(e.target).find('input[type=text]').first().focus();
	})
	
</script>
@overwrite
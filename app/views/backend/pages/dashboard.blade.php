@extends('backend.scaffolding')

@section('title')
	Dashboard | FinalEdit
@stop	

@section('breadcrumbs')
	<a href="{{ URL::route('dashboard') }}">
	    Dashboard
	</a>
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="app-col app-col-full dashboard-list">
				<div class="app-col-header">
					<h2 class="app-col-title">
						Projects
					</h2>
					<div class="tool-bar">	
						<button class="tip-bottom tool-bar-btn create create-project" onclick="createProject()" title="Projekt hinzufügen"><i class="icon-plus"></i></button>
					</div>
				</div>
				<div class="app-col-body">
					<div class="project-list">
						@each('backend.dashboard.project', $projects, 'project')
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="app-col app-col-full dashboard-list">
				<div class="app-col-header">
					<h2 class="app-col-title">
						Users
					</h2>
					<div class="tool-bar">	
						<button class="tip-bottom tool-bar-btn create create-user" onclick="createUser()" title="Benutzer hinzufügen"><i class="icon-plus"></i></button>
					</div>
				</div>
				<div class="app-col-body">
					<div class="project-list">
						@each('backend.dashboard.user', $users, 'user')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			lala
		</div>
	</div>
</div>

<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				Really?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary submit" data-dismiss="modal"b>confirm</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">close</button>
			</div>
		</div>
	</div>
</div>

@stop






@section('javascript')
<script>
	$(window).load(function(){
		initDragDrop();
	})

	function redrawProject(id){
		var project = $(".project .entry[data-id="+id+"]");
		$.ajax({
			url: '{{ URL::to("/") }}/dashboard/project/'+id
		}).success(function(data) {
			project.html(data);
			initDragDrop();
		});
	}

	function redrawUser(id){
		var user = $(".user .entry[data-id="+id+"]");
		$.ajax({
			url: '{{ URL::to("/") }}/dashboard/user/'+id
		}).success(function(data) {
			user.html(data);
			initDragDrop();
		});
	}

	function createProject(){
		$.ajax({
			url: '{{ URL::to('/') }}/projects/create'

		}).done(function(data) {

			$('#modal .modal-dialog').removeClass('modal-sm');
			$('#modal .modal-dialog').addClass('modal-lg');
			$('#modal .modal-content').html(data);
			$('#modal').modal('show');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					location.reload();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function editProject(projectId){

		$.ajax({
			url: '{{ URL::to('/') }}/projects/'+projectId+'/edit'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal .modal-dialog').removeClass('modal-sm');
			$('#modal .modal-dialog').addClass('modal-lg');
			$('#modal').modal('show');

			$('#modal form').ajaxForm({
				dataType: "html",
				// type: 'PUT',
				success: function (response) {
					$('#modal').modal('toggle');
					location.reload();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function deleteProject(projectId){
		$('#modal-confirm .modal-body').text('Do you really want to delete this project?');
		$('#modal-confirm').modal('show');
		$('#modal-confirm .submit').unbind('click');
		$('#modal-confirm .submit').click(function(){
			$.ajax({
				url: '{{ URL::to('/') }}/projects/delete/'+projectId
			}).success(function(data) {
				location.reload();
			});
		});
	}

	function createUser(){
		$.ajax({
			url: '{{ URL::to('/') }}/users/create'

		}).done(function(data) {
			$('#modal .modal-dialog').removeClass('modal-sm');
			$('#modal .modal-dialog').addClass('modal-lg');
			$('#modal .modal-content').html(data);
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					// $('.app-col.users .user-list').append('<li>' + response + '</li>');
					// $('#modal').modal('toggle');
					// initUserInteraction();
					// reInitDragDrop(false, false, true, false);
					location.reload();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function editUser(userId){

		$.ajax({
			url: '{{ URL::to('/') }}/users/'+userId+'/edit'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal .modal-dialog').removeClass('modal-sm');
			$('#modal .modal-dialog').addClass('modal-lg');
			$('#modal').modal('show');

			$('#modal form').ajaxForm({
				dataType: "html",
				// type: 'PUT',
				success: function (response) {
					$('#modal').modal('toggle');
					location.reload();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function deleteUser(userId){
		$.ajax({
			url: '{{ URL::to('/') }}/users/delete/'+userId
		}).success(function(data) {
			location.reload();
		});
	}

	function detachUserFromProject(userId, projectId, username, projectTitle){
		$('#modal-confirm .modal-body').html('Do you really want to detach the user <b>' + username + '</b> from the project <b>' + projectTitle + '</b>?');
		$('#modal-confirm').modal('show');
		$('#modal-confirm .submit').unbind('click');
		$('#modal-confirm .submit').click(function(){
			var data = {userId:userId, projectId:projectId};
			$.ajax({
				url: '{{ URL::to('/') }}/users/detachproject',
				type: 'POST',
				data: data
			}).success(function(data) {
				location.reload();
			});
		});
	}

	function initDragDrop(){
		$( ".entry" ).draggable({ 
			helper: "clone",
			appendTo: ".app-container",
			containment: "document",
			cursorAt:{
				top: 20,
				left: 20
			}
		})

		$( ".project .entry" ).droppable({ 
			accept: ".user .entry",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
				console.log('drop');
				$.ajax({
					url: '{{ URL::to('/') }}/users/connect/' + $(ui.helper).data('id') + '/toproject/' + $(event.target).data('id')
				}).done(function(data) {
					console.log('done');
					redrawProject($(event.target).data('id'));
					redrawUser($(ui.helper).data('id'));
				});
			}
		});

		$( ".user .entry" ).droppable({ 
			accept: ".project .entry",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
				console.log('drop');
				$.ajax({
					url: '{{ URL::to('/') }}/users/connect/' + $(event.target).data('id') + '/toproject/' + $(ui.helper).data('id')
				}).done(function(data) {
					console.log('done');
					redrawProject($(ui.helper).data('id'));
					redrawUser($(event.target).data('id'));
				});
			}
		});

		
		$('#modal').on('shown.bs.modal', function (e) {
		  console.log(modal)
		  $(e.target).find('input[type=text]').first().focus();
		  // $('#myInput').focus()
		})

	}
</script>
@stop
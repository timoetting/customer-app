@extends('backend.scaffolding')

@section('title')
	{{$user->username}} in {{$project->title}} | FinalEdit
@stop	

@section('breadcrumbs')
	<a href="{{ URL::route('backend') }}">
	    Dashboard
	</a>
	<i class="icon-arrow-right"></i>
	<a href="{{ URL::route('project', array('slug' => $project->slug)) }}">
		<i class="icon-box"></i> {{$project->title}}
	</a>
	<i class="icon-arrow-right"></i>
	<a href="{{ URL::route('user-in-project', array('projectSlug' => $project->slug, 'username' => $user->username)) }}">
		<i class="icon-user"></i> {{$user->username}}
	</a>
@overwrite

@section('content')
<div class="app-col app-col-list">
	<div class="app-col-header">
		<h2 class="app-col-title">
			Users
		</h2>
	</div>
	<div class="app-col-body">	
		<ul class="entry-list user-list">
			@foreach($users as $listedUser)
			<li>
				<a href="{{ URL::route('user-in-project', array('slug' => $project->slug, 'username' => $listedUser->username)) }}{{$getParams}}" class="">
					<div class="user entry @if($listedUser->id == $user->id) active @endif" data-id="{{$user->id}}">
						<div class="icon"><i class="icon-user"></i></div> {{$listedUser->username}} <span class="secondary name">{{$listedUser->first_name}} {{$listedUser->last_name}}</span>
					</div>
				</a>
			</li>
			@endforeach
		</ul>
	</div>
</div>
<div class="app-col app-col-minus-1 app-col-detail userfiles">
	<div class="app-col-header">
		<h2 class="app-col-title">
			{{$user->username}}
		</h2>
		<div class="control">
			<div class="control-item">
				<a class="link" target="_blank" href="{{URL::route('downloadarea_project', array($user->username, $project->slug))}}">go to user's project site </a>
			</div>
			<div class="control-item">
				<div class="btn-group">
				  <a href="?sorting={{$sorting}}&view_size=s&custom_sort_mode={{$custom_sort_mode}}" type="button" class="btn btn-default @if($view_size == 's') active @endif"><i class="icon-list"></i></a>
				  <a href="?sorting={{$sorting}}&view_size=m&custom_sort_mode={{$custom_sort_mode}}" type="button" class="btn btn-default @if($view_size != 's') active @endif"><i class="icon-th-list"></i></a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="app-col-body user-nav">
		<div class="app-col app-col-userfolders">
			<div class="app-col-body">
				<div class="section-header">
					<h6 class="header-title">FTP Directory</h6>
				</div>
				<ul class="userfolders entry-list">
					<li>
						<a href="{{ URL::route('user-in-project', array('slug' => $project->slug, 'username' => $user->username))}}{{$getParams}}" >
							<div class="userfolder entry @if($userfolder == null) active @endif">
								show all user files
							</div>
						</a> 
					</li>
				</ul>
				<div class="section-header">
					<h6 class="header-title">Frontend Directory</h6>
				</div>
				<ul class="userfolders entry-list">
					<!-- <li>
						<a href="{{ URL::route('user-in-project', array('slug' => $project->slug, 'username' => $user->username, 'userfolderEntry' => 0)) }}" >
							<div class="userfolder entry @if($mainFolder) active @endif" data-id="0">
							Main
							</div>
						</a> 
					</li> -->
					@foreach ($user->userfoldersByProject($project) as $userfolderEntry) 
					<li>
							<div class="userfolder entry @if($userfolder != null  && ($userfolderEntry->id == $userfolder->id)) active @endif" data-id="{{$userfolderEntry->id}}">
						<a href="{{ URL::route('user-in-project', array('slug' => $project->slug, 'username' => $user->username, 'userfolderEntry' => $userfolderEntry->id)) }}{{$getParams}}" >
							@if($userfolderEntry->is_main)
								<i class="icon-home"></i>
							@endif
							{{$userfolderEntry->title}}
							@if($userfolderEntry->is_default)
								<span class="secondary"> (default folder)</span>
							@endif
							<div class="secondary frontend-settings">
							
							<span class="setting-title">Sorting: </span>
							@if($userfolderEntry->sorting() == 'default')
								custom sorting
							@else
								{{$userfolderEntry->sorting()}} 
							@endif
							<span class="setting-title">View Size: </span>
							@if($userfolderEntry->view_style == 's')
								<i class="icon-list"></i>
							@elseif($userfolderEntry->view_style == 'l')
								<i class="icon-th-large"></i>
							@else
								<i class="icon-th-list"></i>
							@endif	
							</div>
							

						</a> 
						<div class="control">
							<button class="btn-clear" title="edit userfolder" onclick="editUserfolder({{$userfolderEntry->id}})" data-id={{$userfolderEntry->id}} ><i class="icon-pencil"></i></button>
							@if(!$userfolderEntry->is_main)
								<button class="btn-clear" title="delete userfolder" onclick="deleteUserfolder({{$userfolderEntry->id}})" data-id={{$userfolderEntry->id}}><i class="icon-trash-o"></i></button>
							@endif
						</div>
							</div>
					</li>
					@endforeach
					<li>
						<button class=" btn btn-default btn-block" onclick="createUserfolder()"><i class="icon-plus"></i> create user folder</button>
					</li>	
				</ul>
				
			</div>
		</div>
		<div class="file-tree-wrapper">
			<div class="file-tree-container">
					@if($userfolder == null)

						<div class="file-tree all-userfiles">
							@include('backend.file_tree_user', array('folder' => $projectFolder, 'depth' => 0, 'parent' => $projectFolder))
						</div>
					@else
						<div class="file-tree userfolder files">
							@include('backend.file_tree_userfolder', array('project' => $project, 'user' => $user, 'userfolder' => $userfolder))

						</div>
					@endif
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		</div>
	</div>
</div>
@overwrite


@section('dropzone-template')
<div id="dropzone-template">
	<span class="name" data-dz-name></span>
	<strong class="error text-danger" data-dz-errormessage></strong>
	<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
		<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
	</div>
</div>
@overwrite




@section('javascript')
<script>
	folderId = 0;
	myDropzone = '';
	selectedFiles = new Array();
	collapsedFoldersIds = {{json_encode($collapsedFolders)}};
	$collapsedFolders = new Array();

	$(window).load(function(){
		initDragDrop();
		// initDropzone();
		initMouseInteraction();
		initFileSelectable();
		collapsedFoldersIds.forEach(function(id){
			collapseFolder(id);
		});
		redrawFileTreeHeader();
	})

		$.fn.selectControl = function() {
			console.log('in selectable');
		    var lastSelected,
		        $files = this;

		    $files.click(function(evt) {
		    	evt.preventDefault();
		    	evt.stopPropagation();
		    	console.log('click on file');
		        if(!lastSelected) {

		        	console.log('no last selected');
		            lastSelected = this;
		            $(this).parent('.file.entry').addClass('checked');
		            return;
		        }
		 
		        if(evt.shiftKey) {
		        	console.log('shift');
		            var start = $files.index(this),
		                end = $files.index(lastSelected);
		            if($(lastSelected).parent('.file.entry').hasClass('checked')){
			            $files.slice(Math.min(start, end), Math.max(start, end) + 1)
			            	.parent('.file.entry')
			                .addClass('checked');
		            }else{
		            	$files.slice(Math.min(start, end), Math.max(start, end) + 1)
		            		.parent('.file.entry')
		            	    .removeClass('checked');
		            }
		        }else if(evt.altKey) {
		        	console.log('alt');
	                $(this).parent('.file.entry').toggleClass('checked');
	            }else{
		        	console.log('no key');
	            	$files.parent('.file.entry').removeClass('checked');
	            	$(this).parent('.file.entry').addClass('checked');
	            }
		        lastSelected = this;
		        selectedFiles = new Array();
		        $('.file.entry.checked').each(function(element){
					selectedFiles[selectedFiles.length] = $(this).attr('data-id');
				});
		    });

			$('.folder.entry .name').click(function(evt){
		    	evt.preventDefault();
		    	evt.stopPropagation();
				console.log('folder click');
				var id = $(this).parent('.folder.entry').data('id');
				console.log(id);
				$subFiles = $('.file.entry[data-parent-id='+id+']');
				if($('.file.entry.checked[data-parent-id='+id+']').length == $subFiles.length){
					$subFiles.removeClass('checked');
				}else{
					$subFiles.addClass('checked');
				}
		        selectedFiles = new Array();
		        $('.file.entry.checked').each(function(element){
					selectedFiles[selectedFiles.length] = $(this).attr('data-id');
				});
			})
		};

	function toggleFolderUserRelation(folderId, userId){
		$.ajax({
			url: '{{ URL::to("/") }}/folders/toggle-user-relation/' + folderId+ '/' + userId
		}).success(function(data) {
			redrawFileTree();
		});
	}

	function toggleFileUserRelation(fileId, userId){
		$.ajax({
			url: '{{ URL::to("/") }}/files/toggle-user-relation/' + fileId+ '/' + userId
		}).success(function(data) {
			redrawFileTree();
		});
	}

	function redrawFileTree(){
		console.log('redrawFileTree')
		@if($userfolder == null)

			$.ajax({
				url: '{{ URL::to("file-tree-user", array("projectId" => $project->id, "userId" => $user->id, "view_size" => $view_size, "sorting" => $sorting) ) }}'
			}).success(function(data) {
				$('.file-tree').html(data);
				initDragDrop();
				initMouseInteraction();
				initFileSelectable();
				$collapsedFolders.each(function(){
					// $(this).addClass('collapsed');
					$('.folder.entry[data-id='+$(this).attr('data-id')+']').addClass('collapsed');
					collapseFolders($(this).attr('data-id'));
				});
				redrawFileTreeHeader();
			});

		@else

			$.ajax({
				url: '{{ URL::to("file-tree-userfolder", array("projectId" => $project->id, "userId" => $user->id, "userfolderId" => $userfolder->id) ) }}'
			}).success(function(data) {
				$('.file-tree').html(data);
				initDragDrop();
				initMouseInteraction();
				initFileSelectable();
			});

		@endif
	}

	function createUserfolder(){

		$.ajax({
			url: '{{ URL::to('/') }}/userfolders/create'
		}).done(function(data) {
			$('#modal .modal-content').html(data);
			//$('#modal .modal-content').find('.current-userfolder-path').val(currentState['userfolder']['path']);
			$('#modal .modal-content').find('.current-user-id').val({{$user->id}});
			$('#modal .modal-content').find('.current-project-id').val({{$project->id}});

			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				//dataType: "html",
				success: function (response) {
					$('#modal').modal('toggle');
					location.reload();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function editUserfolder(id){

		$.ajax({
			url: '{{ URL::to('/') }}/userfolders/'+id+'/edit'
		}).done(function(data) {
			$('#modal .modal-content').html(data);

			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				//dataType: "html",
				success: function (response) {
					//$('#modal').modal('toggle');
					location.reload();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	}

	function deleteUserfolder(id){
		$.ajax({
			url: '{{ URL::to('/') }}/userfolders/delete/'+id
		}).success(function(data) {
			location.reload();
		});
	}

	function editFile(id){
		//e.stopPropagation();
		
		$.ajax({
			url: '{{ URL::to('/') }}/files/' + id + '/edit'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					$('#modal').modal('toggle');
					redrawFileTree();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend(errorThrown);
				}
			});
		});
	}

	function deleteFile(id){
		$.ajax({
			url: '{{ URL::to('/') }}/files/delete/'+id
		}).success(function(data) {
			redrawFileTree();
		});
	}

	function initDragDrop(){
		$( ".all-userfiles .file.entry" ).draggable({ 
			helper: function(event) {
				return $('<div class="file drag-container" data-ids=>Drag Container</div>').text($(this).find('.file-name').text());
			},
			appendTo: ".app-container",
			containment: "document",
			cursorAt:{
				top: 20,
				left: 20
			},
			start: function(event, ui){
				// $(ui.helper).html($(event.target).find('file_name').html());
				var fileIds = new Array();
				if($(event.target).hasClass('checked') &&  $('.file.entry.checked').length > 1){
					$(ui.helper).text($('.file.entry.checked').length + ' files');
					$('.file.entry.checked').each(function(id){
						fileIds[fileIds.length] = $(this).data('id');
						// fileIds.push($(this).attr('data-id'));
					})
					$(ui.helper).data('ids', fileIds);
				}else{
					fileIds[0] = $(this).data('id');
					$(ui.helper).data('ids', fileIds);
				}
			}
		})

		$( ".all-userfiles .folder.entry .name" ).draggable({ 
			helper: 'clone',
			appendTo: ".app-container",
			containment: "document",
			cursorAt:{
				top: 20,
				left: 20
			}
		})

		$( ".userfolder.entry" ).droppable({ 
			accept: ".file.entry, .folder.entry .name",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
				selectedFiles = new Array();
				if($(ui.helper).hasClass('file')){
						var fileIds = $(ui.helper).data('ids').toString().split(',');
						var userfolderId = $(event.target).attr('data-id');
						console.log(fileIds);

						$.ajax({
							url: "{{ URL::route('moveFilesToUserfolder') }}",
							// data: {fileIds: fileIds, userfolderId: userfolderId},
							data: {fileIds: fileIds, userfolderId: userfolderId},
							type: 'POST'
						}).done(function(data) {
							redrawFileTree();
						});
				}else if($(ui.helper).hasClass('folder')){
					console.log('drop folder on userfolder', $(ui.helper).data('id'), $(event.target).data('id'));
					$.ajax({
						url: "{{ URL::route('attachFolderToUserfolder') }}",
						type: 'POST',
						data: {
							folderId: 		$(ui.helper).data('id'),
							userfolderId: 	$(event.target).data('id')
						}
					}).done(function(data) {
						redrawFileTree();
					});
				}
			}
		});

		@if($userfolder != null && $custom_sort_mode)

		$('.userfolder.files').sortable({
			update: function() {

	            var order = $('.userfolder.files').sortable('serialize');
	            console.log('Order: ' + order);
	            $.ajax({
	            	type: 'POST',
	            	url: '{{ URL::to('/') }}/userfolder-sort/{{$userfolder->id}}',
	            	data: order
	            }).done(function(data) {
	            	console.log(data);
	            });
	        }
		});

		@endif
	}

	function initMouseInteraction(){
		$( ".file.entry" ).hover(function(e){
			console.log('hover');
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').addClass('look-up');
		}, function(e){
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').removeClass('look-up');
		})

		$( ".folder.entry" ).hover(function(e){
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').addClass('look-up');
		}, function(e){
			$('.folder.entry[data-id='+$(this).data('parent-id')+']').removeClass('look-up');
		})
	}

	function initFileSelectable(){

		$('.file.entry .name').selectControl();

		selectedFiles.forEach(function(id){
			$('.file.entry[data-id='+id+']').addClass('checked');
		})
	}

	function toggleFolder(id){
		event.stopPropagation();
		// if($(this).hasClass('collapsed'))
		var $folder = $('.folder.entry[data-id='+id+']');
		// $folder.toggleClass('collapsed');
		if(!$folder.hasClass('collapsed')){
			$.ajax({
				url: '{{ URL::to('/') }}/folders/collapse/' + id + '/{{Auth::user()->id}}',
				type: 'POST'
			}).success(function(data) {
				collapseFolder(id);
				$('.folder.entry[data-id='+id+']').addClass('collapsed')
			});
			
		}else{
			$.ajax({
				url: '{{ URL::to('/') }}/folders/decollapse/' + id + '/{{Auth::user()->id}}',
				type: 'POST'
			}).success(function(data) {
				decollapseFolder(id);
				$('.folder.entry[data-id='+id+']').removeClass('collapsed')
			});
		}
	}

	function collapseFolder(id){
		$('.folder.entry[data-id='+id+']').addClass('collapsed');
		collapseFolders(id);
		$collapsedFolders = $('.folder.entry.collapsed');
		redrawFileTreeHeader();
	}

	function decollapseFolder(id){
		$('.folder.entry[data-id='+id+']').removeClass('collapsed');
		decollapseFolders(id);
		$collapsedFolders = $('.folder.entry.collapsed');
		redrawFileTreeHeader();
	}

	function collapseFolders(id){
		$('.file.entry[data-parent-id='+id+']').addClass('hidden');
		var $subfolders = $('.folder.entry[data-parent-id='+id+']');
		$subfolders.each(function(){
			$(this).addClass('hidden');
			collapseFolders($(this).attr('data-id'));
		})
	}

	function decollapseFolders(id){
		$('.file.entry[data-parent-id='+id+']').removeClass('hidden');
		var $subfolders = $('.folder.entry[data-parent-id='+id+']');
		$subfolders.each(function(){
			$(this).removeClass('hidden');
			if(!$(this).hasClass('collapsed')){
				decollapseFolders($(this).attr('data-id'));
			}
		})
	}

	function redrawFileTreeHeader(){
		console.log('fixwidth');
		$('.file-tree-header .col.name').css('min-width', $('.file.entry:not(.hidden) .col.name').outerWidth());
		$('.file.entry .col.order').css('min-width', $('.file-tree-header .col.order').outerWidth());
	
	}

	$('.file-tree-container').on('scroll', function(e){
		console.log($('.file-tree-container'));
		var scrollLeft = $('.file-tree-container').scrollLeft();
		$(e.target).find('.file-tree-header').css('-webkit-transform', 'translateX(-' + scrollLeft + 'px')
		// $(e.target).find('.file-tree-header').css('transform', 'translateX(-' + scrollLeft + 'px')
	})

	$('#modal').on('shown.bs.modal', function (e) {
	  $(e.target).find('input[type=text]').first().focus();
	})
</script>
@overwrite
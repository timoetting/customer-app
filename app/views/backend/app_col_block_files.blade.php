@extends('backend.app_col_block')


@section('classname')
	file-block files-manager file-manager
@overwrite	

@section('opened')
	opened
@overwrite	



@section('list-header')
	<h2 class="app-col-title">
		Ordner
	</h2>
	<div class="tool-bar">	
		<button class="tip-bottom tool-bar-btn create create-folder" title="Ordner hinzufügen"><i class="icon-plus"></i></button>
	</div>
@overwrite	

@section('list-body')
	@include('backend.folder_tree')
@overwrite	

@section('detail-header')
	<h2 class="app-col-title">Dateien</h2>
	<div class="tool-bar">
		<button class="tip-bottom tool-bar-btn add-file-btn " title="Datei hochladen"><i class="icon-plus"></i></button>
		<button class="toggle-view tool-bar-btn show-as-list active tip-bottom " onclick="toggleViewStyle('list')" title="Listenansicht"><i class="icon-align-justify"></i></button>
		<button class="toggle-view tool-bar-btn show-as-grid tip-bottom" onclick="toggleViewStyle('grid')" title="Rasteransicht"><i class="icon-grid"></i></button>
	</div>
@overwrite	 

@section('detail-body')
	<div class="drag-overlay">
		<div class="message">
			<i class="icon-outbox"></i> Datei in den aktuellen Ordner hochladen
		</div>
	</div>

	<div>
		<div id="template" class="file-entry">
			<img class="thumb" data-dz-thumbnail />
			<span class="name" data-dz-name></span>
			<strong class="error text-danger" data-dz-errormessage></strong>
			<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
				<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
			</div>
		</div>
		<!-- This is used as the file preview template -->
	</div>
	<div class="entry-list">
		Ordner auswählen.
	</div>
@overwrite	 
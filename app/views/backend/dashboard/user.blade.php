<div class="user">
	<div class="entry" data-id="{{$user->id}}">
		<div class="title">{{$user->username}} @if(!empty($user->last_name)) ({{$user->first_name}} {{$user->last_name}}) @endif</div>
		<ul class="users relations-list">
		@foreach($user->projects as $project)
			<li>{{$project->title}}</li>
		@endforeach
		</ul>
		<div class="control">
			<button class="btn-clear" title="edit folder" onclick="editUser({{$user->id}})" data-id={{$user->id}} ><i class="icon-pencil"></i></button>
			<button class="btn-clear" title="delete folder" onclick="deleteUser({{$user->id}})" data-id={{$user->id}}><i class="icon-trash-o"></i></button>
		</div>
	</div>
</div>
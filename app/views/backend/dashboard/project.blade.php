<div class="project">
	<div class="entry" data-id="{{$project->id}}">
		<div class="title"><a href="{{URL::route('project', $project->slug)}}">{{$project->title}}</a></div>
		<ul class="users relations-list">
		@foreach($project->users as $user)
			<li class="user">
				{{$user->username}} @if(!empty($user->last_name)) ({{$user->first_name}} {{$user->last_name}}) @endif
				<button class="small-control btn-clear" onclick="detachUserFromProject({{$user->id}}, {{$project->id}}, '{{$user->username}}', '{{$project->title}}')" title="detach user {{$user->username}} from project {{$project->title}}"><i class="icon-cross"></i></button>
			</li>
		@endforeach
		</ul>
		<div class="control">
			<button class="btn-clear" title="edit folder" onclick="editProject({{$project->id}})" data-id={{$project->id}} ><i class="icon-pencil"></i></button>
			<button class="btn-clear" title="delete folder" onclick="deleteProject({{$project->id}})" data-id={{$project->id}}><i class="icon-trash-o"></i></button>
		</div>
	</div>
</div>
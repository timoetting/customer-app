{{ Form::open(array('method' => 'POST', 'route' => 'users.store')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">Neuer Benutzer</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				
				<div class="form-group">
					{{ Form::label('username', 'Benutzername*: ') }}
					{{ Form::text('username', null, array('class' => 'form-control')) }}
				</div>

				<div class="form-group">
					{{ Form::label('first_name', 'Vorname: ') }}
					{{ Form::text('first_name', null, array('class' => 'form-control')) }}
				</div>

				<div class="form-group">
					{{ Form::label('last_name', 'Nachname: ') }}
					{{ Form::text('last_name', null, array('class' => 'form-control')) }}
				</div>

				<div class="form-group">
					{{ Form::label('company', 'Firma: ') }}
					{{ Form::text('company', null, array('class' => 'form-control')) }}
				</div>	

				<div class="form-group">
					{{ Form::label('password', 'Passwort: ') }}
					{{ Form::text('password', null, array('class' => 'form-control')) }}
				</div>		

				<div class="form-group">
					{{ Form::label('role', 'Rolle (nur Administrator  kann Backend einsehen): ') }}
					{{ Form::select('role', array('client' => 'Client', 'admin' => 'Administrator'), 'client', array('class' => 'form-control'));}}
				</div>	
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">abbrechen</button>
		{{Form::submit('Speichern', array('class' => 'btn btn-primary'))}}
	</div>

{{ Form::close() }}


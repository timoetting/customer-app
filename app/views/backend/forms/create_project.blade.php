{{ Form::open(array('method' => 'POST', 'route' => 'projects.store')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">New Project</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				
				<div class="form-group">
					{{ Form::label('title', 'Title*: ') }}
					{{ Form::text('title', null, array('class' => 'form-control autofocus')) }}
				</div>

				<div class="form-group">
					{{ Form::label('thumb_', 'filename of thumbnail: ') }}
					{{ Form::text('thumb', null, array('class' => 'form-control')) }}<p class="help-block">Thumbnails können unter public/thumbnails/projects/ abgelegt werden. Damit sie angezeigt werden, muss hier der Dateiname der Grafik angegeben werden.</p>
				</div>

				<div class="form-group">
					{{ Form::label('thumb_', 'project color (optional): ') }}
					<div class="input-group">
    				<span class="input-group-addon">#</span>
						{{ Form::text('color', null, array('class' => 'form-control')) }}
					</div>
					<p class="help-block">This color will set the appearance of the header in the frontend. If left empty, the header will remain gray. Otherwise, type in a hexadecimal code (without the '#')</p>
				</div>

			</div>
				<div class="col-md-12">
					<h3>Meta Infirmations</h3>
				</div>
			<!-- <div class="col-md-6"> -->
				<div class="form-group col-md-6">
					{{ Form::text('meta_info_titles[]', null, array('class' => 'form-control', 'placeholder' => 'title'))  }}
				</div>
				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_contents[]', null, array('class' => 'form-control', 'placeholder' => 'content')) }}
				</div>	

				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_titles[]', null, array('class' => 'form-control', 'placeholder' => 'title')) }}
				</div>	
				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_contents[]', null, array('class' => 'form-control', 'placeholder' => 'content')) }}
				</div>	

				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_titles[]', null, array('class' => 'form-control', 'placeholder' => 'title')) }}
				</div>	
				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_contents[]', null, array('class' => 'form-control', 'placeholder' => 'content')) }}
				</div>

				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_titles[]', null, array('class' => 'form-control', 'placeholder' => 'title')) }}
				</div>	
				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_contents[]', null, array('class' => 'form-control', 'placeholder' => 'content')) }}
				</div>	
				
				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_titles[]', null, array('class' => 'form-control', 'placeholder' => 'title')) }}
				</div>	
				<div class="form-group  col-md-6">
					{{ Form::text('meta_info_contents[]', null, array('class' => 'form-control', 'placeholder' => 'content')) }}
				</div>	

			<!-- </div>

			<div class="col-md-6"> -->

				


			<!-- </div> -->
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">abbrechen</button>
		{{Form::submit('Speichern', array('class' => 'btn btn-primary'))}}
	</div>

{{ Form::close() }}
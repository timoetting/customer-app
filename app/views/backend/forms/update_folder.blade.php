
{{ Form::model($folder, array('method' => 'PATCH', 'route' => array('folders.update', $folder->id))) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">Edit folder {{$folder->title}} {{$folder->id}}</h4>
	</div>
	<div class="modal-body">
		<div class="form-group">
			{{ Form::label('title', 'Ordnername: ') }}
			{{ Form::text('title', $folder->title, array('class' => 'form-control')) }}
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
		{{Form::submit('save', array('class' => 'btn btn-primary'))}}
	</div>
{{ Form::close() }}
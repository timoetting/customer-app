{{ Form::model($userfolder, array('method' => 'PATCH', 'route' => array('userfolders.update', $userfolder->id))) }}

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Edit user folder {{$userfolder->title}}</h4>
</div>
<div class="modal-body">
	<div class="form-group">
		{{ Form::label('title', 'Title: ') }}
		{{ Form::text('title', null, array('class' => 'form-control')) }}
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label for="sorting">Sorting:</label>
				{{Form::select('sorting', array('title' => 'title', 'date' => 'date', 'default' => 'custom sorting'), $userfolder->sorting(), array('class' => 'form-control'));}}
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="view_style">View Style</label>
				{{Form::select('view_style', array('s' => 'small list', 'm' => 'medium list', 'l' => 'big thumbnails'), $userfolder->view_style(), array('class' => 'form-control'));}}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="checkbox">
				<label>
					{{Form::checkbox('is_default');}}
					Show this userfolder as default folder
				</label>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
	{{Form::submit('save', array('class' => 'btn btn-primary'))}}
</div>
{{ Form::close() }}
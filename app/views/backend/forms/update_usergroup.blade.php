<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Benutzer bearbeiten</h4>
</div>
{{ Form::model($usergroup, array('method' => 'PATCH', 'route' => array('usergroups.update', $usergroup->id))) }}
<div class="modal-body">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">

			<div class="form-group">
				{{ Form::label('title', 'Name der Gruppe: ') }}
				{{ Form::text('title', null, array('class' => 'form-control')) }}
			</div>			

		</div>
	</div>
</div>

<div class="form-group modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">abbrechen</button>
	{{Form::submit('Speichern', array('class' => 'btn btn-primary'))}}
</div>

<div class="modal-header">
	<h4 class="modal-title" id="myModalLabel">Benutzer der Gruppe</h4>
</div>

<div class="modal-body">

	<ul class="list-group file-list">
		@foreach($usergroup->users->sortBy('path') as $user)
		<li class="list-group-item">{{$user->username}} <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></li>
		@endforeach
	</ul>

</div>

<div class="modal-header">
	<h4 class="modal-title" id="myModalLabel">Dateien der Gruppe</h4>
</div>

<div class="modal-body">

	<ul class="list-group file-list">
		@foreach($usergroup->customerFiles->sortBy('path') as $file)
		<li class="list-group-item"><span class="path">{{$file->path}}</span>{{$file->file_name}} <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></li>
		@endforeach
	</ul>

</div>

{{ Form::close() }}

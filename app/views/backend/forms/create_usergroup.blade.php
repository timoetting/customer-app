{{ Form::open(array('method' => 'POST', 'route' => 'usergroups.store')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">Neue Benutzergruppe</h4>
	</div>
	<div class="modal-body">
		<div class="form-group">
			{{ Form::label('title', 'Name der Gruppe*: ') }}
			{{ Form::text('title', null, array('class' => 'form-control')) }}
		</div>			
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">abbrechen</button>
		{{Form::submit('Speichern', array('class' => 'btn btn-primary'))}}
	</div>
{{ Form::close() }}
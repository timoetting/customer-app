{{ Form::open(array('method' => 'POST', 'route' => 'folders.store')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">New folder</h4>
	</div>
	<div class="modal-body">
		<div class="form-group">
			{{ Form::label('title', 'Ordnername: ') }}
			{{ Form::text('title', null, array('class' => 'form-control')) }}
		</div>
	</div>
	<div class="modal-footer">
		<input class="parent-id" name="parent-id" type="hidden" value="">
		<button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
		{{Form::submit('add folder', array('class' => 'btn btn-primary'))}}
	</div>
{{ Form::close() }}
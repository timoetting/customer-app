<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">edit file</h4>
</div>
<div class="modal-body">
	<div class="thumbnail">
		<div class="toggle-thumbnail" onclick="uploadThumb()">
			@if($file->thumb != null)
			<p>
				<img src="{{URL::to(Config::get('paths.thumbnailsFilesMd').$file->thumb)}}"  class="img-responsive" alt="">
			</p>
			@endif
		</div>

		{{ Form::open(array('method' => 'POST', 'route' => 'uploadThumb', 'files' => true, 'id' => 'upload-thumb')) }}
			{{ Form::file('thumb', array('class' => 'btn-thumb-upload hidden')); }}
			{{ Form::hidden('fileId', $file->id) }}
			<div class="form-group">
			<p>
				<a href="#0" class="btn-primary btn-sm" onclick="selectThumb({{$file->id}})">change thumb</a>
			</p>
			</div>
			{{Form::submit('change thumb', array('class' => 'btn btn-default btn-sm hidden'))}}
		{{ Form::close() }}
	</div>
</div>
{{ Form::model($file, array('method' => 'PATCH', 'route' => array('files.update', $file->id), 'id' => 'update-file')) }}
<div class="modal-body">

	<div class="form-group">
		{{ Form::label('filename', 'Filename: ') }}
		{{ Form::text('filename', $file->getFileName(), array('class' => 'form-control')) }}
	</div>	

	<div class="form-group">
		{{ Form::label('title', 'Title: ') }}
		{{ Form::text('title', null, array('class' => 'form-control')) }}
	</div>			

	<div class="form-group">
		{{ Form::label('description', 'Note: ') }}
		{{ Form::text('description', null, array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group modal-footer">
	{{Form::submit('save', array('class' => 'btn btn-primary'))}}
	<button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
</div>
{{ Form::close() }}

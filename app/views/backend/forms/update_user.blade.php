<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Benutzer bearbeiten</h4>
</div>
{{ Form::model($user, array('method' => 'PATCH', 'route' => array('users.update', $user->id))) }}
<div class="modal-body">
	<div class="row">
		<div class="col-md-6">

			<div class="form-group">
				{{ Form::label('username', 'Username: ') }}
				{{ Form::text('username', null, array('class' => 'form-control')) }}
			</div>
			<div class="form-group">
				{{ Form::label('company', 'Company: ') }}
				{{ Form::text('company', null, array('class' => 'form-control')) }}
			</div>		
			<div class="form-group">
				{{ Form::label('password', 'New Passwort: ') }}
				{{ Form::text('password', '', array('class' => 'form-control', 'placeholder' => '&#9679;&#9679;&#9679;&#9679;&#9679;')) }}
			</div>		

		</div>
		<div class="col-md-6">

			<div class="form-group">
				{{ Form::label('first_name', 'First Name: ') }}
				{{ Form::text('first_name', null, array('class' => 'form-control')) }}
			</div>
			<div class="form-group">
				{{ Form::label('last_name', 'Last Name: ') }}
				{{ Form::text('last_name', null, array('class' => 'form-control')) }}
			</div>
			<div class="form-group">
				{{ Form::label('role', 'Role (only admin can view and edit Backend): ') }}
				{{Form::select('role', array('client' => 'Client', 'admin' => 'Administrator'), null, array('class' => 'form-control'));}}
			</div>	
		</div>
	</div>
</div>

<div class="form-group modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">abbrechen</button>
	{{Form::submit('Speichern', array('class' => 'btn btn-primary'))}}
</div>

{{ Form::close() }}



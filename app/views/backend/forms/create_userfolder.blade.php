{{ Form::open(array('method' => 'POST', 'route' => 'userfolders.store')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">Neuer Benutzerordner</h4>
	</div>
	<div class="modal-body">
		<div class="form-group">
			{{ Form::label('title', 'Name des Benutzerordners*: ') }}
			{{ Form::text('title', null, array('class' => 'form-control')) }}
		</div>			
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label for="sorting">Sorting:</label>
					{{Form::select('sorting', array('default' => 'custom sorting', 'title' => 'title', 'date' => 'date'), 'default', array('class' => 'form-control'));}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="view_style">View Style</label>
					{{Form::select('view_style', array('s' => 'small list', 'm' => 'medium list', 'l' => 'big thumbnails'), 'm', array('class' => 'form-control'));}}
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<input class="current-userfolder-path" name="current-userfolder-path" type="hidden" value="">
		<input class="current-user-id" name="current-user-id" type="hidden" value="">
		<input class="current-project-id" name="current-project-id" type="hidden" value="">
		<input class="parent-id" name="parent-id" type="hidden" value="">
		<button type="button" class="btn btn-default" data-dismiss="modal">abbrechen</button>
		{{Form::submit('Speichern', array('class' => 'btn btn-primary'))}}
	</div>
{{ Form::close() }}
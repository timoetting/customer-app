<html>
<head>
	<meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/icon-style.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/style.css') }}">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
</head>
<body class="view-size-{{$view_size}}">
	<header class="page-header">
		<div class="breadcrumbs">@yield('breadcrumbs')</div>
		<div class="backend-user-actions">Signed in as {{Auth::user()->username}} | <a href="{{URL::route('logout')}}" title="Logout">Logout</a></div>
		<div class="clear"></div>
	</header>
	<div class="app-container">
		@yield('content')
	</div> <!-- app-container -->
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/min/bootstrap.min.js') }}"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/jquery-ui.min.js') }}"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/dropzone.min.js') }}"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/ajaxform.js') }}"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/backend.js') }}"></script>
	@yield('javascript')
</body>
</html>
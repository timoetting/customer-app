@extends('backend.app_col_block')


@section('classname')
	project-block
@overwrite	

@section('opened')
	
@overwrite	



@section('list-header')
	<h2 class="app-col-title">
	Projekte
	</h2>
	<div class="tool-bar">	
		<button class="tip-bottom tool-bar-btn create create-project" title="Projekt hinzufügen"><i class="icon-plus"></i></button>
	</div>
@overwrite	

@section('list-body')
	<ul class="entry-list">
		@foreach($projects as $project)
		<li>
			<div class="project entry @if($currentProject != null && $currentProject->id == $project->id) active @endif" data-id="{{$project->id}}">
@if($currentProject == null || $currentProject->id != $project->id)
				<a href="{{URL::to('/backend/project/' . $project->slug )}}"><div class="icon"><i class="icon-box"></i></div> {{$project->title}}</a>
@else
				<a href="{{URL::to('/backend')}}"><div class="icon"><i class="icon-box"></i></div> {{$project->title}}</a>
@endif
				<div class="control">
					<button class="btn-clear open-detail" title="Projektinfos einblenden" data-id="{{$project->id}}"><i class="icon-arrow-right"></i></button>
					<button class="btn-clear close-detail" title="Projektinfos ausblenden"><i class="icon-arrow-left"></i></button>
				</div>
			</div>
		</li>
		@endforeach
	</ul>
@overwrite	

@section('detail-header')
	<h2 class="app-col-title">Details zum aktuellen Projekt</h2>
	<div class="tool-bar">
		<button class="btn btn-xs btn-primary" title="aktuelles Projekt bearbeiten">bearbeiten</button>
		<button class="btn btn-xs btn-primary" title="aktuelles Projekt löschen">löschen</button>
	</div>
@overwrite	 

@section('detail-body')
	nichts drin
@overwrite	 
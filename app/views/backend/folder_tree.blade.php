<ul class="folder-tree">
	<?php 
	$foldersTree = CustomerFilesController::build_tree($foldersArray); 
	drawFolderTree($foldersTree, '' );
	?>
</ul>

<?php 


function drawFolderTree($dirArray, $path){
	foreach ($dirArray as $dirName => $value) { ?>
		@if ($value == null)
		    <li><a href="#0" data-path="{{$path.$dirName}}/"><i class="icon-folder"></i> {{$dirName}}</a></li>
		@else
			<li class="has-child opened"><a href="#0" data-path="{{$path.$dirName}}/"><div class="toggle-btn"></i><i class="icon-folder"></i></div></i> {{$dirName}}</a>
			<ul>
				<?php drawFolderTree($value, $path.$dirName.'/'); ?>
			</ul>
		@endif
	<?php }
}

 ?>
<div class="usergroup entry" data-id="{{$usergroup->id}}">
	<div class="header collapsed" data-toggle="collapse" data-target="#collapse-usergroup-{{$usergroup->id}}">
		<i class="icon-users"></i> {{$usergroup->title}}
		<div class="control">
			<button class="btn-clear edit" title="Benutzergruppe bearbeiten" data-id="{{$usergroup->id}}"><i class="icon-menu"></i></button>
		</div>
	</div>
	<div id="collapse-usergroup-{{$usergroup->id}}" class="collapse">
		<div class="collapse-body">
			<div class="row">
				<div class="col-md-4">
					<h6>Benutzer</h6>
					<ul class="users">
						@foreach($usergroup->users as $user)
						<li>{{$user->username}}</li>
						@endforeach
					</ul>
				</div>
				<div class="col-md-8">
					<h6>Dateien</h6>
					<ul class="files">
						@foreach($usergroup->customerFiles as $file)
						<li>{{$file->title}}</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@if ($userfolder->children()->count() == 0)
    <li>
    	<div class="userfolder entry @if($activeUserfolder->id == $userfolder->id) active @endif" data-id="{{$userfolder->id}}">
	    	<a href="#0"><i class="icon-folder"></i> {{$userfolder->title}}</a>
			<div class="control">	
				<button class="tip-bottom create create-userfolder" data-parent-id="{{$userfolder->id}}" title="Unterordner hinzufügen"><i class="icon-plus"></i></button>
			</div>
    	</div>
    </li>
@else
	<li class="has-child opened">
		<div class="userfolder entry @if($activeUserfolder->id == $userfolder->id) active @endif" data-id="{{$userfolder->id}}">
			<a href="#0"><div class="toggle-btn"></i><i class="icon-folder"></i></div></i> {{$userfolder->title}}</a> 
			<div class="control">	
				<button class="tip-bottom create create-userfolder" data-parent-id="{{$userfolder->id}}" title="Unterordner hinzufügen"><i class="icon-plus"></i></button>
			</div>
		</div>
		<ul>
			@foreach($userfolder->children as $childFolder)
				@include('backend.userfolder_recursive', array('userfolder' => $childFolder))
			@endforeach
		</ul>
	</li>
@endif
<ul class="userfolder files">
	@if(count($userfolder->customerFiles))
	@foreach($userfolder->customerFiles as $file)
		@if(!($file->hasVideoBrother() && $file->file_type == 'webm'))
			<div id="file_{{$file->id}}" class="file entry" data-id={{$file->id}}>

				<div class="thumb">
					@if($file->thumb == null || empty($file->thumb))
					<div class="placeholder">
						<div class="file-type">
							{{$file->file_type}}
						</div>
					</div>
					@else

					<img src="{{URL::to(Config::get('paths.thumbnailsFilesSm').$file->thumb)}}" class="img-responsive" alt="">
					@endif
				</div>
				<div class="file-info"> 
					<div class="title">
						@if($file->isVideo() )
							<i class="icon-film video-indicator"></i>
							{{$file->file_name}}
							@if($file->hasVideoBrother())
								<span class="secondary">(.mp4|.webm)</span>
							@endif
						@else
							{{$file->title}}
						@endif
					</div>
					<div class="file-name">{{$file->file_name}}</div> 
				</div>
			</div>
		@endif
	@endforeach
	@endif
</ul>

<?php 
$files = ($custom_sort_mode) ? $userfolder->customerFiles : $userfolder->customerFiles->sortBy($sorting, SORT_NATURAL);
//$files = array_unique($files);
//$files = $files->unique();

 ?>
 <div class="file-tree-header">
 	@if($custom_sort_mode)
 	<div class="col order">order<span class="caret"></span></div>
 	@else
 	<div class="col order"><a href="?sorting={{$sorting}}&view_size={{$view_size}}&custom_sort_mode=true">order</a></div>
 	@endif
 	@if(!$custom_sort_mode && $sorting == 'file_name')
 	<div class="col name">name<span class="caret"></span></div>
 	@else
 	<div class="col name"><a href="?sorting=file_name&view_size={{$view_size}}">name</a></div>
 	@endif
 	@if(!$custom_sort_mode && $sorting == 'created_at')
 	<div class="col date">date<span class="caret"></span></div>
 	@else
 	<div class="col date"><a href="?sorting=created_at&view_size={{$view_size}}">date</a></div>
 	@endif

 	<div class="col size">size</div>
 	<div class="col userfolder"></div>
 	<div class="col fill"></div>
 </div>

@foreach($files as $file)
	@if(!($file->hasVideoBrother() && $file->file_type == 'webm'))
		<div id="file_{{$file->id}}" class="file entry {{$file->file_type}} depth-0" data-id={{$file->id}}>
			<div class="col order">
			@if($custom_sort_mode)
			<i class="icon-menu2"></i>
			@else
			{{$file->pivot->sort + 1}}
			@endif
			</div>
			<div class="col name file" data-id={{$file->id}}>
				@if($view_size == 's')
				<div class="icon"></div>
				@else
				<div class="thumb">
					@if($file->thumb == null || empty($file->thumb))
					<div class="placeholder">
						<div class="file-type">
							{{$file->file_type}}
						</div>
					</div>
					@else

					<img src="{{URL::to(Config::get('paths.thumbnailsFilesSm').$file->thumb)}}" alt="">
					@endif
				</div>
				@endif
				<span class="file-name">
							{{$file->id}} {{$file->file_name}}</span> 
			</div>
			<div class="col date">{{date("d.m.y", strtotime($file->created_at))}} <span class="secondary">{{date("H:i", strtotime($file->created_at))}}</span></div>
			<div class="col size">{{number_format(File::size(public_path().'/'.$file->path.$file->file_name)/1048576, 2)}} MB</div>
			<div class="col fill"></div>
		</div>
	@endif
@endforeach

<?php 
	if(!isset($depth)){
		$depth = 0;
	}
?>
@if($depth == 0)
	<div class="file-tree-header">
		@if($sorting == 'file_name')
		<div class="col name">name<span class="caret"></span></div>
		@else
		<div class="col name"><a href="?sorting=file_name">name</a></div>
		@endif
		@if($sorting == 'created_at')
		<div class="col date">date<span class="caret"></span></div>
		@else
		<div class="col date"><a href="?sorting=created_at">date</a></div>
		@endif

		<div class="col size">size</div>
		@foreach($users as $user)
		<div class="col user">
			<a href="{{ URL::route('user-in-project', array('slug' => $project->slug, 'username' => $user->username)) }}">
				<span class="username">{{$user->username}}</span> 
			</a>
		</div>
		@endforeach
		<div class="col fill"></div>
	</div>
@endif
<div class="folder entry depth-{{{ $depth or 0 }}}" data-id={{$folder->id}} data-parent-id={{$parent->id}}>
	<div class="col name folder" data-id={{$folder->id}}>
	<div class="icons">
		<button class="toggle-btn" @if($depth != 0)onclick="toggleFolder({{$folder->id}})"@endif>
			<div class="toggle-arrow"></div>
			<div class="icon"></div>
		</button>
	</div>
	<span class="folder-name">{{$folder->title}}</span> 
	<div class="control">
		<button class="btn-clear" title="add folder" onclick="createFolder({{$folder->id}})" data-id={{$folder->id}} ><i class="icon-folder-plus"></i></button>
		<button class="btn-clear upload-file" title="upload file" onclick="uploadFile({{$folder->id}})" data-id={{$folder->id}}><i class="icon-file-plus"></i></button>
		@if($depth != 0)
			<button class="btn-clear" title="edit folder" onclick="editFolder({{$folder->id}})" data-id={{$folder->id}} ><i class="icon-pencil"></i></button>
			<button class="btn-clear" title="delete folder" onclick="deleteFolder({{$folder->id}})" data-id={{$folder->id}}><i class="icon-trash-o"></i></button>
		@endif
	</div>
	</div>
	<div class="col date">{{date("d.m.y", strtotime($folder->created_at))}} <span class="secondary">{{date("H:i", strtotime($folder->created_at))}}</span></div>
	<div class="col size"></div>
	@foreach($users as $user)
		<div class="col user-toggle">
			@if($folder->hasUser($user->id))
			<div class="toggle active" onclick="toggleFolderUserRelation({{$folder->id}}, {{$user->id}} )"></div>
			@else
			<div class="toggle" onclick="toggleFolderUserRelation({{$folder->id}}, {{$user->id}} )"></div>
			@endif
		</div>
	@endforeach
	<div class="col fill"></div>
</div>





@foreach($folder->customerFiles->sortBy($sorting, SORT_NATURAL) as $file)
	@if(!($file->hasVideoBrother() && $file->file_type == 'webm'))
		<div class="file entry {{$file->file_type}} depth-{{{isset($depth) ? $depth + 1 : 1}}}" data-parent-id={{$folder->id}} data-id={{$file->id}}>
			<div class="col name file" data-id={{$file->id}}>
				@if($view_size == 's')
				<div class="icon"></div>
				@else
				<div class="thumb">
					@if($file->thumb == null || empty($file->thumb))
					<div class="placeholder">
						<div class="file-type">
							{{$file->file_type}}
						</div>
					</div>
					@else

					<img src="{{URL::to(Config::get('paths.thumbnailsFilesSm').$file->thumb)}}" alt="">
					@endif
				</div>
				@endif
				<span class="file-name">{{$file->file_name}}</span> 
				<div class="control">
					<button class="btn-clear" title="edit file" onclick="@if($file->isVideo() ) editVideo({{$file->id}}) @else editFile({{$file->id}}) @endif" data-id={{$file->id}} ><i class="icon-pencil"></i></button>
					<button class="btn-clear" title="delete file" onclick="deleteFile({{$file->id}})" data-id={{$file->id}}><i class="icon-trash-o"></i></button>
				</div>
			</div>
			<div class="col date">{{date("d.m.y", strtotime($file->created_at))}} <span class="secondary">{{date("H:i", strtotime($file->created_at))}}</span></div>
			<div class="col size">{{number_format(File::size(public_path().'/'.$file->path.$file->file_name)/1048576, 2)}} MB</div>
			@foreach($users as $user)
				<div class="col user-toggle">
					@if($file->hasUser($user->id))
					<div class="toggle active" onclick="toggleFileUserRelation(this, {{$file->id}}, {{$user->id}} )"></div>
					@else
					<div class="toggle" onclick="toggleFileUserRelation(this, {{$file->id}}, {{$user->id}} )"></div>
					@endif
				</div>
			@endforeach
			<div class="col fill"></div>
		</div>
	@endif
@endforeach


@if (count($folder->children) != 0)
	<?php 
	$childFolders = $folder->children;
	//$childFolders->load('users');
	 ?>
	@foreach($childFolders as $childFolder)
		@include('backend.file_tree', array('folder' => $childFolder, 'depth' => $depth + 1, 'parent' =>$folder))
	@endforeach
@endif
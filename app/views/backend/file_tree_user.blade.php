<?php 
	if(!isset($depth)){
		$depth = 0;
	}
?>

@if($depth == 0)
	<div class="file-tree-header">
		@if($sorting == 'file_name')
		<div class="col name">name<span class="caret"></span></div>
		@else
		<div class="col name"><a href="?sorting=file_name">name</a></div>
		@endif
		@if($sorting == 'created_at')
		<div class="col date">date<span class="caret"></span></div>
		@else
		<div class="col date"><a href="?sorting=created_at">date</a></div>
		@endif

		<div class="col size">size</div>
		<div class="col userfolder"></div>
		<div class="col fill"></div>
	</div>
@endif

<div class="folder entry depth-{{{ $depth or 0 }}}" data-id={{$folder->id}} data-parent-id={{$parent->id}}>
	<div class="col name folder" data-id={{$folder->id}}>
	<div class="icons">
    	<button class="toggle-btn" onclick="toggleFolder({{$folder->id}})">
    		<div class="toggle-arrow"></div>
    		<div class="icon"></div>
    	</button>
	</div>
	<span class="folder-name">{{$folder->title}}</span>
	<div class="control">
	</div>
	</div>
	<div class="col date">{{date("d.m.y", strtotime($folder->created_at))}} <span class="secondary">{{date("H:i", strtotime($folder->created_at))}}</span></div>
	<div class="col size"></div>
	<div class="col userfolder">

	@if(count($folder->userfolders) != 0 && count($user->userfolders))
		<?php $folderUserfolder = $folder->userfolders->intersect($user->userfolders)->first() ?>
		@if($folderUserfolder != null)
		{{$folder->userfolders->intersect($user->userfolders)->first()->title}}
		@endif
	@endif
	</div>
	<div class="col fill"></div>
</div>


@foreach($folder->customerFiles->sortBy($sorting, SORT_NATURAL) as $file)
@if(!($file->hasVideoBrother() && $file->file_type == 'webm'))
	@if($file->hasUser($user->id))
		<div class="file entry {{$file->file_type}} depth-{{{isset($depth) ? $depth + 1 : 1}}} " data-parent-id={{$folder->id}} data-id={{$file->id}}>
			<div class="col name file" data-id={{$file->id}}>

				@if($view_size == 's')
				<div class="icon"></div>
				@else
				<div class="thumb">
					@if($file->thumb == null || empty($file->thumb))
					<div class="placeholder">
						<div class="file-type">
							{{$file->file_type}}
						</div>
					</div>
					@else

					<img src="{{URL::to(Config::get('paths.thumbnailsFilesSm').$file->thumb)}}" alt="">
					@endif
				</div>
				@endif
				<span class="file-name">{{$file->file_name}}</span> 
				
				<div class="control">
					<button class="btn-clear" title="remove from user" onclick="toggleFileUserRelation({{$file->id}}, {{$user->id}})" data-id={{$file->id}}><i class="icon-cross"></i></button>
				</div>
			</div>
			<div class="col date">{{date("d.m.y", strtotime($file->created_at))}} <span class="secondary">{{date("H:i", strtotime($file->created_at))}}</span></div>
			<div class="col size">{{number_format(File::size(public_path().'/'.$file->path.$file->file_name)/1048576, 2)}} MB</div>
			<div class="col userfolder">

			@if(count($file->userfolders) != 0 && count($user->userfolders))
				<?php $fileUserfolder = $file->userfolders->intersect($user->userfolders)->first() ?>
				@if($fileUserfolder != null)
				{{$file->userfolders->intersect($user->userfolders)->first()->title}}
				@endif
			@endif
			</div>
			<div class="col fill"></div>

		</div>
	@endif
@endif
@endforeach
@if (count($folder->children) != 0)
	<?php 
	$childFolders = $folder->children;
	//$childFolders->load('users');
	$user->load('userfolders');
	 ?>
	@foreach($childFolders as $childFolder)
		@include('backend.file_tree_user', array('folder' => $childFolder, 'depth' => $depth + 1, 'parent' =>$folder))
	@endforeach
@endif
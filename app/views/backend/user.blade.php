<div class="user entry" data-id="{{$user->id}}">
	<div class="icon"><i class="icon-user"></i></div> {{$user->username}} <span class="secondary name">{{$user->first_name}} {{$user->last_name}}</span>
	<div class="control">
		<button class="btn-clear edit" title="Benutzer bearbeiten" data-id="{{$user->id}}"><i class="icon-menu"></i></button>
	</div>
</div>
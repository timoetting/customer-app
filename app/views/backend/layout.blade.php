<html>
<head>
	<meta charset="utf-8"> 
	<title>Kundenportal</title>
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/icon-style.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/style.css') }}">
	<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="app-container">
		@include('backend.app_col_block_projects')
		@include('backend.app_col_block_files')
		@include('backend.app_col_block_users')


	</div> <!-- app-container -->

	<!-- Modal -->
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

			</div>
		</div>
	</div>
	<div class="modal fade" id="welcome-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h2>Dateisynchronisation</h2>
				</div>
				<div class="modal-body">
					@if ( !empty($filesDiff[0]) || !empty($filesDiff[1]) )
					@if (!empty($filesDiff[0]))
					<div class="new-files">
						<h4>Hinzugefgte Dateien</h4>
						<p>Diese neuen Dateien wurden auf dem Server entdeckt. Sollen diese ins Kundenportal übernommen werden?</p>
						<ul class="list-group new-files">
							@foreach ($filesDiff[0] as $filePath)
							<li class="list-group-item list-group-item-success">
								{{$filePath}}
							</li>
							@endforeach
							<li class="list-group-item">
								<button class="btn btn-default pull-right" onclick="addNewFiles();">Dateien aufnehmen</button><div class="clear"></div>
							</li>
						</ul>
					</div>
					@endif
					@endif

					@if (!empty($filesDiff[1]) || !empty($filesDiff[1]))
					@if (!empty($filesDiff[1]))
					<div class="not-found-files">
						<h4>Diese Dateien konnten nicht gefunden werden</h4>
						<p>Diese Dateien wurdem vom System nicht gefunden. Möglicherweise wurden die entsprechenden Dateien über FTP verschoben, sodass die Verweise der Einträge nicht mehr aktuell sind.</p>
						<ul class="list-group not-found-files">
							@foreach ($filesDiff[1] as $key => $filePath)
							<li class="list-group-item list-group-item-warning" data-file-id="{{$key}}">
								{{$filePath}} <button class="btn btn-default btn-xs pull-right btn-danger" onclick="removeFile({{$key}})">Eintrag entfernen <i class="icon-trash"></i></button>
							</li>
							@endforeach
						</ul>
					</div>
					@endif
					@endif
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/min/bootstrap.min.js') }}"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/jquery-ui.min.js') }}"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/dropzone.min.js') }}"></script>
	<script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/ajaxform.js') }}"></script>
	<!-- <script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/swiper.js') }}"></script> -->
	<!-- <script async="" type="text/javascript" charset="UTF-8" src="{{ URL::asset('javascripts/jquery.kinetic.min.js') }}"></script> -->
	<script>
		currentState = {
			currentDir: '{{Config::get('paths.baseDir')}}/',
			user: null,
			userfolder: null,
			@if($currentProject != null)
			projectId: {{$currentProject->id}}
			@else
			projectId: null
			@endif
		}

			//currentDir = '{{Config::get('paths.baseDir')}}/';
			mouse = {x: 0, y: 0};

			$(window).load(function(){
				initFolderTree();

	// $('.app-container').kinetic();

	@if (!empty($filesDiff[0]) || !empty($filesDiff[1]))
	$('#welcome-modal').modal('toggle');
	@endif

	// $('.files-container .row.file .btn').click(function(e){
	// 	e.stopPropagation();
	// });

	// $('.files-container .row.file').click(function(){
	// 	$('.files-container .row.file').removeClass('active');
	// 	$('.files-container .file-panel').removeClass('active');
	// 	// $(this).parent('.row.file').addClass('active');
	// 	$(this).addClass('active');
	// 	$('.files-container .file-panel').addClass('active');
	// });

	// $('.files-container .file-panel button.close').click(function(){
	// 	$('.files-container .row.file').removeClass('active');
	// 	$('.files-container .file-panel').removeClass('active');
	// });

	$('.create-folder').click(function(e){
		e.stopPropagation();

		$.ajax({
			url: '{{ URL::to('/') }}/folder/create'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal .modal-content').find('.current-dir').val( currentState['currentDir'] );
			$('#modal .modal-content').find('.modal-title').append('<br><small>' + currentState['currentDir'] + '</small>');

			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					$('#modal').modal('toggle');
					$.ajax({
						url: '{{action('BackendController@getFolderTreeView')}}'

					}).done(function(data) {
						$('.app-col .folder-tree').replaceWith(data);
						initFolderTree();
					});

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	})

	

	$('.create-user').click(function(e){

		e.stopPropagation();

		$.ajax({
			url: '{{ URL::to('/') }}/users/create'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					$('.app-col.users .user-list').append('<li>' + response + '</li>');
					$('#modal').modal('toggle');
					initUserInteraction();
					reInitDragDrop(false, false, true, false);
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	})

	$('.create-project').click(function(e){

		e.stopPropagation();

		$.ajax({
			url: '{{ URL::to('/') }}/projects/create'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					// $('.app-col.users .user-list').append('<li>' + response + '</li>');
					// $('#modal').modal('toggle');
					// initUserInteraction();
					// reInitDragDrop(false, false, false, true);
					location.reload();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	})

$('.app-col-list .app-col-header').click(function(e){
	// $(e.target).parents('.app-col-block').toggleClass('opened');
});

// $('.user-manager .app-col-list .user.entry').click(function(e){
// 	var userId = $(this).data('id');
// 	toggleUser(userId);
// });

$('.user-block .app-col-list .user.entry .open-detail').click(function(e){
	var userId = $(this).data('id');

	toggleUser(userId);
	$('.user-block').addClass('opened');
	$('.user-block .app-col-list .user.entry').removeClass('opened');
	$(this).parents('.user.entry').addClass('opened');
});

$('.user-block .app-col-list .user.entry .close-detail').click(function(e){
	$('.user-block').removeClass('opened');
	$('.user-block .app-col-list .user.entry').removeClass('opened');
});



$('.project-block .app-col-list .project.entry .open-detail').click(function(e){
	$('.project-block').addClass('opened');
	$('.project-block .app-col-list .project.entry').removeClass('opened');
	$(this).parents('.project.entry').addClass('opened');
});

$('.project-block .app-col-list .project.entry .close-detail').click(function(e){
	$('.project-block').removeClass('opened');
	$('.project-block .app-col-list .project.entry').removeClass('opened');
});

document.addEventListener('mousemove', function(e){ 
	mouse.x = e.clientX || e.pageX; 
	mouse.y = e.clientY || e.pageY 
}, false);

reInitDragDrop(true, true, true, true);

initUserInteraction();
initUsergroupInteraction();
initDropzone('{{Config::get('paths.baseDir')}}');

$(".tip-top").tooltip({placement : 'top'});
$(".tip-right").tooltip({placement : 'right'});
$(".tip-bottom").tooltip({placement : 'bottom'});
$(".tip-left").tooltip({ placement : 'left'});


function toggleUser(userId, userfolderId){

	@if($currentProject != null)

	console.log('toggleUser');
	currentUserFolderId = null;

	$.ajax({
		url: '{{ URL::to('/') }}/user/'+userId+'/'+{{$currentProject->id}}+'/'+userfolderId
	}).done(function(data) {
		$('.user-manager .app-col-detail').html(data['html']);
		currentState["user"]= data['user'];
		currentState["userfolder"]= data['userfolder'];
		reInitDragDrop(0,1,0,0,1);
		initUserInteraction();
	});

	

	@endif

}

function toggleUserfolder(userfolderId){
	console.log(userId);
	currentUserFolderId = null;

	$.ajax({
		url: '{{ URL::to('/') }}/users/'+userId
	}).done(function(data) {
		$('.user-manager .app-col-detail .app-col-body').html(data);
	});
}

function toggleViewStyle(style){
	$('.tool-bar .show-as-list, .tool-bar .show-as-grid').removeClass('active');

	if(style == 'grid'){
		console.log('grid');
		$('.files-container').removeClass('list');
		$('.files-container').addClass('grid');
		$('.tool-bar .show-as-grid').addClass('active');
	} 
	if(style == 'list'){
		$('.files-container').removeClass('grid');
		$('.files-container').addClass('list');
		$('.tool-bar .show-as-list').addClass('active');
	}
}

function addNewFiles(){
	$.ajax({
		url: '{{ URL::to('/') }}/files/add-new-files'

	}).done(function(data) {
		window.location.reload();
	});
}

function removeFile(id){
	$.ajax({
		url: 'files/' + id,
		type: 'DELETE',
		success: function(){
			$('#welcome-modal .not-found-files .list-group li[data-file-id="' + id + '"]').remove();
			checkWelcomeModal();
		}
	})
}

function checkWelcomeModal(){
	if ( $('#welcome-modal .new-files .list-group').html() == '' ){
		$('#welcome-modal .new-files').remove();
		checkWelcomeModal();
	}
	if ( $('#welcome-modal .not-found-files .list-group').html() == '' ){
		$('#welcome-modal .not-found-fafiles').remove();
		checkWelcomeModal();
	}
	if( $('#welcome-modal modal-body').html == ''){
		$('#welcome-modal').modal('toggle');
	}
}

function initFolderTree(){
	$('.folder-tree li.has-child .toggle-btn').click(function(e){
		e.stopPropagation();
		console.log('button click');
		$(this).closest('li.has-child').toggleClass('opened');
	});

	$('.folder-tree li a').click(function(e){
		changeDir($(e.target).data('path'));
	});

	reInitDragDrop(true, false, false, false);
}

function initUserInteraction(){
	$('.user.entry .control .edit').unbind( "click" );
	$('.user.entry .control .edit').click(function(e){

		e.stopPropagation();
		
		$.ajax({
			url: '{{ URL::to('/') }}/users/' + $(this).data('id') + '/edit'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					var userId = $(response).data('id');
					$('.user.entry[data-id="'+userId+'"]').html(response);
					$('#modal').modal('toggle');
					initUserInteraction();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend(errorThrown);
				}
			});
		});
	})

	$('.userfolder.entry').click(function(e){
		console.log('klick auf userfolder');
		$('.user-block .app-col-detail .userfolder.entry').removeClass('active');
		$(this).addClass('active');
		toggleUser(currentState['user']['id'], $(this).data('id'));
	});

	$('.create-userfolder').click(function(e){
		e.stopPropagation();
		parentId = $(this).data('parent-id');

		$.ajax({
			url: '{{ URL::to('/') }}/userfolders/create'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			//$('#modal .modal-content').find('.current-userfolder-path').val(currentState['userfolder']['path']);
			$('#modal .modal-content').find('.current-user-id').val(currentState['user']['id']);
			$('#modal .modal-content').find('.current-project-id').val(currentState['projectId']);
			$('#modal .modal-content').find('.parent-id').val(parentId);

			$('#modal .modal-content').find('.modal-title').append('<br><small>Unter ' + currentState['userfolder']['title'] + '</small>');

			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				//dataType: "html",
				success: function (response) {
					$('#modal').modal('toggle');
					console.log(response);
					toggleUser(currentState['user']['id']);
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend('<div class="alert alert-danger" role="alert">'+errorThrown+'</div>');
				}
			});
		});
	})

	console.log('initUserInteraction');
}

function initFileInteraction(){
	$('.file.entry .control .edit').unbind( "click" );
	$('.file.entry .control .edit').click(function(e){

		e.stopPropagation();
		
		$.ajax({
			url: '{{ URL::to('/') }}/files/' + $(this).data('id') + '/edit'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					var fileId = $(response).data('id');
					$('.file.entry[data-id="'+fileId+'"]').replaceWith(response);
					$('#modal').modal('toggle');
					initFileInteraction();
					reInitDragDrop(false, true, false, false);
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend(errorThrown);
				}
			});
		});
	})
}

function initUsergroupInteraction(){
	$('.usergroup.entry .control .edit').unbind( "click" );
	$('.usergroup.entry .control .edit').click(function(e){

		e.stopPropagation();
		
		$.ajax({
			url: '{{ URL::to('/') }}/usergroups/' + $(this).data('id') + '/edit'

		}).done(function(data) {
			$('#modal .modal-content').html(data);
			$('#modal').modal('toggle');

			$('#modal form').ajaxForm({
				dataType: "html",
				success: function (response) {
					var usergroupId = $(response).data('id');
					$('.usergroup.entry[data-id="'+usergroupId+'"]').html(response);
					$('#modal').modal('toggle');
					initUserInteraction();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$('#modal .modal-content .modal-body').prepend(errorThrown);
				}
			});
		});
	})
}

function changeDir(path){
	console.log('pfad: '+ path);

	$('.folder-tree li a').removeClass('active');
	$('*[data-path="'+path+'"]').addClass('active');

	redrawFiles(path);
	console.log(currentState['currentDir']);
	currentState['currentDir'] = path;
	console.log(currentState);

	$('.files-manager  .app-col-detail .app-col-header .app-col-title').html(currentState['currentDir'].replace(/\//g, ' / '));
}

function redrawFiles(path){
	$.ajax({
		url: '{{ URL::to('/') }}/files/path/' + path.slice(0, -1),//letzten Slash entfernen, sonst Probleme mit URL
		// context: document.body
	}).done(function(data) {
		console.log('path created');
		$('.files-manager .entry-list').html( data );
		initFileInteraction();
		reInitDragDrop(false, true, false, false);
		setDropzonePath(path.slice(0, -1));
	});
}

function reInitDragDrop(initFolders, initFiles, initUsers, initProjects, initUserfolders){
	initFolders = typeof initFolders !== 'undefined' ? initFolders : true;
	initFiles = typeof initFiles !== 'undefined' ? initFiles : true;
	initUsers = typeof initUsers !== 'undefined' ? initUsers : true;
	initProjects = typeof initProjects !== 'undefined' ? initProjects : true;
	initUserfolders = typeof initUserfolders !== 'undefined' ? initUserfolders : true;

	if(initFolders){
		console.log('drop folder');
		$( ".folder-tree a" ).droppable({ 
			accept: ".file.entry",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
				$.ajax({
					url: '{{ URL::to('/') }}/files/move-to-folder/' + $(ui.helper).data('id') + '/' + $(event.target).data('path').slice(0,-1)//letzten Slash entfernen, sonst Probleme mit URL
					// context: document.body
				}).done(function(data) {
					redrawFiles(currentState['currentDir']);
					//$(event.target).find('.collapse').html( $(data).find('.collapse').html() );
				});
			}
		});
	}

	if(initFiles){
		$( ".file.entry" ).draggable({ 
			helper: "clone",
			appendTo: ".app-container",
			containment: "document",
			cursorAt:{
				top: 100,
				left: 100
			}
		})
	}

	if(initUsers){
		$( ".user-list .user.entry" ).draggable({ 
			helper: "clone",
			appendTo: ".app-container",
			containment: "document",
			scrollSensitivity: 200,
			cursorAt:{
				top: 100,
				left: 100
			}
		});


		@if($currentProject != null)
		$( ".user.entry" ).droppable({ 
			accept: ".file.entry, .folder.entry",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
						// console.log($(event.target).data('id'));
				if($(ui.helper).hasClass('file')){
					$.ajax({
						url: '{{ URL::to('/') }}/files/connect-to-user/' + $(ui.helper).data('id') + '/' + $(event.target).data('id') + '/{{$currentProject->id}}'//letzten Slash entfernen, sonst Probleme mit URL
						// context: document.body
					}).done(function(data) {
						$(event.target).find('.collapse').html( $(data).find('.collapse').html() );
						reInitDragDrop(false, false, true, false);
						toggleUser(currentState['user']['id'], currentState['userfolder']['id']);
					});
				}else if($(ui.helper).hasClass('folder')){
					$.ajax({
						url: 'users/add-to-usergroup/' + $(ui.helper).data('id') + '/' + $(event.target).data('id')//letzten Slash entfernen, sonst Probleme mit URL
						// context: document.body
					}).done(function(data) {
						$(event.target).find('.collapse').html( $(data).find('.collapse').html() );
						reInitDragDrop(false, false, false, true);
					});
				}
			}
		});
		@endif

		
	}

	if(initProjects){
		$( ".project.entry" ).draggable({ 
			helper: "clone",
			appendTo: ".app-container",
			containment: "document",
			scrollSensitivity: 200,
			cursorAt:{
				top: 100,
				left: 100
			}
		});

		$( ".project.entry" ).droppable({ 
			accept: ".user.entry",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
				
				$.ajax({
					url: '{{ URL::to('/') }}/users/connect/' + $(ui.helper).data('id') + '/toproject/' + $(event.target).data('id')//letzten Slash entfernen, sonst Probleme mit 
				}).done(function(data) {
					console.log('connected');
				});

			}
		});
	}


	if(initUserfolders){

		$( ".userfolder.entry" ).droppable({ 
			accept: ".file.entry",
			activeClass: "drop-target",
			hoverClass: "drop-target-hover",
			drop: function(event, ui){
				
				$.ajax({
					url: '{{ URL::to('/') }}/userfolders/connect-file/' + $(ui.helper).data('id') + '/to-userfolder/' + $(event.target).data('id')
					//url: '{{ URL::to('/') }}/users/connect/' + $(ui.helper).data('id') + '/toproject/' + $(event.target).data('id')//letzten Slash entfernen, sonst Probleme mit 
				}).done(function(data) {
					console.log('connected');
					toggleUser(currentState['user']['id'], currentState['userfolder']['id']);
					//$(event.target).find('.collapse').html( $(data).find('.collapse').html() );
					//reInitDragDrop(false, false, false, true);
				});
			}
		});
	}
}

// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);


function setDropzonePath(path){
	myDropzone.options.url = "upload-handler/" + path;
}

function initDropzone(currentPath){
	
	console.log(currentPath);

	myDropzone = new Dropzone('.drag-overlay', { // Make the whole body a dropzone
	  url: "{{ URL::to('/') }}/upload-handler/" + currentPath, // Set the url
	  thumbnailWidth: 80,
	  thumbnailHeight: 80,
	  parallelUploads: 20,
	  previewTemplate: previewTemplate,
	  autoQueue: true, // Make sure the files aren't queued until manually added
	  previewsContainer: ".entry-list", // Define the container to display the previews
	  clickable: ".add-file-btn" // Define the element that should be used as click trigger to select files.
	});

	// Hide the total progress bar when nothing's uploading anymore
	myDropzone.on("queuecomplete", function(progress) {
		console.log('fertig');
		redrawFiles(currentState['currentDir']);
	});

	console.log('dropzone url ' + myDropzone.options.url);
}


</script>

</body>

</html>
<html>
<head>
	<meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/icon-style.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('stylesheets/style.css') }}">
	<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
  @if(Auth::check())
  <header class="page-header">
    <div class="breadcrumbs">@yield('breadcrumbs')</div>
    <div class="backend-user-actions">Signed in as {{Auth::user()->username}} | <a href="{{URL::route('logout')}}" title="Logout">Logout</a></div>
    <div class="clear"></div>
  </header>
  @endif
	@yield('content')
</body>
</html>
@extends('scaffolding')

@section('title')
Login
@stop

@section('content')

<div class="login-panel panel">
	<div class="panel-heading">
		<h1>Login</h1>
	</div>
	<div class="panel-body">
		
		{{ Form::open(array('route' => 'sessions.store')) }}


		<div class="form-group">
			{{ Form::label('username', 'Username: ') }}
			{{ Form::text('username', null, array('class' => 'form-control', 'autofocus' => 'autofocus')) }}
		</div>
		<div class="form-group">
			{{ Form::label('password', 'Password: ') }}
			{{ Form::password('password', array('class' => 'form-control')) }}
		</div>

	</div>
	<div class="panel-footer">
		{{Form::submit('Log In', array('class' => 'btn btn-primary'))}}

	</div>

		{{ Form::close() }}
</div>

@stop
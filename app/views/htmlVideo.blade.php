

	@if($qt == null)

		<video class="video-player" id="previewVideo" controls autoplay>
			<source src="{{url($file->getTemporaryDownloadUrl())}}" type="video/{{$file->file_type}}" />
		</video>
	@else

		<div class="fallback">
			<object class="flash-player" type="application/x-shockwave-flash"
			        data="{{URL::to('swf/flowplayer-3.2.18.swf')}}" width="1000" height="560">
			    <param name="movie" value="{{URL::to('swf/flowplayer-3.2.18.swf')}}" />
			    <param name="allowFullScreen" value="true" />
			    <param name="wmode" value="transparent" />
			    <!-- note the encoded path to the image and video files, relative to the .swf! -->
			    <!-- more on that here: http://en.wikipedia.org/wiki/Percent-encoding -->
			    <param name="flashVars"
			           value="config={'playlist':['{{$file->thumbMd()}}',{'url':'{{url($file->getTemporaryDownloadUrl())}}','autoPlay':true}]}" />
			    <!-- fallback image if flash fails -->
			    <img class="poster" src="{{$file->thumbLg()}}" width="100%" title="No Flash found" />
			</object>
			<img class="size-helper" src="{{$file->thumbMd()}}"/>
			<script>
			$('.size-helper').load(function(){
				var aspectRatio = $('.size-helper').width() / $('.size-helper').height();
				$('.size-helper').remove();
				console.log(aspectRatio);

				$('.flash-player').width($('#video').width());
				$('.flash-player').height($('#video').width() / aspectRatio);
			})
			</script>
		</div>
		

	@endif
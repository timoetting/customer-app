@if ($userfolder->children()->count() == 0)
    <li>
		<a href="{{URL::to('/downloadarea/'.$user->username.'/'.$project->slug.'/'.$userfolder->id)}}" class=" @if($currentUserfolder->id == $userfolder->id)) active @endif ">{{$userfolder->title}}</a>
    </li>
@else
	<li class="has-child opened">
		<a href="{{URL::to('/downloadarea/'.$user->username.'/'.$project->slug.'/'.$userfolder->id)}}" class=" @if($currentUserfolder->id == $userfolder->id)) active @endif ">{{$userfolder->title}}</a> 
		<ul>
			@foreach($userfolder->children as $childFolder)
				@include('frontend.userfolder_recursive', array('userfolder' => $childFolder))
			@endforeach
		</ul>
	</li>
@endif
<div class="file-list size-m">
	@foreach($files->unique() as $file)
		@if(!($file->hasVideoBrother() && $file->file_type == 'webm'))
		<div class="file-list-entry">
			@if($file->isVideo()) 
			<div class="col col-thumb">
			<div class="thumb-container @if($file->isVideo()) video @endif" onclick="showVideo({{$file->id}})">
				@if($file->thumb == null || empty($file->thumb))
				<div class="file-type">
					@if($file->file_type != "none")
					.{{$file->file_type}}
					@endif
				</div>
				@else
				<img class="thumbnail" src="{{$file->thumbSm()}}" alt="">
				@endif
				<div class="overlay">
					<div class="play-icon">
						<i class="icon-play"></i>
					</div>
				</div>
			</div>
			</div>

			@else
			<div class="col col-thumb">
			<div class="thumb-container ">
				@if($file->thumb == null || empty($file->thumb))
				<div class="file-type">
					@if($file->file_type != "none")
					.{{$file->file_type}}
					@endif
				</div>
				@else
				<img class="thumbnail" src="{{$file->thumbMd()}}" alt="">
				@endif
			</div>
			</div>

			@endif
			<div class="col col-name">
				<div class="file-title">{{$file->file_name}}</div>
				<div class="file-description">{{$file->description}}</div>
			</div>
		<!-- 	<div class="col col-length">0:40</div> -->
			<div class="col col-date">{{ date("d.m.y", strtotime($file->pivot->created_at))}}<span class="secondary">{{date("H:i", strtotime($file->created_at))}}</span></div>
			<div class="col col-size">{{number_format(File::size(public_path().'/'.$file->path.$file->file_name)/1048576, 2)}} MB</div>
			<div class="col col-download-button"><a href="{{url($file->getTemporaryDownloadUrl())}}"  class="btn download-button">Download</a></div>
			<div class="col col-download-checkbox"><input type="checkbox" class="download-checkbox" data-id={{$file->id}}></div>
		</div>
		@endif
	@endforeach
	
</div>
<div class="file-list size-l">
	<div class="download-checkbox-all-container">
		<label>
			select all files
			<input type="checkbox" class="download-checkbox-all">
		</label>
	</div>
	@foreach($files->unique() as $file)
		@if(!($file->hasVideoBrother() && $file->file_type == 'webm'))
		<div class="file-list-entry">
			@if($file->isVideo()) 

			<div class="thumb-container @if($file->isVideo()) video @endif" onclick="showVideo({{$file->id}})">
				@if($file->thumb == null || empty($file->thumb))
				<div class="file-type">
					@if($file->file_type != "none")
					.{{$file->file_type}}
					@endif
				</div>
				@else
				<img class="thumbnail" src="{{$file->thumbMd()}}" alt="">
				@endif
				<div class="overlay">
					<div class="play-icon">
						<i class="icon-play"></i>
					</div>
				</div>
			</div>

			@else

			<div class="thumb-container ">
				@if($file->thumb == null || empty($file->thumb))
				<div class="file-type">
					@if($file->file_type != "none")
					.{{$file->file_type}}
					@endif
				</div>
				@else
				<img class="thumbnail" src="{{$file->thumbMd()}}" alt="">
				@endif
			</div>

			@endif
			<div class="file-info-container">
				<div class="file-name">{{$file->file_name}}</div>
				<div class="file-description">{{$file->description}}</div>
				<a href="{{url($file->getTemporaryDownloadUrl())}}"  class="btn download-button">Download</a>
				<input type="checkbox" class="download-checkbox" data-id={{$file->id}}>
			</div>

		<!-- 	<div class="col col-length">0:40</div> -->
		</div>
		@endif
	@endforeach
	<div class="clear"></div>
	
</div>
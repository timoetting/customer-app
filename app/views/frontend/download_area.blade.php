@extends('scaffolding')

@section('title')
Login
@stop

@section('content')

<div class="login-panel panel">
	<div class="panel-heading">
		<h1>Your projects</h1>
	</div>
	<div class="list-group">
		@foreach ($user->projects as $project)
			<a class="list-group-item" href="{{route('downloadarea_project', array(Auth::user()->username, $project->slug))}}">
				{{$project->title}}
			</a>
		@endforeach
	</ul>
</div>



@stop
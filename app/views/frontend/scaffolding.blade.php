<html>
<head>
	<meta charset="utf-8"> 
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ URL::asset('frontend/stylesheets/style.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('frontend/stylesheets/icon-style.css') }}">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
</head>
<body>
	@yield('content')
	
	
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	@yield('javascript')
	
</body>
</html>
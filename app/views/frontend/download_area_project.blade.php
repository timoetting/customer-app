@extends('frontend.scaffolding')
@section('title')
	{{$project->title}}
@stop	

@section('content')
	<div class="header">
		<div class="header-bar" @if(!empty($project->color)) style="background-color: {{$project->color}}" @endif >
			<div class="logo">
				<img src="{{URL::to('frontend/img/logo_one_line.svg')}}" alt="Finaledit Contentaccess">
			</div>
			<div class="logout">
				{{$user->username}} <a href="{{URL::route('logout')}}">logout</a>
			</div>
			
		</div>
		<div class="header-content">
			<div class="project-thumb">
				@if(count($project->thumb) && $project->thumb != '' )
					<img src="{{URL::to(Config::get('paths.thumbnailsProjects')).'/'.$project->thumb}}" alt="{{$project->title}}">
				@endif
			</div>
			<div class="project-infos">

				<div class="project-info project-name">{{$project->title}}</div>
				@if($project->meta_informations != null)
					@foreach($project->meta_informations as $meta_key => $meta_info)
					<div class="project-info"><span class="label">{{$meta_key}}: </span>{{$meta_info}}</div>
					@endforeach
				@endif
			</div>
			<div class="control">
				<div class="view-switch">
					<a href="{{Request::url()}}?view-size=s&sort-by={{$sortBy}}" @if($viewSize == 's') class="active" @endif><i class="icon-list"></i></a>
					<a href="{{Request::url()}}?view-size=m&sort-by={{$sortBy}}" @if($viewSize == 'm') class="active" @endif><i class="icon-th-list"></i></a>
					<a href="{{Request::url()}}?view-size=l&sort-by={{$sortBy}}" @if($viewSize == 'l') class="active" @endif><i class="icon-th-large"></i></a>
				</div>

				@if($sortBy == 'default')
				<div class="sort-switch">
					<div class="option-list">
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=default">default sorting <i class="arrow icon-arrow-down5"></i></a>
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=title">sort by title</a>
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=date">sort by date</a>
					</div>
				</div>
				@elseif($sortBy == 'title')
				<div class="sort-switch">
					<div class="option-list">
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=title">sort by title <i class="arrow icon-arrow-down5"></i></a>
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=date">sort by date</a>
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=default">default sorting</a>
					</div>
				</div>
				@elseif($sortBy == 'date')
				<div class="sort-switch">
					<div class="option-list">
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=date">sort by date <i class="arrow icon-arrow-down5"></i></a>
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=default">default sorting</a>
						<a href="{{Request::url()}}?view-size={{$viewSize}}&sort-by=title">sort by title</a>
					</div>
				</div>
				@endif
		</div>

			
		</div>
		<div class="clear"></div>

		
	</div>
	<div class="left-col">

		<div class="nav">
			<ul>
				<?php $userfolders = $user->userfoldersByProject($project) ?>
				@if(count($userfolders) > 1)
					@foreach ($userfolders as $userfolder)
						<li><a href="{{URL::to('/downloadarea/'.$user->username.'/'.$project->slug.'/'.$userfolder->id)}}" class=" @if($currentUserfolder->id == $userfolder->id)) active @endif ">{{$userfolder->title}}</a></li>
					@endforeach
				@endif
			</ul>
		</div>
	</div>
		

	<div class="content-col size-{{$viewSize}}">
		<div class="main">
			<div class="content">
					
					<div class="table-header">
						<div class="file-list size-{{$viewSize}}">
							<div class="file-list-header">
								<div class="col col-thumb"></div>
								<div class="col col-name">name</div>
								<div class="col col-note">note</div>
								<!-- <div class="col col-length">length</div> -->
								<div class="col col-date">date</div>
								<div class="col col-size">size</div>
								<div class="col col-download-button"></div>
								<div class="col col-download-checkbox"><input type="checkbox" class="download-checkbox-all"></div>
							</div>
						</div>
				</div>
				@include('frontend.files_'.$viewSize)
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="userfolder-actions">
		<span class="selectedFilesCount"></span> file(s) selected <a href="{{url('zipstream/'.$project->title.'/')}}" class="zip-download-button btn download-button">download zip file</a>
	</div>
	<div class="video-overlay">
		<div id="video" class="video">
		<div class="close" onclick="closeOverlay()"><i class="icon-cross"></i></div>
			
		</div>
	</div>
@stop	

@section('javascript')
<script>
	function closeOverlay(){
		$('.video-overlay').removeClass('active');
		$('.video-overlay video').remove();
		$('.video-overlay embed').remove();
		$('.video-overlay object').remove();
		$('.video-overlay .fallback').remove();
	}
	function showVideo(id){
		var macFF = (window.mozInnerScreenX != null && navigator.platform.indexOf('Mac')>=0);

		if(macFF){
			$.ajax({
				url: '{{ URL::to("/") }}/video/' + id + '/qt'
			}).success(function(data) {
				console.log(data);
				$('.video-overlay').addClass('active');
				$('.video-overlay .video').append(data);
			});
		}else{
			$.ajax({
				url: '{{ URL::to("/") }}/video/' + id
			}).success(function(data) {
				console.log(data);
				$('.video-overlay').addClass('active');
				$('.video-overlay .video').append(data);
			});
		}
	}
	$(function(){
		var onFileSelect = function(){
			var selectedFiles = $('.download-checkbox:checked').map(function() {
		    return $(this).data('id');
		  })
		  .get()
		  .join();
		  if(selectedFiles != ''){
		  	$('.userfolder-actions').addClass('visible');
		  }else{
		  	$('.userfolder-actions').removeClass('visible');
		  }
		  $('.selectedFilesCount').text($('.download-checkbox:checked').length);
		  var zipBaseUrl = "{{url('zipstream/'.$project->slug.'/')}}/";
		  $('.zip-download-button').attr('href', zipBaseUrl + selectedFiles);
		}

		$('.download-checkbox').change(onFileSelect);

		var onFileSelectAll = function(){
			if($(this).prop('checked')){
				$('.download-checkbox').prop('checked', true);
			}else{
				$('.download-checkbox').prop('checked', false);
			}
			$('.download-checkbox').trigger('change');
		}

		$('.download-checkbox-all').change(onFileSelectAll);

		$(document).on('click', '.download-button', function(){
			$('.download-checkbox').prop( "checked", false );
		  	$('.userfolder-actions').removeClass('visible');
		});

	});
</script>
@stop









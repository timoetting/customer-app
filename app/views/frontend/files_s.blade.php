<div class="file-list size-s">
	@foreach($files->unique() as $file)
		@if(!($file->hasVideoBrother() && $file->file_type == 'webm'))
		<div class="file-list-entry @if($file->isVideo()) video @endif">
			<div class="col col-thumb">
				<div class="icon {{$file->file_type}} filetype"></div>
				<div class="icon play"></div>
			</div>
			<div class="col col-name file-title">
				@if($file->isVideo())
				<a href="#0" onclick="showVideo({{$file->id}})">
					{{$file->title}}
				</a> 
				@else
					{{$file->title}}
				@endif
			</div>
			<div class="col col-note"><div class="file-description">{{$file->description}}</div></div>
		<!-- 	<div class="col col-length">0:40</div> -->
			<div class="col col-date">{{ date("d.m.y", strtotime($file->pivot->created_at))}}<span class="secondary">{{date("H:i", strtotime($file->created_at))}}</span></div>
			<div class="col col-size">{{number_format(File::size(public_path().'/'.$file->path.$file->file_name)/1048576, 2)}} MB</div>
			<div class="col col-download-button"><a href="{{url($file->getTemporaryDownloadUrl())}}"  class="btn download-button">Download</a></div>
			<div class="col col-download-checkbox"><input type="checkbox" class="download-checkbox" data-id={{$file->id}}></div>
		</div>
		@endif
	@endforeach
	
</div>
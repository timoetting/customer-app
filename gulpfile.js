/**
 * gulp.js File
 *
 * Compiles sass and minifies style and script files for use in productive
 * environments. Allows to auto-refresh pages on file changes.
 */

/* jshint node:true */

// Loads gulp plugins.
var pkg          = require('./package.json'),
    gulp         = require('gulp'),
    gulpif       = require('gulp-if'),
    notify       = require('gulp-notify'),
    rename       = require('gulp-rename'),
    size         = require('gulp-size'),
    watch        = require('gulp-watch'),
    browserSync  = require('browser-sync'),
    reload       = browserSync.reload,
    autoprefixer = require('gulp-autoprefixer'),
    compass      = require('gulp-compass'),
    concat       = require('gulp-concat'),
    cssmin       = require('gulp-cssmin'),
    jshint       = require('gulp-jshint'),
    plumber      = require('gulp-plumber'),
    uglify       = require('gulp-uglify');

// Builds and compiles sass files.
gulp.task('sass', function() {
  return gulp.src('public/stylesheets/*.scss')
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(compass({
      css: 'public/stylesheets',
      sass: 'public/stylesheets',
      image: 'public/img'
    }))
    .pipe(autoprefixer('last 2 version', 'android 4', 'ios 6', 'ie 9'))
    .pipe(gulp.dest('public/stylesheets'))
    .pipe(size({ title: 'style' }));
});

// Builds and compiles sass files for frontend.
gulp.task('sassFrontend', function() {
  return gulp.src('public/frontend/stylesheets/*.scss')
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(compass({
      css: 'public/frontend/stylesheets',
      sass: 'public/frontend/stylesheets',
      image: 'public/img'
    }))
    .pipe(autoprefixer('last 2 version', 'android 4', 'ios 6', 'ie 9'))
    .pipe(gulp.dest('public/frontend/stylesheets'))
    .pipe(size({ title: 'style' }));
});

// Minify combined styles
gulp.task('cssmin', function() {
  return gulp.src([
      'assets/css/*.css',
      '!assets/css/*.min.css'
    ])
    .pipe(rename({ suffix: '.min' }))
    .pipe(cssmin())
    .pipe(gulp.dest('assets/css'))
    .pipe(size({ title: 'cssmin' }));
});

// Combines 3th party javascript files.
gulp.task('plugins', function() {
  return gulp.src(['assets/js/source/plugins.js', 'assets/js/vendor/*.js'])
    .pipe(concat('plugins.js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(size({ title: 'plugins' }));
});

// Builds and compiles custom javascript files.
gulp.task('scripts', function() {
  return gulp.src([
      'assets/js/source/*.js',
      '!assets/js/source/plugins.js'
    ])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(size({ title: 'scripts' }));
});

// Lint JavaScript files.
gulp.task('jshint', function() {
  return gulp.src('assets/js/source/**/*.js')
    .pipe(reload({
      stream: true,
      once: true
    }))
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(gulpif(!browserSync.active, jshint.reporter('fail')));
});

// Minify JavaScript files.
gulp.task('uglify', function() {
  return gulp.src([
      'assets/js/*.js',
      '!assets/js/*.min.js'
    ])
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
    .pipe(size({ title: 'uglify' }));
});

// Synchronize and refresh browser viewport.
gulp.task('sync', function() {
  browserSync({
    notify: true,
    logPrefix: pkg.name
  });
});

// Triggers tasks on file changes.
gulp.task('watch', ['sync'], function() {
  // gulp.watch('**/*.{html,php}', reload);
  gulp.watch('public/stylesheets/**/*.scss', ['sass', reload]);
  gulp.watch('public/frontend/stylesheets/**/*.scss', ['sassFrontend', reload]);
  //gulp.watch('assets/js/**/*.js', ['jshint', 'plugins', 'scripts', reload]);
});

// Triggers the build manually.
gulp.task('build',   ['sass', 'sassFrontend',/* 'jshint', */'plugins', 'scripts']);
gulp.task('dist',    ['build', 'cssmin', 'uglify']);
gulp.task('default', ['build', 'watch']);
